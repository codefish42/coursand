// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.fibolist;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;

import fr.upem.coursand.R;

/**
 * An example of use of a ListView with a lazy loading induced by scrolling.
 *
 * @author chilowi at u-pem.fr
 */
public class FiboList extends Activity 
{
	/** Numbers of fibo numbers computed in a batch */
	public static final int BATCH_SIZE = 10;
	
	private ArrayList<Long> list;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		FrameLayout l = new FrameLayout(this);
		setContentView(l);
		list = new ArrayList<>();
		// Create an adapter backed on the list
		ListView listView = new ListView(this);
		l.addView(listView);
		final ArrayAdapter<Long> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
		listView.setAdapter(adapter);
		// Create a scroll listener to load the numbers as the user scroll
		listView.setOnScrollListener(new OnScrollListener() {
			
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) 
			{
			}
			
			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) 
			{
				// If we have scroll to the end of the list
				if (firstVisibleItem + visibleItemCount == totalItemCount)
				{
					// We must add new Fibonacci numbers
					generateNumbers(BATCH_SIZE);
					adapter.notifyDataSetChanged(); // The model must inform the view of the change
				}
			}
		});
		generateNumbers(BATCH_SIZE);
	}

	/** Generate a batch of fibo numbers to add in the list */
	private void generateNumbers(int numberToAppend)
	{
		int size = list.size();
		for (int i = 0; i < numberToAppend; i++)
		{
			long v = ((size > 0)?list.get(size-1):1) 
				+ ((size > 1)?list.get(size-2):1);
			list.add(v);
			size++;
		}
	}
}
