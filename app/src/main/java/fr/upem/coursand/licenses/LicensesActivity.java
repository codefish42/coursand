// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.licenses;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fr.upem.coursand.R;

public class LicensesActivity extends Activity implements SelectedTextListener
{
    public static class LicenseListFragment extends Fragment
    {
        private AssetLibrary library = null;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.fragment_textlist, null);
            ListView lv = (ListView)v.findViewById(R.id.libraryListView);
            library = AssetLibrary.getLibrary(getActivity(), getArguments().getString("libraryName"));
            List<String> libraryElements = new ArrayList<>();
            for (String e: library) libraryElements.add(e);
            Log.i(getClass().getName(), "show the library with the following elements; " + libraryElements);
            // set the adapter on the list view
            lv.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, libraryElements));
            // the activity must implement the listener
            SelectedTextListener listener = (SelectedTextListener)getActivity();
            lv.setOnItemClickListener((adapterView, view, i, l) -> listener.onSelectedText(libraryElements.get(i)));
            return v;
        }

        /** Called when the fragment is attached on the activity */
        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
        }

        /** Create a license fragment with the given arguments */
        public static LicenseListFragment create(String libraryName)
        {
            LicenseListFragment llf = new LicenseListFragment();
            Bundle b = new Bundle();
            b.putString("libraryName", libraryName);
            llf.setArguments(b);
            return llf;
        }
    }

    public static class LicenseFragment extends Fragment
    {
        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.fragment_textviewer, null);
            // load the text from the library of assets
            String libraryName = getArguments().getString("libraryName");
            String textName = getArguments().getString("textName");
            String text = null;
            if (libraryName!= null && textName != null)
                text = AssetLibrary.getLibrary(getActivity(), libraryName).getText(textName);
            TextView tv = v.findViewById(R.id.textViewerView);
            tv.setMovementMethod(new ScrollingMovementMethod());
            tv.setText(text != null ? text : "no text selected");
            return v;
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
        }

        /** Create a license fragment with the given arguments */
        public static LicenseFragment create(String libraryName, String textName)
        {
            LicenseFragment lf = new LicenseFragment();
            Bundle b = new Bundle();
            b.putString("libraryName", libraryName);
            b.putString("textName", textName);
            lf.setArguments(b);
            return lf;
        }
    }

    /**
     * An activity that is used as fallback if the mother activity cannot display
     * both the list of licenses and the selected license as two fragments on the same activity.
     * If only the list of licenses is displayed, a new activity is opened to view the selected license.
     */
    public static class LicenseActivity extends Activity
    {
        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            String libraryName = getIntent().getStringExtra("libraryName");
            String textName = getIntent().getStringExtra("textName");
            getFragmentManager()
                    .beginTransaction()
                    .replace(android.R.id.content, LicenseFragment.create(libraryName, textName))
                    .commit();
        }
    }

    public static final String LIBRARY_NAME = "licenses";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_licenses);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        // use replace since a fragment can already been there in cas of an activity recreation
        ft.replace(R.id.licenseListContainer, LicenseListFragment.create(LIBRARY_NAME));
        // check if the license container exists (landscape mode), in that case, add on it the license fragment
        if (findViewById(R.id.licenseContainer) != null)
            ft.replace(R.id.licenseContainer, LicenseFragment.create(null, null));
        ft.commit();
    }

    /** When a text is selected in the library */
    @Override
    public void onSelectedText(String textName)
    {
        if (findViewById(R.id.licenseContainer) != null) // there is a fragment to display the license
            getFragmentManager().beginTransaction()
            .replace(R.id.licenseContainer, LicenseFragment.create(LIBRARY_NAME, textName))
            .addToBackStack(null) // to allow back button
            .commit();
        else // there is no fragment, we must start a new activity
        {
            Intent i = new Intent(this, LicenseActivity.class);
            i.putExtra("libraryName", LIBRARY_NAME);
            i.putExtra("textName", textName);
            startActivity(i);
        }
    }
}

interface SelectedTextListener
{
    void onSelectedText(String textName);
}