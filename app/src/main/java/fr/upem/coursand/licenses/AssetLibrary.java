// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.licenses;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.SoftReference;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Manager for a library of Android assets.
 * Uses a multiton pattern to store the different libraries.
 */

public class AssetLibrary implements Iterable<String>
{
    private static final Map<String, AssetLibrary> libraries = new HashMap<>();

    public static AssetLibrary getLibrary(Context context, String name)
    {
        AssetLibrary lib = libraries.get(name);
        if (lib == null)
        {
            lib = new AssetLibrary(context);
            lib.load(name, "");
            libraries.put(name, lib);
        }
        return lib;
    }

    private final Context context;

    /** Files present in the library */
    private Map<String, String> entries = new TreeMap<>();

    /** Cache for the texts loaded from the library */
    private Map<String, SoftReference<String>> textCache = new HashMap<>();

    public AssetLibrary(Context context)
    {
        this.context = context.getApplicationContext();
    }

    /** Load the file entries present under the assets/$path directory */
    private void load(String root, String path)
    {
        Log.i(getClass().getName(), "Load root=" + root + ",path=" + path);
        boolean isFile = true;
        try {
            String[] filenames = context.getResources().getAssets().list(path.length() > 0 ? root + File.separatorChar + path : root);
            for (String filename: filenames)
            {
                load(root, path.length() > 0 ? path + File.separatorChar + filename : filename);
                isFile = false;
            }
        } catch (IOException e)
        {
            Log.i(getClass().getName(), "Exception while browsing the assets", e);
        }
        if (isFile) // if it is not a directory
        {
            // it is a file if it is not a directory
            if (path.length() > 0)
                entries.put(path, root + File.separatorChar + path);
        }
    }

    @Override
    public Iterator<String> iterator()
    {
        Log.i(getClass().getName(), "entries=" + entries);
        return entries.keySet().iterator();
    }

    /**
     * Get a text contained into an asset file.
     * Be careful if the file is too voluminous since the text is loaded entirely in memory
     */
    public String getText(String key)
    {
        String sep = System.getProperty("line.separator");
        String text = textCache.containsKey(key) ? textCache.get(key).get() : null;
        if (text != null) return text; // text is already in the cache
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(context.getAssets().open(entries.get(key)), StandardCharsets.UTF_8)))
        {
            StringBuilder sb = new StringBuilder();
            int counter = 0;
            for (String line = br.readLine(); line != null; line= br.readLine())
            {
                if (counter > 0) sb.append(sep);
                sb.append(line);
                counter++;
            }
            text = sb.toString();
            textCache.put(key, new SoftReference<>(text));
            return text;
        } catch (IOException e)
        {
            return null;
        }
    }
}
