// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.growingcircle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.MotionEvent
import fr.upem.coursand.R
import kotlinx.android.synthetic.main.activity_growing_circle.*

/**
 * An activity that displays a circle at the position of the pointer
 * that grows with time (until the pointer is released)
 */
class GrowingCircleActivity : AppCompatActivity() {

    companion object {
        const val START_RADIUS = 1.0f // initial radius
        const val RADIUS_SPEED = 20.0f // increment of radius by second
        const val REFRESH_PERIOD = 20L // in milliseconds
    }

    class State {
        var circle: Circle? = null
        var initialRadius: Float = Float.NaN
        var startTimestamp: Long = -1L // timestamp to compute the radius
        var endTimestamp: Long = -1L // timestamp when the touch is released

        val currentRadius: Float get() {
            val end = if (endTimestamp != -1L) endTimestamp else SystemClock.elapsedRealtime()
            return initialRadius + (end - startTimestamp) * RADIUS_SPEED / 1000.0f
        }

        val currentCircle: Circle? get() =
            circle?.let { Circle(it.x, it.y, currentRadius, it.color) }
    }

    private lateinit var state: State

    private val handler by lazy { Handler(mainLooper) }

    private val _viewRefresher: Runnable = Runnable {
        circleView.circle = state.currentCircle
        handler.postDelayed(viewRefresher, REFRESH_PERIOD) // refresh one more time
    }
    private val viewRefresher get() = _viewRefresher

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_growing_circle)
        state = State()
        circleView.setOnTouchListener { _, event ->
            when(event.actionMasked) {
                MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {
                    // new circle
                    state.initialRadius = if (event.actionMasked == MotionEvent.ACTION_DOWN)
                        START_RADIUS else state.currentRadius
                    state.startTimestamp = SystemClock.elapsedRealtime()
                    state.endTimestamp = -1L
                    state.circle = Circle(event.x, event.y, state.initialRadius)
                    if (event.actionMasked == MotionEvent.ACTION_DOWN)
                        handler.post(viewRefresher) // start refreshment
                    true
                }
                MotionEvent.ACTION_UP -> {
                    // end of circle
                    state.endTimestamp = SystemClock.elapsedRealtime()
                    handler.removeCallbacks(viewRefresher)
                    true
                }
                else -> false
            }
        }
    }


}
