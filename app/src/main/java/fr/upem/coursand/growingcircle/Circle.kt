// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.growingcircle

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint

data class Circle(val x: Float, val y: Float, val radius: Float, val color: Int = Color.BLACK) {
    companion object {
        val paint = Paint() // a single allocation
    }

    /** Draw the circle on the canvas */
    fun draw(canvas: Canvas) {
        paint.color = color
        canvas.drawCircle(x, y, radius, paint)
    }
}