// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.growingcircle

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View

class CircleView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    var circle: Circle? = null
    set(value) {
        if (value != field) {
            field = value
            invalidate()
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        circle?.draw(canvas)
    }
}