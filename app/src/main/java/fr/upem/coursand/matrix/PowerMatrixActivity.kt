// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.matrix

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.GridView
import fr.upem.coursand.R
import fr.upem.matrixlibrary.Matrix
import android.content.Intent
import fr.upem.matrixlibrary.IMatrixMultiplier
import android.widget.Toast
import android.os.IBinder
import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.RemoteException
import android.text.InputType
import android.util.Log
import java.util.*


/** Raise a matrix to a specified power using the external service "matriservice" */
class PowerMatrixActivity : Activity() {
    companion object {
        const val initialMatrixSize = 2
    }

    var matrixSize = initialMatrixSize
    var matrixContent = Matrix(0,0)
    var previousContent : Matrix? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_power_matrix)
        changeMatrixSize(initialMatrixSize)
    }

    /** A customized text watcher for the EditText of the grid view */
    inner class MyTextWatcher: TextWatcher
    {
        var position = -1

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun afterTextChanged(p0: Editable?) {
            Log.i(javaClass.name, "set $p0 to $matrixContent")
            if (position >= 0)
                matrixContent[position / matrixContent.cols, position % matrixContent.cols] = p0.toString().toInt()
        }

    }

    private fun changeMatrixSize(matrixSize: Int) {
        this.matrixSize = matrixSize
        // create an identity matrix
        this.matrixContent = Matrix.createIdentity(matrixSize)
        (findViewById<GridView>(R.id.matrixGridView)).adapter = object: ArrayAdapter<Int>(this, android.R.layout.simple_list_item_1) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                var v = (convertView ?: EditText(this@PowerMatrixActivity)) as EditText
                if (v.tag == null) {
                    v.inputType = InputType.TYPE_CLASS_NUMBER
                    val tw = MyTextWatcher()
                    v.tag = tw
                    v.addTextChangedListener(tw)
                }
                (v.tag as MyTextWatcher).position = position
                v.setText("" + matrixContent[position / matrixContent.cols, position % matrixContent.cols])
                return v
            }

            override fun getCount(): Int {
                return matrixSize * matrixSize
            }
        }
        (findViewById<GridView>(R.id.matrixGridView)).numColumns = matrixSize
    }

    private fun invalidateMatrixDisplay() {
        ((findViewById<GridView>(R.id.matrixGridView)).adapter as ArrayAdapter<*>).notifyDataSetChanged()
    }

    fun exp(m0: Matrix, power: Int, mult: (Matrix, Matrix) -> Matrix): Matrix {
        Log.i(javaClass.name, "Matrix exponentiation: $m0 to $power")
        if (power <= 0) return Matrix.createIdentity(m0.cols)
        val m1 = if (power / 2 > 0) {
            val a = exp(m0, power / 2, mult)
            mult(a, a)
        } else m0
        val m2 = if (power % 2 == 1) mult(m1, m0) else m1
        return m2
    }

    fun raise(v: View) {
        val power = (findViewById<EditText>(R.id.powerEditText)).text.toString().toInt()
        val conn = object : ServiceConnection {
            override fun onServiceDisconnected(name: ComponentName) {
                Toast.makeText(this@PowerMatrixActivity, "Disconnected", Toast.LENGTH_LONG).show()
            }

            override fun onServiceConnected(name: ComponentName, service: IBinder) {
                System.err.println("Connected!")
                Toast.makeText(this@PowerMatrixActivity, "Connected", Toast.LENGTH_LONG).show()
                try {
                    System.err.println("Implemented class: " + service.javaClass + ", " + Arrays.toString(service.javaClass.interfaces) +
                            " methods=" + Arrays.toString(service.javaClass.methods))
                    val r = exp(matrixContent, power, { x, y -> IMatrixMultiplier.Stub.asInterface(service).multiply(x, y) as Matrix })
                    previousContent = matrixContent
                    matrixContent = r
                    invalidateMatrixDisplay()
                } catch (e: RemoteException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
            }
        }
        val connected = bindService(Intent().setClassName("fr.upem.matriservice", "fr.upem.matriservice.MatrixMultiplierService"), conn, Context.BIND_AUTO_CREATE)
        if (! connected)
            Toast.makeText(this, "Cannot connect to the mutiplier service", Toast.LENGTH_LONG).show()
    }

    fun undo(v: View) {
        if (previousContent != null)
        {
            matrixContent = previousContent as Matrix
            previousContent = null
            invalidateMatrixDisplay()
        }
    }
}
