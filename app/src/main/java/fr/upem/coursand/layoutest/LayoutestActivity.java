// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.layoutest;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

import fr.upem.coursand.R;

public class LayoutestActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int sv = -1;
        if (savedInstanceState != null)
            sv = savedInstanceState.getInt("selectedView", -1);
        setContentView(R.layout.activity_layoutest);
        ViewGroup root = findViewById(R.id.layoutestRoot);
        for (int i = 0; i < root.getChildCount(); i++)
        {
            View v = root.getChildAt(i);
            boolean selected = sv != -1 && v.getId() == sv;
            v.setVisibility(selected ? View.VISIBLE : View.GONE);
            if (selected) selectedView = v;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("selectedView", selectedView.getId());
    }

    private final Map<MenuItem, View> menuMap = new HashMap<>();
    private View selectedView = null;

    /** Display a view on the frame layout (and render the previously displayed view not visible) */
    private void selectView(View v)
    {
        if (selectedView != null)
            selectedView.setVisibility(View.GONE);
        selectedView = v;
        selectedView.setVisibility(View.VISIBLE);
    }

    /** We populate the menu with all the layouts inside the frame layout of the activity */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ViewGroup root = (ViewGroup)findViewById(R.id.layoutestRoot);
        for (int i = 0; i < root.getChildCount(); i++)
        {
            View v = root.getChildAt(i);
            MenuItem mi = menu.add(v.getClass().getSimpleName());
            mi.setCheckable(true);
            menuMap.put(mi, v);
        }
        return super.onCreateOptionsMenu(menu);
    }

    /** We check the menu item related to the currently displayed layout */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        for (int i = 0; i < menu.size(); i++)
        {
            View v = menuMap.get(menu.getItem(i));
            if (v != null)
                menu.getItem(i).setChecked(v == selectedView);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    /** This method is executed when a menu item is clicked */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        View v = menuMap.get(item);
        if (v != null)
            selectView(v);
        return super.onOptionsItemSelected(item);
    }
}
