// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.topcities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

import fr.upem.coursand.R;
import fr.upem.coursand.configchange.ConfigChangeSupportActivity;

/** An activity displaying essential facts about top cities using a navigation drawer */
public class TopCitiesActivity extends ConfigChangeSupportActivity {

    /** Where the cities data are in the assets directory */
    public static final String CITIES_FILEPATH = "cities/topcities.csv";

    static class State {
        List<City> cities;
        int selectedCity = -1;
    }

    private State state = new State();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_cities);
        if (state.cities == null) loadCities();
        initToolbar();
        initDrawer();
        // display a random city
        displayCity(new Random().nextInt(state.cities.size()));
    }

    private void loadCities() {
        state.cities = City.Companion.loadFromAsset(this, CITIES_FILEPATH);
    }

    private void initToolbar() {
        // install the toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    private void initDrawer() {
        // configure the drawer with a listener for an item selection
       DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);

       NavigationView navigationView = findViewById(R.id.nav_view);

       // Populate the menu with the cities
       Menu menu = navigationView.getMenu();
       int i = 0;
       for (City city: state.cities)
           menu.add(Menu.NONE, Menu.FIRST + (i++), Menu.NONE, city.getName());

       // add a listener for the city selection
       navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    menuItem.setChecked(true);
                    displayCity(menuItem.getItemId() - Menu.FIRST);
                    drawerLayout.closeDrawers();
                    return true;
                });
    }

    /** To open the drawer when the menu icon is clicked */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        if (item.getItemId() == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void setText(int id, Object value) {
        ((TextView)findViewById(id)).setText(value.toString());
    }

    protected void displayCity(int i) {
        City city = state.cities.get(i);

        // update selection about the menu
        NavigationView navigationView = findViewById(R.id.nav_view);
        Menu menu = navigationView.getMenu();
        if (state.selectedCity >= 0)
            menu.findItem(state.selectedCity + Menu.FIRST).setChecked(false);
        menu.findItem(i + Menu.FIRST).setChecked(true);

        state.selectedCity = i;

        // display information about the city
        getSupportActionBar().setTitle(city.getName());
        setText(R.id.latitudeTextView, city.getLatitude());
        setText(R.id.longitudeTextView, city.getLongitude());
        setText(R.id.populationTextView, city.getPopulation());
        setText(R.id.meanAltitudeTextView, city.getElevation());

        // display the local hour of the city
        Calendar calendar = Calendar.getInstance(city.getTimeZone());
        setText(R.id.localHourTextView, String.format("%02d:%02d", calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));

        // display the planisphere
        ImageView planisphere = findViewById(R.id.planisphereView);
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.equirectangular_world_map);
        bm = bm.copy(Bitmap.Config.ARGB_8888, true); // to allow modification of the bitmap
        Canvas canvas = new Canvas(bm);
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        canvas.drawCircle(bm.getWidth() / 360.0f * (city.getLongitude() + 180.0f), bm.getHeight() - bm.getHeight()/180.0f * (city.getLatitude() + 90.0f), 50.0f, paint);
        planisphere.setImageBitmap(bm);
    }
}
