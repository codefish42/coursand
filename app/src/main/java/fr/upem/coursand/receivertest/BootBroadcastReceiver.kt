package fr.upem.coursand.receivertest

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import fr.upem.coursand.toastservice.ToastService

class BootBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // This method is called when the BroadcastReceiver is receiving an Intent broadcast.
        context.startService(Intent(context, ToastService::class.java).apply {
            action = ToastService.TOAST_ACTION
            putExtra("message", "Toast at boot!")
        })
    }
}