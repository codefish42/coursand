// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.dbbackuper

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.CalendarContract
import android.provider.CallLog
import android.provider.ContactsContract
import android.provider.Telephony
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat

data class Database(val location: Uri, val permission: String? = null) {

    fun isGranted(context: Context): Boolean {
        return permission == null || ContextCompat.checkSelfPermission(context, permission) ==
                PackageManager.PERMISSION_GRANTED
    }
}

object Databases {
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    val predefined = arrayOf(
        Database(Telephony.Sms.CONTENT_URI, Manifest.permission.READ_SMS),
        Database(Telephony.Mms.CONTENT_URI, Manifest.permission.READ_SMS),
        Database(ContactsContract.Data.CONTENT_URI, Manifest.permission.READ_CONTACTS),
        Database(CalendarContract.Events.CONTENT_URI, Manifest.permission.READ_CALENDAR),
        Database(CallLog.Calls.CONTENT_URI, Manifest.permission.READ_CALL_LOG)
    )

    /** Return if a permission is missing */
    fun checkMissingPermission(context: Context, location: String): String? {
        val db = predefined.find { location.startsWith(it.location.toString()) }
        return db?.let { if (it.isGranted(context)) null else it.permission }
    }
}