// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.dbbackuper

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.fragment.app.DialogFragment
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import fr.upem.coursand.R
import fr.upem.coursand.launcher.ActivityMetadata
import kotlinx.android.synthetic.main.activity_database_backup.*

@ActivityMetadata(permissions= [Manifest.permission.READ_CONTACTS, Manifest.permission.READ_CALENDAR, Manifest.permission.READ_CALL_LOG, Manifest.permission.READ_SMS])
class DatabaseBackupActivity : AppCompatActivity() {
    private val CREATE_DOCUMENT_REQUEST_CODE = 1
    private val REQUESTED_PERMISSION = 2

    class State {
        var outputUri: Uri? = null
    }

    val state = State()

    var currentBackupTask: AsyncTask<*,*,*>? = null

    private fun refreshDisplay() {
        outputTextView.text = state.outputUri?.toString() ?: ""
        backupButton.isEnabled = currentBackupTask == null && databaseEditText.text.isNotEmpty() && state.outputUri != null
        cancelButton.isEnabled = currentBackupTask != null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_database_backup)
    }

    class DatabaseChooser: androidx.fragment.app.DialogFragment() {
        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val builder = AlertDialog.Builder(requireContext())
            builder.apply {
                setTitle("Choose a predefined database")
                setItems(Databases.predefined.map { it.location.toString() }.toTypedArray()) { _, item ->
                    activity?.findViewById<EditText>(R.id.databaseEditText)?.setText(Databases.predefined[item].location.toString())
                }
            }
            return builder.create()
        }
    }

    fun onChooseDatabaseClick(v: View) {
        DatabaseChooser().show(supportFragmentManager, "databaseChooser")
    }

    fun onChooseOutputFileClick(v: View) {
        Intent(Intent.ACTION_CREATE_DOCUMENT)
            .setType("application/zip")
            .apply { startActivityForResult(this, CREATE_DOCUMENT_REQUEST_CODE) }
    }

    /** When the target output file is chosen */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CREATE_DOCUMENT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            state.outputUri = data?.data
            refreshDisplay()
        }
    }

    private fun doBackup() {
        val task = object: AsyncTask<Pair<Uri, Uri>, Float, Exception?>() {
            override fun doInBackground(vararg params: Pair<Uri, Uri>?): Exception? {
                val p = params[0] as Pair<Uri, Uri>
                return try {
                    backupDatabase(this@DatabaseBackupActivity, p.first, p.second, { isCancelled }, { publishProgress(it) })
                    null
                } catch (e: Exception) {
                    Log.e(javaClass.name, "Exception encountered while backuping", e)
                    e
                }
            }

            override fun onProgressUpdate(vararg values: Float?) {
                super.onProgressUpdate(*values)
                progressBar.apply { progress = ((values.last()?:0.0f) * max).toInt() }
            }

            override fun onPreExecute() {
                super.onPreExecute()
                refreshDisplay()
            }

            override fun onPostExecute(result: Exception?) {
                super.onPostExecute(result)
                currentBackupTask = null
                refreshDisplay()
                if (result == null)
                    Toast.makeText(this@DatabaseBackupActivity, "Backup is done", Toast.LENGTH_LONG).show()
                else
                    Toast.makeText(this@DatabaseBackupActivity, "Backup failed: ${result.message}", Toast.LENGTH_LONG).show()
            }
        }
        task.execute(Uri.parse(databaseEditText.text.toString()) to state.outputUri!!)
        currentBackupTask = task
    }

    fun onBackupClick(v: View) {
        val location = databaseEditText.text.toString()
        val missingPermission = Databases.checkMissingPermission(this, location)
        if (missingPermission != null)
            ActivityCompat.requestPermissions(this, arrayOf(missingPermission), REQUESTED_PERMISSION)
        else
            doBackup()
    }

    /** When the permission is granted we can start the backup */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == REQUESTED_PERMISSION && grantResults.firstOrNull() == PackageManager.PERMISSION_GRANTED)
            doBackup()
    }

    fun onCancelClick(v: View) {
        currentBackupTask?.cancel(true)
    }
}
