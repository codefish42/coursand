// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.dbbackuper

import android.content.Context
import android.database.Cursor
import android.net.Uri
import org.json.JSONObject
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

fun Cursor.getColumnValue(i: Int): Any? {
    return when (this.getType(i)) {
        Cursor.FIELD_TYPE_STRING -> this.getString(i)
        Cursor.FIELD_TYPE_INTEGER -> this.getLong(i)
        Cursor.FIELD_TYPE_FLOAT -> this.getDouble(i)
        Cursor.FIELD_TYPE_BLOB -> this.getBlob(i)
        else -> return null
    }
}

fun Cursor.getRecordAsMap(): Map<String, Any?> {
    return this.columnNames.mapIndexed{ i,s -> s to getColumnValue(i)  }.toMap()
}


/** Backup a database to a zip file */
fun backupDatabase(context: Context, database: Uri, outputFile: Uri, interrupter: () -> Boolean = { false }, progressCallback: (Float) -> Unit = {}) {
    val cursor = context.contentResolver.query(database, null, null, null, null)
    cursor?.use { cursor ->
        val output = context.contentResolver.openOutputStream(outputFile)
        val zip = ZipOutputStream(output)
        zip.use {
            var remaining = cursor.moveToFirst()
            var counter = 0
            while (remaining && !interrupter()) {
                val map = cursor.getRecordAsMap()
                val _id = map["_id"]?.toString() ?: "_$counter"
                val entry = ZipEntry("${_id}.json")
                it.putNextEntry(entry)
                it.write(JSONObject(map).toString().toByteArray(Charsets.UTF_8))
                progressCallback(counter.toFloat() / cursor.count.toFloat())
                it.closeEntry()
                counter++
                remaining = cursor.moveToNext()
            }
            it.flush()
        }
    }
}
