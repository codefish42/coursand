// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.counter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import fr.upem.coursand.R;

/**
 * Example activity incrementing a counter without using an additional thread
 * (only a Handler is employed)
 */
public class HandlerCounterActivity extends AppCompatActivity {

    private TextView counterView;
    private Handler handler;
    private long counterNow = 0L;

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_thread_counter); // use the same layout than the thread version
        handler = new Handler();
        counterView = findViewById(R.id.counterView);
    }

    /** Code executed periodically to display and increment the counter */
    private final Runnable incrementRunnable = () -> {
        counterView.setText("" + counterNow++);
        // recursive call of increment runnable 1 second later
        handler.postDelayed(getIncrementRunnable(), 1000);
    };

    /** Required to allow recursive call from increment runnable */
    private Runnable getIncrementRunnable() {
        return incrementRunnable;
    }

    /**
     * we start here the incrementation
     */
    @Override
    protected void onStart() {
        super.onStart();
        incrementRunnable.run();
    }

    /**
     * we stop here the incrementation
     */
    @Override
    public void onStop() {
        super.onStop();
        handler.removeCallbacks(incrementRunnable);
    }
}
