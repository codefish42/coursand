// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.counter;

import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import fr.upem.coursand.R;

public class ThreadCounterActivity extends AppCompatActivity {

    private TextView counterView;
    private Handler handler;
    private Thread refreshThread = null;

    public void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_thread_counter);
        handler = new Handler();
        counterView = findViewById(R.id.counterView);
    }

    /**
     * we start here the incrementing thread
     */
    @Override
    protected void onStart() {
        super.onStart();
        refreshThread = new Thread( ()-> {
                int counter = 0;
                while (!Thread.interrupted()) {
                    // UI code must be executed in the UI thread (we post the code on the handler)
                    final int counterNow = counter;
                    handler.post( () -> counterView.setText("" + counterNow));
                    // we could also use this
                    // runOnUiThread( () -> { counterView.setText("" + counter); } );
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        return; // exit from the thread
                    }
                    counter++;
                }
        });
        refreshThread.start();
    }

    /**
     * we stop here the incrementing thread
     */
    @Override
    public void onStop() {
        super.onStop();
        // testing nullity is a precaution (refreshThread should be not null)
        if (refreshThread != null) refreshThread.interrupt();
    }
}
