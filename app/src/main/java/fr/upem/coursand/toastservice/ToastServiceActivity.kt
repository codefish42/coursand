// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.toastservice

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.upem.coursand.R
import kotlinx.android.synthetic.main.activity_toast_service.*

//tag::main[]
/** An activity submitting messages to a service displaying them as toasts */
class ToastServiceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_toast_service)
        sendButton.setOnClickListener {
            val intent = Intent(this, ToastService::class.java)
            intent.action = ToastService.TOAST_ACTION
            intent.putExtra("message", messageView.text.toString())
            intent.putExtra("longDuration", longDurationView.isChecked)
            startService(intent)
        }
    }
}
//end::main[]
