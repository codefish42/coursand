// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.toastservice

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import fr.upem.coursand.R
import kotlinx.android.synthetic.main.activity_future_toast.*

/**
 * Schedule a toast in the future
 * We use a  Intent that starts the ToastService in the future
 * This Intent will be launched using the AlarmManager
 */
class FutureToastActivity : AppCompatActivity() {

    private var currentPendingIntent: PendingIntent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_future_toast)
        scheduleButton.setOnClickListener {
            // get the alarm manager
            val am = getSystemService(Context.ALARM_SERVICE) as AlarmManager

            // retrieve the data supplied in the fields
            val delay = delayView.text.toString().toInt() * 1000 // delay in millis
            val message = messageView.text.toString()

            // create the Intent to launch
            val intent = Intent(this, ToastService::class.java)
            intent.action = ToastService.TOAST_ACTION
            intent.putExtra("message", message)

            // wrap the Intent into a PendingIntent (delegated Intent that will be sent in the future)
            currentPendingIntent = PendingIntent.getService(this, 0, intent, 0)

            // schedule the intent with the AlarmManager
            am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + delay, currentPendingIntent)
            cancelButton.isEnabled = true
        }
        cancelButton.setOnClickListener {
            currentPendingIntent?.let {
                val am = getSystemService(Context.ALARM_SERVICE) as AlarmManager
                // cancel the scheduled PendingIntent
                // note that the PendingIntent may have already been sent
                am.cancel(it)
                cancelButton.isEnabled = false
            }
        }
    }
}
