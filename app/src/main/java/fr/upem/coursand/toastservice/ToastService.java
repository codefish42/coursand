// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.toastservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

/**
 * A simple that displays a toast.
 * Not very useful (an activity can display a toast itself without relying on a service)
 * but illustrates the implementation of a simple service.
 */
//tag::main[]
public class ToastService extends Service {

    /** An action that has an unique name */
    public static final String TOAST_ACTION = ToastService.class.getName() + ".displayToast";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(TOAST_ACTION)) {
            String id = intent.getStringExtra("id");
            String message = intent.getStringExtra("message");
            boolean longDuration = intent.getBooleanExtra("longDuration", false);
            Toast t = Toast.makeText(this, message, longDuration ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT);
            t.show();
        }
        return START_NOT_STICKY; // if the service is killed, the intent will not be redelivered
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not implemented (since we do not use RPC methods)");
    }
}
//end::main[]
