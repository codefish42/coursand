// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.clipboard

import android.Manifest
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import fr.upem.coursand.R
import kotlinx.android.synthetic.main.activity_encrypt_clipboard.*

/** An activity that encrypt/decrypt the text content of the clipboard */
class EncryptClipboardActivity : AppCompatActivity() {

    private val clipboardManager: ClipboardManager
        by lazy { getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_encrypt_clipboard)
    }

    /** Paste text from the clipboard */
    fun onPasteClick(v: View) {
        if (clipboardManager.hasPrimaryClip()) {
            val item = clipboardManager.primaryClip?.getItemAt(0)
            val text = item?.coerceToText(this)
            if (text != null)
                clipboardView.setText(text)
        }
    }

    /** Copy text to the clipboard */
    fun onCopyClick(v: View) {
        val clip = ClipData.newPlainText("text", clipboardView.text)
        clipboardManager.setPrimaryClip(clip)
    }

    /** Encrypt text */
    fun onEncryptClick(v: View) {
        // we assume encryption and decryption is fast and can be done directly in UI thread
        val encrypted = clipboardView.text.toString().encrypt(passphraseView.text.toString())
        clipboardView.setText(encrypted)
    }

    /** Decrypt text */
    fun onDecryptClick(v: View) {
        val decrypted = clipboardView.text.toString().decrypt(passphraseView.text.toString())
        if (decrypted == null)
            Toast.makeText(this, "Decryption failure: is the passphrase correct?", Toast.LENGTH_SHORT).show()
        else
            clipboardView.setText(decrypted)
    }
}
