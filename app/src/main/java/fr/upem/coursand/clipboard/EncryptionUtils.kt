// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.clipboard

import android.util.Base64
import java.lang.Exception
import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

val ByteArray.sha256: ByteArray get() {
    val md = MessageDigest.getInstance("SHA-256")
    return md.digest(this)
}

val String.sha256: ByteArray get() {
    return this.toByteArray(Charsets.UTF_8).sha256
}

/** Encrypt the string using the AES 256 bits algorithm.
 *  The key is computed applying the SHA-256 hash function on the UTF-8 encoded passphrase.
 *  The initialization vector is the first 16 bits from the SHA-256 value of the string to encrypt
 *  (the result is deterministic)
 */
fun String.encrypt(passphrase: String): String {
    val iv = this.sha256.copyOf(16)
    val keySpec = SecretKeySpec(passphrase.sha256, "AES")
    val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
    cipher.init(Cipher.ENCRYPT_MODE, keySpec, IvParameterSpec(iv))
    return Base64.encodeToString(iv + cipher.doFinal(this.toByteArray(Charsets.UTF_8)), 0)
}

/** Decrypt the string using the AES 256 bits algorithm */
fun String.decrypt(passphrase: String): String? {
    val ba = Base64.decode(this, 0)
    val iv = ba.copyOf(16)
    val content = ba.copyOfRange(16, ba.size)
    val keySpec = SecretKeySpec(passphrase.sha256, "AES")
    val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
    cipher.init(Cipher.DECRYPT_MODE, keySpec, IvParameterSpec(iv))
    val decrypted = try { cipher.doFinal(content) } catch ( e: Exception ) { null }
    if (true != decrypted?.sha256?.copyOf(16)?.contentEquals(iv)) return null // hashes do not match
    return decrypted.toString(Charsets.UTF_8)
}