package fr.upem.coursand.permission;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.HashMap;
import java.util.Map;

/** Template for an activity that needs to acquire a dangerous permission */
public class RequiringPermissionActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    /** Store here the permission requests with the associated onSuccess and onFailure runnables */
    private Map<Integer, Runnable[]> permissionRequests = new HashMap<>();
    private int nextPermissionRequestCode = 1;

    /** Run code that requires a prior permission grant 
     *  @param permissionName name of the permission to claim
     *  @param rationale message to display to explain to the user why this permission must be used 
     * 	@param onFailure code to execute if the permission is refused
     *  @param onSuccess code to execute if the permission is granted 
     */
    protected void runWithPermission(String permissionName, String rationale, Runnable onFailure, Runnable onSuccess)
    {
		// Is the permission already granted?
        if (ContextCompat.checkSelfPermission(this, permissionName) != PackageManager.PERMISSION_GRANTED)
        {
			// The permission is not granted yet
            // Should we show an explanation?
            if (rationale != null && ActivityCompat.shouldShowRequestPermissionRationale(this, permissionName))
            {
                // show an explanation with a dialog
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Permission information");
                alertDialog.setMessage(rationale);
                alertDialog.setIcon(android.R.drawable.ic_dialog_info);
                // if ok is pressed rerun the method (but not showing now the rationale)
                alertDialog.setButton("OK", ((dialogInterface, i) -> runWithPermission(permissionName, null, onFailure, onSuccess)));
                alertDialog.show();
            } else
            {
                int code = nextPermissionRequestCode++; // use an unique id for the permission request
                permissionRequests.put(code, new Runnable[]{onFailure, onSuccess});
                // request asynchronously the permission
                ActivityCompat.requestPermissions(this, new String[]{permissionName}, code);
            }
        } else
            onSuccess.run(); // the permission has already been given already given
    }

	/** This method is called when the user has answered to the permission request */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        Runnable[] runnables = permissionRequests.get(requestCode);
        if (runnables != null)
        {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                runnables[1].run(); // run onSuccess since the permission has been granted
            else
                runnables[0].run(); // permission not granted
        }
    }
}
