package fr.upem.coursand.gpslogger;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import fr.upem.coursand.chrono.NotifiedChronometer;

//tag::main[]
/** A simple service that logs locations into a file.
 * It must be activated by a companion activity that must check that the location permissions are enabled.
 *
 * @author chilowi at u-pem.fr
 */
public class LocationLoggerService extends Service
{
	public static final String LOCATION_FILE = "locations.log";

	public static final String LOCATION_BROADCAST_ACTION = LocationLoggerService.class.getName() + ".LOCATION_INFO";
	
	LocationManager locationManager = null;
	LocationListener locationListener = null;
	Writer writer = null;

	private static boolean running = false;

	public static boolean isRunning()
	{
		return running;
	}
	
	@Override
	public IBinder onBind(Intent arg0) { return null; }

	public static final String CHANNEL_ID = "locationLogger";

	/** This method creates a new notification channel (required for API 26+)
	 *  It is copied from https://developer.android.com/training/notify-user/build-notification
	 */
	private void createNotificationChannel()
	{
		// Create the NotificationChannel, but only on API 26+ because
		// the NotificationChannel class is new and not in the support library
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			CharSequence name = "Location logger channel";
			String description = "Channel for the location logger";
			int importance = NotificationManager.IMPORTANCE_DEFAULT;
			NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
			channel.setDescription(description);
			// Register the channel with the system; you can't change the importance
			// or other notification behaviors after this
			NotificationManager notificationManager = getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(channel);
		}
	}

	private Notification createNotification()
	{
		NotificationCompat.Builder mBuilder =
				new NotificationCompat.Builder(this, CHANNEL_ID)
						.setSmallIcon(android.R.drawable.ic_media_play)
						.setContentTitle("Location logger")
						.setContentText("The logger is active")
						.setPriority(NotificationCompat.PRIORITY_DEFAULT)
						.setSilent(true);
		return mBuilder.build();
	}
	
	private void log(String message)
	{
		try {
			if (writer == null)
			{
				writer = new OutputStreamWriter(openFileOutput(LOCATION_FILE, MODE_APPEND));
			}
			writer.write(new Date() + ": " + message);
			writer.flush();
		} catch (IOException e)
		{
			Log.e(getClass().getName(), "Cannot log message " + message + " due to an exception", e);
		}
	}
	
	@Override
	public void onCreate()
	{
		createNotificationChannel();
		locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		locationListener = new LocationListener() {
			
			@Override public void onStatusChanged(String provider, int status, Bundle extras) 
			{
				log(String.format("Change of status of provider %s: %d", provider, status));
			}
			
			@Override public void onProviderEnabled(String provider) 
			{
				log(String.format("Provider %s is enabled", provider));
			}
			
			@Override public void onProviderDisabled(String provider) 
			{
				log(String.format("Provider %s is disabled", provider));
			}
			
			@Override public void onLocationChanged(Location location) 
			{
				log(String.format("latitude=%f, longitude=%f, altitude=%f", 
					location.getLatitude(), location.getLongitude(), location.getAltitude()));
				Intent i = new Intent(LOCATION_BROADCAST_ACTION);
				i.putExtra("latitude", location.getLatitude());
				i.putExtra("longitude", location.getLongitude());
				sendBroadcast(i);
			}
		};
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) 
	{
		startForeground(1, createNotification());
		try {
			String provider = intent.getStringExtra("provider");
			if (provider == null) provider = LocationManager.GPS_PROVIDER;
			locationManager.requestLocationUpdates(provider,
					intent.getLongExtra("minTime", 10000),
					intent.getFloatExtra("minDistance", 100.0f),
					locationListener);
			running = true;
		} catch (SecurityException e)
		{
			Toast.makeText(this, "Cannot start the location logger service", Toast.LENGTH_LONG).show();
			stopSelf();
		}
		return Service.START_REDELIVER_INTENT; // Restart the service with the intent if it is destroyed
	}
	
	@Override
	public void onDestroy()
	{
		try { if (writer != null) writer.close(); } catch (IOException ignored) {}
		try {
			locationManager.removeUpdates(locationListener);
		} catch (SecurityException e)
		{
			// this case should not be encountered
		}
		running = false;
	}

}
//end::main[]
