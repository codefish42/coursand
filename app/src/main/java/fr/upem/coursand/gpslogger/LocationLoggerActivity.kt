package fr.upem.coursand.gpslogger

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import fr.upem.coursand.R
import fr.upem.coursand.launcher.ActivityMetadata
import fr.upem.coursand.permission.RequiringPermissionActivity
import java.util.*

/** Companion activity for the location logger */
@ActivityMetadata(permissions= [Manifest.permission.ACCESS_FINE_LOCATION])
//tag::main[]
class LocationLoggerActivity : RequiringPermissionActivity() {

    var serviceIntent: Intent? = null

    var receiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val infoTextView = findViewById<TextView>(R.id.infoTextView)
            val latitude = intent.getDoubleExtra("latitude", Double.NaN)
            val longitude = intent.getDoubleExtra("longitude", Double.NaN)
            infoTextView.text = "Update received on ${Date()}: $latitude/$longitude"
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        serviceIntent = Intent(this, LocationLoggerService::class.java)
        setContentView(R.layout.activity_location_logger)
    }

    override fun onStart() {
        super.onStart()
        val cb = findViewById<CheckBox>(R.id.locationLoggerServiceCheckBox)
        runWithPermission(Manifest.permission.ACCESS_FINE_LOCATION, "You must grant the GPS permission",
                {Toast.makeText(this, "Location permission is not granted", Toast.LENGTH_SHORT).show()
                    cb.isEnabled = false},
                {cb.isEnabled = true})
        cb.isChecked = LocationLoggerService.isRunning()
        registerReceiver(receiver, IntentFilter(LocationLoggerService.LOCATION_BROADCAST_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(receiver)
    }


    fun onCheckBoxClick(v: View) {
        val action = if ((v as CheckBox).isChecked) { x: Intent? -> startService(x) } else { x -> stopService(x) }
        action(serviceIntent)
    }
}
//end::main[]
