// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.chrono;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ToggleButton;

import fr.upem.coursand.R;

//tag::main[]
/**
 * Activity controlling the chronometer notification service.
 *
 * @author chilowi at u-pem.fr
 */
public class NotifiedChronometer extends Activity 
{
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notified_chronometer);
		// We update the state of the button if the chronometer runs
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		((ToggleButton)findViewById(R.id.chronoButton)).setChecked(ChronoService.isRunning());
	}
	
	public void onChronoButtonClick(View v)
	{
		CheckBox highPriorityCheckBox = findViewById(R.id.highPriorityCheckBox);
		boolean on = ((ToggleButton)v).isChecked();
		if (on)
			startService(new Intent(this, ChronoService.class)
					.setAction(ChronoService.ACTION_START)
					.putExtra("highPriority", highPriorityCheckBox.isChecked()));
		else
			startService(new Intent(this, ChronoService.class).setAction(ChronoService.ACTION_STOP));
	}

}
//end::main[]
