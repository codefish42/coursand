package fr.upem.coursand.sms

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.widget.Toast

/** This service receives intercepted text messages and display them as toasts */
class SMSSpyService : Service() {
    companion object {
        const val GIVE_MESSAGE_ACTION = "GIVE_MESSAGE"
    }


    override fun onBind(intent: Intent): IBinder? {
      return null
    }

    private fun spy(message: String, sender: String) {
        Toast.makeText(this, "Have spied SMS from $sender: $message", Toast.LENGTH_LONG).show()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        if (intent.action == GIVE_MESSAGE_ACTION)
            spy(intent.getStringExtra("message")!!, intent.getStringExtra("sender")!!)
        return super.onStartCommand(intent, flags, startId)
    }
}
