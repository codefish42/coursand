package fr.upem.coursand.sms;

import java.util.ArrayList;
import java.util.Random;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import fr.upem.coursand.R;
import fr.upem.coursand.launcher.ActivityMetadata;
import fr.upem.coursand.permission.RequiringPermissionActivity;

/** An activity that can be used to send a SMS */
@ActivityMetadata(permissions={Manifest.permission.SEND_SMS})
public class SMSSender extends RequiringPermissionActivity
{
	// A list saving the statuses of the SMS parts
	private final ArrayList<String> sentStatuses = new ArrayList<>();
	private ArrayAdapter<String> sentStatusesAdapter;
	private BroadcastReceiver br;
	private long sendTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sms_sender);
		// Initialize the adapter displaying the statuses
		sentStatusesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, sentStatuses);
		((ListView)findViewById(R.id.smsSentStatus)).setAdapter(sentStatusesAdapter);
	}
	
	public void onSend(View v)
	{
		// Send a SMS using the recipient and message given in the form
		String recipient = ((EditText)findViewById(R.id.smsRecipient)).getText().toString();
		String message = ((EditText)findViewById(R.id.smsMessage)).getText().toString();
		runWithPermission(Manifest.permission.SEND_SMS, "This activity requires to send SMS, please grant the permission",
			() -> Toast.makeText(this, "Cannot send since we do not have the permission", Toast.LENGTH_SHORT),
				() -> send(recipient, message));
	}
	
	private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz ";
	private static final int RANDOM_MESSAGE_LENGTH = 200;
	
	public void onGenerateRandomMessage(View v)
	{
		Random r = new Random();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < RANDOM_MESSAGE_LENGTH; i++) 
			sb.append(ALPHABET.charAt(r.nextInt(ALPHABET.length())));
		((EditText)findViewById(R.id.smsMessage)).setText(sb.toString());
	}
	
	public static final String SENT_ACTION = SMSSender.class.getName() + ".sentAction";
	public static final String DELIVERY_ACTION = SMSSender.class.getName() + ".deliveryAction";
	
	public void send(String recipient, String message)
	{
		Log.v("smsSender", String.format("Sending message %s to %s", recipient, message));
		SmsManager sm = SmsManager.getDefault();
		ArrayList<String> split = sm.divideMessage(message);
		sentStatuses.clear();
		for (int i = 0; i < split.size(); i++) sentStatuses.add("sending part #" + i);
		sentStatusesAdapter.notifyDataSetChanged();
		// Register broadcast receivers to obtain the notifications
		if (br != null) unregisterReceiver(br);
		br = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) 
			{
				Log.v("smsSender", "Retrieved intent=" + intent + ", extras=" + intent.getExtras());
				int chunkID = intent.getIntExtra("chunkID", -1);
				String message = null;
				if (intent.getAction().equals(SENT_ACTION))
				{
					switch (getResultCode())
					{
					case Activity.RESULT_OK:
						message = "sent"; break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						message = "send error #" + intent.getIntExtra("errorCode", -1); break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						message = "radio off"; break;
					default:
						message = "unknown send error"; break;
					}
				} else if (intent.getAction().equals(DELIVERY_ACTION))
				{
					message = SmsMessage.createFromPdu(intent.getByteArrayExtra("pdu")).getDisplayMessageBody();
				}
				if (message != null && chunkID >= 0)
				{
					sentStatuses.set(chunkID, sentStatuses.get(chunkID) + ", " + message + " after " + (System.nanoTime() - sendTime) / 1000000 + "ms");
					sentStatusesAdapter.notifyDataSetChanged();
				}
			}
		};
		IntentFilter filter = new IntentFilter();
		filter.addAction(SENT_ACTION);
		filter.addAction(DELIVERY_ACTION);
		registerReceiver(br, filter);
		ArrayList<PendingIntent> sentIntents = new ArrayList<>();
		ArrayList<PendingIntent> deliveryIntents = new ArrayList<>();
		for (int i = 0; i < split.size(); i++)
		{
			sentIntents.add(PendingIntent.getBroadcast(this, 2*i, new Intent(SENT_ACTION).putExtra("chunkID", i), 0));
			deliveryIntents.add(PendingIntent.getBroadcast(this, 2*i+1, new Intent(DELIVERY_ACTION).putExtra("chunkID", i), 0));
		}
		sendTime = System.nanoTime();
		sm.sendMultipartTextMessage(recipient, null, split, sentIntents, deliveryIntents);
	}
}
