// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.websocketchat

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import fr.upem.coursand.R
import fr.upem.coursand.launcher.ActivityMetadata
import kotlinx.android.synthetic.main.activity_web_socket_chat.*
import okhttp3.*
import java.util.*

/**
 * An activity exchanging UTF-8 text messages with a WebSocket.
 * It relies on the OkHttp library to manage the client WebSocket.
 */
@ActivityMetadata(permissions= [Manifest.permission.INTERNET])
class WebSocketChatActivity : AppCompatActivity() {

    lateinit var client: OkHttpClient
    var webSocket: WebSocket? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_socket_chat)
        client = OkHttpClient()
    }

    /** Append data in TextView */
    fun appendInChatView(text: String) {
        runOnUiThread { chatView.append(text) }
    }

    fun startWebSocket(v: View) {
        val request = Request.Builder().url(urlView.text.toString()).build()
        val listener = object: WebSocketListener() {
            override fun onOpen(webSocket: WebSocket, response: Response) {
                appendInChatView("Connected to server")
                runOnUiThread {
                    connectButton.visibility = View.GONE
                    closeButton.visibility = View.VISIBLE
                }
            }

            override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
                appendInChatView("Closing for reason '$reason'")
            }

            override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                appendInChatView("Disconnected from the server")
                runOnUiThread { // we must use the UI thread to act on the UI
                    connectButton.visibility = View.VISIBLE
                    closeButton.visibility = View.GONE
                }
            }

            override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
                appendInChatView("Failure: ${t.message}")
            }

            override fun onMessage(webSocket: WebSocket, text: String) {
                appendInChatView("${Date()}: $text")
            }
        }
        webSocket = client.newWebSocket(request, listener)
    }

    fun sendMessage(v: View) {
        val message = messageView.text.toString().trim()
        if (message != null)
            webSocket?.also { it.send(message) } // call is asynchronous
    }

    fun closeWebSocket(v: View) {
        webSocket?.also { it.close(1001, "client wants to disconnect")}
    }
}
