// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.squareview;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;

import fr.upem.coursand.R;

public class SquareActivity extends AppCompatActivity {

    private SquareView squareView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_square);
        squareView = findViewById(R.id.squareView);
        for (int id: new int[]{R.id.sizeBar, R.id.rednessBar})
            ((SeekBar)findViewById(id)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    float value = getValue(seekBar.getId());
                    switch (seekBar.getId()) {
                        case R.id.sizeBar: squareView.setSize(value); break;
                        case R.id.rednessBar: squareView.setRedness(value); break;
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) { }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) { }
            });
    }

    public float getValue(int viewId) {
        SeekBar bar = (SeekBar)findViewById(viewId);
        return (float)bar.getProgress() / bar.getMax();
    }
}
