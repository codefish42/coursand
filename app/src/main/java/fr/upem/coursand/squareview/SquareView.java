// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.squareview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/** A variable-sized view displaying a black-red square */
public class SquareView extends View {

    // Implementation of all the ancestor constructors
    public SquareView(Context context) {
        super(context);
    }

    public SquareView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SquareView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private float redness = 0.0f;
    private float size = 0.0f;

    public void setSize(float size) {
        if (size != this.size) {
            this.size = size;
            requestLayout(); // to resize
        }
    }

    public void setRedness(float redness) {
        if (redness != this.redness) {
            this.redness = redness;
            invalidate();
        }
    }

    protected int computeDimension(int spec) {
        if (MeasureSpec.getMode(spec) == MeasureSpec.EXACTLY)
            return MeasureSpec.getSize(spec); // we have no choice
        else
            return (int)(MeasureSpec.getSize(spec) * size);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(computeDimension(widthMeasureSpec), computeDimension(heightMeasureSpec));
    }

    private final Paint paint = new Paint();

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setARGB(255, (int)(255 * redness), 0, 0); // set the color
        canvas.drawRect(0, 0, getWidth(), getHeight(), paint);
    }
}
