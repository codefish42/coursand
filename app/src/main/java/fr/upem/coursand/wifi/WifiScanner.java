package fr.upem.coursand.wifi;

import java.util.List;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import fr.upem.coursand.R;
import fr.upem.coursand.launcher.ActivityMetadata;

/** An activity scanning (when it is in foreground) wifi networks in the vicinity */
@ActivityMetadata(permissions={Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.ACCESS_COARSE_LOCATION})
public class WifiScanner extends Activity
{
	public static final int SCAN_INTERVAL = 10000;

	WifiManager manager = null;
	List<ScanResult> scanResult = null;
	Handler handler = null;
	boolean running = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wifi_scanner);
		handler = new Handler();
		manager = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
	}

	BroadcastReceiver receiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			scanResult = manager.getScanResults();
			unregisterReceiver(this);
			((ListView)findViewById(R.id.wifiListView)).setAdapter(new ArrayAdapter<>(WifiScanner.this, android.R.layout.simple_list_item_1, scanResult));
			Toast.makeText(WifiScanner.this, "Having fetched " + scanResult.size() + " access points", Toast.LENGTH_SHORT).show();
			if (running)
				handler.postDelayed(WifiScanner.this::startScan, SCAN_INTERVAL);
		}
	};
	
	public void startScan()
	{
		registerReceiver(receiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		manager.startScan();
	}

	/** Reenable wifi scanning */
	@Override
	protected void onResume() {
		super.onResume();
		running = true;
		manager.setWifiEnabled(true);
		handler.post(this::startScan);
	}

	/** Stop scanning to prevent energy leaks */
	@Override
	protected void onPause() {
		super.onPause();
		running = false;
		handler.removeCallbacks(this::startScan);
	}
}
