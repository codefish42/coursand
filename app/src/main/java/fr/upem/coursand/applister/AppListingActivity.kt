// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.applister

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_app_listing.*
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import fr.upem.coursand.R

/**
 * List all the installed apps on the Android device.
 * The getInstalledApplication() method does not work since Android 11 for privacy reasons.
 */
class AppListingActivity : AppCompatActivity() {

    private val clipboardManager: ClipboardManager by lazy { getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager }

    class State {
        var user: Boolean = true
        var system: Boolean = false
    }

    var state = State()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_listing)
        appListingView.movementMethod = ScrollingMovementMethod()
        refreshDisplay()
    }

    private fun getInstalledApps(user: Boolean = true, system: Boolean = false): List<ApplicationInfo> {
        val flags = PackageManager.GET_META_DATA
        val pm = packageManager
        val applications = pm.getInstalledApplications(flags)
        return applications.filter {
            (system && (it.flags and ApplicationInfo.FLAG_SYSTEM > 0)) ||
                    (user && (0 == it.flags and ApplicationInfo.FLAG_SYSTEM))
        }
    }

    private fun refreshDisplay() {
        val apps = getInstalledApps(state.user, state.system)
        val text = apps.map { "- ${it.packageName}" }.sorted().joinToString("\n")
        appListingView.text = text
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.app_listing_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.findItem(R.id.displayUserApps)?.isChecked = state.user
        menu?.findItem(R.id.displaySystemApps)?.isChecked = state.system
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.displaySystemApps -> {state.system = !state.system; refreshDisplay(); true }
            R.id.displayUserApps -> { state.user = !state.user; refreshDisplay(); true }
            R.id.copyClipboardItem -> {
                val clip = ClipData.newPlainText("text", appListingView.text)
                clipboardManager.setPrimaryClip(clip)
                true
            }
            else -> false
        }
    }
}
