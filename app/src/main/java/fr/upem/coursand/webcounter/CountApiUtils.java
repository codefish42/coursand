// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.webcounter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/** Definition of useful methods to handle the counter API from CountApi.xyz on Android
 *  We use the HttpURLConnection API froom the java.net package
 */
public class CountApiUtils {
    public static final int BUFFER_SIZE = 2048;
    public static final Charset CHARSET = StandardCharsets.UTF_8;

    public static final String NAMESPACE = "coursand";

    /** This call does not follow the REST philosophy since it supports the GET method;
     *  whereas the GET method should not induce server-side modifications
     */
    public static final String INCREMENT_URL = "https://api.countapi.xyz/hit/:namespace/:key";

    public static final String GET_URL = "https://api.countapi.xyz/get/:namespace/:key";

    public static JSONObject makeHttpRequestWithJSONResult(String url) throws IOException, JSONException {
        URL u = new URL(url);
        HttpURLConnection connection = (HttpURLConnection)u.openConnection();
        connection.connect();
        if (connection.getResponseCode() == 200) {
            try (InputStream input = connection.getInputStream()) {
                // Accumulate all the data in memory
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[BUFFER_SIZE];
                for (int r = input.read(buffer); r != -1; r = input.read(buffer))
                    baos.write(buffer, 0, r); // write into the baos the read buffer
                byte[] data = baos.toByteArray();
                // Convert using the default charset to a string
                String dataStr = new String(data, 0, data.length, CHARSET);
                // Parse the string as JSON
                JSONObject json = new JSONObject(dataStr);
                return json;
            }
        } else
            throw new IOException("HTTP error " + connection.getResponseCode());
    }

    public static class CounterApiException extends Exception {
        public CounterApiException(Throwable cause) { super(cause); }
    }

    private static String makeURL(String base, String key) {
        String url = base.replace(":namespace", NAMESPACE).replace(":key", key);
        return url;
    }

    /** Increment a counter and return the value of the incremented counter */
    public static int incrementCounter(String key) throws CounterApiException {
        try {
            JSONObject result = makeHttpRequestWithJSONResult(makeURL(INCREMENT_URL, key));
            return result.getInt("value");
        } catch (IOException|JSONException e) {
            throw new CounterApiException(e);
        }
    }

    /** Fetch the value of a counter (return -1 if the counter does not exist) */
    public static int fetchCounter(String key) throws CounterApiException {
        try {
            JSONObject result = makeHttpRequestWithJSONResult(makeURL(GET_URL, key));
            if (result.get("value") == null) return -1; // non existing value
            return result.getInt("value");
        } catch (IOException|JSONException e) {
            throw new CounterApiException(e);
        }
    }


}
