// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.webcounter;

import android.Manifest;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import fr.upem.coursand.R;
import fr.upem.coursand.launcher.ActivityMetadata;

/** Activity handling a web counter with 2 operations: get and increment
 * TODO: support screen rotation (the activity is destroyed and restarted)
 */
@ActivityMetadata(permissions={Manifest.permission.INTERNET})
public class WebCounterActivity extends AppCompatActivity {

    private EditText counterKeyEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_counter);
        counterKeyEditText = findViewById(R.id.counterKeyView);
    }

    public void appendText(String message) {
        TextView tv = findViewById(R.id.counterResultView);
        tv.append(new Date().toString() +  ": " + message);
    }

    private String getKey() {
        String key = counterKeyEditText.getText().toString().trim();
        if (key.isEmpty()) {
            Toast.makeText(this, "Key must not be empty", Toast.LENGTH_SHORT).show();
            return null;
        } else
            return key;
    }

    /** Get the counter value */
    public void onGetClick(View v) {
        AsyncTask<String, Void, Integer> task = new AsyncTask<String, Void, Integer>() {
            private CountApiUtils.CounterApiException exception = null;
            private String key;

            @Override
            protected Integer doInBackground(String... strings) {
                this.key = strings[0];
                int result = -1;
                try {
                    result = CountApiUtils.fetchCounter(this.key);
                } catch (CountApiUtils.CounterApiException e) {
                    exception = e;
                }
                return result;
            }

            @Override
            protected void onPostExecute(Integer result) {
                if (result == -1 && exception == null) appendText("Key " + key + " does not exist\n");
                else if (result == -1 && exception != null) appendText("Exception encountered: " + exception.toString() + "\n");
                else appendText(key + "=" + result + "\n");
            }
        };
        String key = getKey();
        if (key != null) task.execute(key);
    }

    /** Get the counter value */
    public void onIncrementClick(View v) {
        AsyncTask<String, Void, Integer> task = new AsyncTask<String, Void, Integer>() {
            private CountApiUtils.CounterApiException exception = null;
            private String key;

            @Override
            protected Integer doInBackground(String... strings) {
                this.key = strings[0];
                int result = -1;
                try {
                    result = CountApiUtils.incrementCounter(this.key);
                } catch (CountApiUtils.CounterApiException e) {
                    exception = e;
                }
                return result;
            }

            @Override
            protected void onPostExecute(Integer result) {
                if (result == -1 && exception != null) appendText("Exception encountered: " + exception.toString() + "\n");
                else appendText(key + " incremented to " + result + "\n");
            }
        };
        String key = getKey();
        if (key != null) task.execute(key);
    }
}
