// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.util.Log
import androidx.compose.foundation.layout.Row
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview

//tag::main[]
@Composable
fun BatteryDisplayer() {
    val context = LocalContext.current
    var batteryLevel by remember { mutableStateOf(Float.NaN) }
    val broadcastReceiver = remember { object: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1).toFloat() /
                    intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1)
        }
    } }
    DisposableEffect(true) {
        val intent = context.registerReceiver(broadcastReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
        onDispose {
            Log.d("BatteryDisplayer", "Unregistered displayer")
            context.unregisterReceiver(broadcastReceiver)
        }
    }
    Text("Battery level: $batteryLevel")
}
//end::main[]

@Composable
@Preview
fun BatteryDisplayerPreview() {
    BatteryDisplayer()
}