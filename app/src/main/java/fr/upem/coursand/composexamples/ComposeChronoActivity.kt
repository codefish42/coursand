// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import fr.upem.coursand.composexamples.ui.theme.CoursandTheme
import fr.upem.coursand.flowchrono.FlowChrono
import fr.upem.coursand.flowchrono.FlowChronoViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import java.io.Serializable

class SystemClockViewModel: ViewModel() {
    val systemClockData: LiveData<Long> = liveData {
        while (true) {
            emit(SystemClock.elapsedRealtime())
            delay(REFRESH_PERIOD)
        }
    }
}

class ComposeChronoActivity : ComponentActivity() {
    private val systemClockViewModel: SystemClockViewModel by viewModels()

    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoursandTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    RootComponentWithLiveData(systemClockViewModel.systemClockData)
                }
            }
        }
    }
}

/** Immutable model */
data class ChronoModel(val start: Long = -1L, val cumulated: Long = 0L): Serializable {
    fun stop() = ChronoModel(start = -1L, cumulated = cumulated + SystemClock.elapsedRealtime() - start)
    fun start() = copy(start = SystemClock.elapsedRealtime())
    fun reset() = ChronoModel()
    val isRunning get() = start >= 0L
    fun elapsedTime(currentTimestamp: Long) = cumulated +
            (if (start >= 0L) (currentTimestamp - start) else 0L)
}

val Long.formattedTime: String get() {
    val millis = this % 1000
    val allSeconds = this / 1000
    val hours = allSeconds / 3600
    val minutes = (allSeconds / 60) % 60
    val seconds = allSeconds % 60
    return "$hours:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}:${millis.toString().padStart(3, '0')}"
}

@ExperimentalFoundationApi
@Composable
fun Chrono(model: ChronoModel, timestamp: Long, onStart: () -> Unit, onStop: () -> Unit, onReset: () -> Unit) {
    Column(Modifier.fillMaxWidth().scale(0f)) {
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
            Text(text = model.elapsedTime(timestamp).formattedTime, fontSize = 40.sp, fontFamily = FontFamily.Monospace,
                modifier = Modifier.combinedClickable(
                    onClick = { if (!model.isRunning) onStart() else onStop() },
                    onLongClick = { onReset() }
                ))
        }
        Spacer(Modifier.size(20.dp))
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
            Text(text="Short click to start/stop, long click to reset")
        }
    }
}

const val REFRESH_PERIOD = 40L // in milliseconds

@ExperimentalFoundationApi
@Composable
fun RootComponentWithLaunchedEffect() {
    Log.i("RootComponent", "Recompose at ${SystemClock.elapsedRealtime()}")
    var chronoModel by rememberSaveable { mutableStateOf(ChronoModel()) }
    var currentTimestamp by remember { mutableStateOf(SystemClock.elapsedRealtime()) }

    Box(Modifier.fillMaxSize()) {
        Box(Modifier.align(Alignment.Center)) {
            Chrono(chronoModel, currentTimestamp, { currentTimestamp = SystemClock.elapsedRealtime(); chronoModel = chronoModel.start()}, { currentTimestamp = SystemClock.elapsedRealtime(); chronoModel = chronoModel.stop() }, { chronoModel = chronoModel.reset() })
        }
    }
    if (chronoModel.isRunning)
        LaunchedEffect(currentTimestamp) {
            delay(REFRESH_PERIOD)
            currentTimestamp = SystemClock.elapsedRealtime()
        }
}

@ExperimentalFoundationApi
@Composable
fun RootComponentWithLiveData(systemClockLiveData: LiveData<Long>) {
    Log.i("RootComponent", "Recompose at ${SystemClock.elapsedRealtime()}")
    var chronoModel by rememberSaveable { mutableStateOf(ChronoModel()) }
    var currentTimestamp = systemClockLiveData.observeAsState(SystemClock.elapsedRealtime())
    Box(Modifier.fillMaxSize()) {
        Box(Modifier.align(Alignment.Center)) {
            Chrono(chronoModel, currentTimestamp.value, { chronoModel = chronoModel.start()}, { chronoModel = chronoModel.stop() }, { chronoModel = chronoModel.reset() })
        }
    }
}

@ExperimentalFoundationApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview3() {
    CoursandTheme {
        Chrono(ChronoModel(0L, 0L), 101452L, {}, {}, {})
    }
}