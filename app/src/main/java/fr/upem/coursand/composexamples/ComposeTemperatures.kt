// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.RadioButton
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

//tag::main[]
// with inline the class behaves as a primitive type in memory
inline class Temperature(val celsius: Float) {
    val farhenheit get() = celsius * 9f / 5f + 32f
    val kelvin get() = celsius + 273.15f
}

sealed class TemperatureScale(open val unit: String, open val extractValue: (Temperature) -> Float)
object CelsiusScale: TemperatureScale("C", { it.celsius })
object FahrenheitScale: TemperatureScale("F", { it.farhenheit })
object KelvinScale: TemperatureScale("K", { it.kelvin })

// default value is Celsius
val LocalTemperatureMode = compositionLocalOf<TemperatureScale> { CelsiusScale }

@Composable
fun TemperatureDisplayer(label: String, value: Temperature) {
    val t = "${LocalTemperatureMode.current.extractValue(value)} ${LocalTemperatureMode.current.unit}"
    Row {
        Text(label, Modifier.padding(5.dp))
        Text(t, Modifier.padding(5.dp))
    }
}

@Composable
fun TemperaturesDisplayer(temperatures: Map<String, Temperature>) {
    LazyColumn {
        items(temperatures.entries.toList(), { it.key }) { entry ->
            TemperatureDisplayer(entry.key, entry.value)
        }
    }
}

@Composable
fun TemperatureScaleSelector(scale: TemperatureScale, onScaleChanged: (TemperatureScale) -> Unit) {
    Row {
        // to use subclasses, the reflect dependency must be installed
        TemperatureScale::class.sealedSubclasses.forEach { s ->
            Row(Modifier.padding(10.dp)) {
                RadioButton(s == scale::class, onClick={ s.objectInstance?.let { onScaleChanged(it) } })
                Text(s.objectInstance?.unit ?: "")
            }
        }
    }
}

@Composable
fun EnhancedTemperaturesDisplayer(temperatures: Map<String, Temperature>) {
    var temperatureScale by remember { mutableStateOf<TemperatureScale>(CelsiusScale) }
    Column {
        TemperatureScaleSelector(temperatureScale) { temperatureScale = it }
        CompositionLocalProvider(LocalTemperatureMode provides temperatureScale) {
            TemperaturesDisplayer(temperatures)
        }
    }
}

@Composable
fun SampleTemperaturesDisplayer() {
    val map = mapOf(
        "Absolute zero" to Temperature(-273.15f),
        "Freezing point of water" to Temperature(0f),
        "Human body temperature" to Temperature(37f),
        "Boiling point of water" to Temperature(100f),
        "Burning point of paper" to Temperature(218f)
    )
    EnhancedTemperaturesDisplayer(temperatures = map)
}
//end::main[]

@Composable
@Preview
fun SampleTemperaturesDisplayerPreview() {
    SampleTemperaturesDisplayer()
}