// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import androidx.lifecycle.*
import fr.upem.coursand.composexamples.ui.theme.CoursandTheme
import fr.upem.coursand.flowchrono.FlowChrono
import fr.upem.coursand.flowchrono.FlowChronoViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import java.io.Serializable

class ComposeMultiActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoursandTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    ParentComposable2()
                }
            }
        }
    }
}

var MYTEXT_INSTANCE_COUNTER = 0

@Composable
fun MyText(text: String) {
    val myTextId by remember { mutableStateOf(MYTEXT_INSTANCE_COUNTER++) }
    Log.v("MyText", "Composition of MyText #${myTextId}")
    Text(text)
}

@Composable
fun ParentComposable() {
    var counter by remember { mutableStateOf(0) }
    Column {
        TwoTexts("first", "$counter")
        Button(onClick = { counter++ }) { Text("Increment") }
    }
}

@Composable
fun TwoTexts(first: String, second: String) {
    Log.v("MyText", "Composition of TwoTexts")
    MyText(first) // first call site
    MyText(second) // second call site, only this text will be recomposed since only third changes
}

@Composable
fun SeveralTexts(texts: List<String>) {
    Log.v("MyText", "Composition of SeveralTexts")
    for (t in texts) {
        MyText(t)
    }
}

@Composable
fun SeveralTextsWithKey(texts: List<String>) {
    Log.v("MyText", "Composition of SeveralTextsWithKey")
    texts.forEach { t ->
        key(t) {
            MyText(t)
        }
    }
}

@Composable
fun ParentComposable2(withKey: Boolean = false) {
    var elements = remember { mutableStateListOf<String>() }
    Column {
        if (withKey) SeveralTextsWithKey(elements) else SeveralTexts(elements)
        Button(onClick = { elements.add(0, "${elements.size}") }) { Text("Insert") }
    }
}


@ExperimentalFoundationApi
@Preview(showBackground = false)
@Composable
fun DefaultPreviewParentComposable2() {
    CoursandTheme {
        ParentComposable2(true)
    }
}