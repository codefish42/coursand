// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import fr.upem.coursand.R
import fr.upem.coursand.composelayouts.ui.theme.CoursandTheme



class ClickableImageActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoursandTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    ClickableImage()
                }
            }
        }
    }
}

@Composable
fun ClickableImage() {
    var opacity by remember { mutableStateOf(100) }
    Image(painter = painterResource(id = R.drawable.mercator_world_map),
        modifier = Modifier.clickable {
            opacity = (100 + opacity - 10) % 100
        },
        contentDescription = "world map",
        alignment = Alignment.Center,
        contentScale = ContentScale.Fit,
        alpha = opacity / 100.0f
    )
}



@Preview(showBackground = true)
@Composable
fun ClickableImagePreview() {
    CoursandTheme {
        ClickableImage()
    }
}