// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.widget.EditText
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.google.accompanist.flowlayout.FlowRow
import fr.upem.coursand.composexamples.ui.theme.CoursandTheme
import fr.upem.coursand.flowchrono.FlowChrono
import fr.upem.coursand.flowchrono.FlowChronoViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import java.io.Serializable
import java.math.BigInteger
import kotlin.math.sqrt


class ComposeFactorizerActivity : ComponentActivity() {

    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoursandTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    ComposeFactorizer()
                }
            }
        }
    }
}

//tag::main[]
data class FactorizationState(val progress: Float, val factors: List<BigInteger>, val cancelled: Boolean = false)

@Composable
fun getFactorizerState(number: BigInteger, cancelled: Boolean) = produceState(FactorizationState(0f, listOf()), number, cancelled) {
    if (! cancelled)
        withContext(Dispatchers.Default) {
            value = FactorizationState(0f, listOf())
            val two = BigInteger.valueOf(2)
            var n = number
            var nsqrt = sqrt(number.toDouble())
            if (n < BigInteger.valueOf(2)) {
                value = FactorizationState(1f, listOf()) // terminated, no factor found
            } else {
                var factors = listOf<BigInteger>()
                var divider = two
                var iterationCounter = 0
                while (divider * divider <= n) {
                    val dividing = n % divider == BigInteger.ZERO
                    if (dividing) {
                        factors += divider
                        n /= divider
                        nsqrt = sqrt(n.toDouble())
                        value =
                            FactorizationState(divider.toDouble().div(nsqrt).toFloat(), factors)
                    } else {
                        divider += if (divider == two) BigInteger.ONE else two
                    }
                    if (iterationCounter % 100 == 0) {
                        value = value.copy(progress = divider.toDouble().div(nsqrt).toFloat())
                        if (!isActive) {
                            value = value.copy(cancelled = true)
                            break // exit since the coroutine has been cancelled
                        }
                    }
                    iterationCounter++
                }
                if (! value.cancelled) {
                    if (n > BigInteger.ONE)
                        factors += n
                    value = FactorizationState(1f, factors)
                }
            }
        }
}

@Composable
fun Factorizer(number: BigInteger, onCancellationRequested: () -> Unit, cancelled: Boolean, modifier: Modifier = Modifier) {
    var state = getFactorizerState(number, cancelled)
    Card(modifier = modifier) {
        Column() {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text("$number")
                if (! state.value.cancelled && state.value.progress != 1f)
                    IconButton(onClick = { onCancellationRequested() }) { Icon(painter = painterResource(android.R.drawable.ic_delete), contentDescription = "Cancel") }
            }
            when {
                state.value.progress < 1f -> {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        LinearProgressIndicator(progress = state.value.progress, modifier=Modifier.weight(1f))
                        if (state.value.cancelled) Text("Cancelled")
                    }
                }
                state.value.progress == 1f -> Text("Computation terminated.")
            }
            FlowRow() {
                state.value.factors.forEach {
                    Text("$it", modifier= Modifier
                        .padding(5.dp)
                        .border(width = 1.dp, color = Color.Black)
                        .padding(5.dp))
                }
            }
        }
    }
}

@Composable
fun ComposeFactorizer() {
    var typedNumber by remember { mutableStateOf(BigInteger.ONE) }
    var validatedNumber by remember { mutableStateOf<BigInteger?>(null) }
    var cancelled by remember { mutableStateOf(false) }
    Column() {
        Row(verticalAlignment = Alignment.CenterVertically) {
            TextField(value="$typedNumber", onValueChange = { typedNumber = BigInteger(it) }, modifier = Modifier.weight(1f), keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number))
            Button(onClick = { validatedNumber = typedNumber; cancelled = false; }) { Text("Validate") }
        }
        val number = validatedNumber
        if (number != null)
            Factorizer(number, onCancellationRequested = {cancelled = true}, cancelled, modifier = Modifier.fillMaxWidth())
        else {
            Text("No number validated yet.")
        }
    }

}
//end::main[]

@Preview(showBackground = true)
@Composable
fun ComposeFactorizerPreview() {
    CoursandTheme {
        ComposeFactorizer()
    }
}