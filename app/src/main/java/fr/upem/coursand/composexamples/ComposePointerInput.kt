// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.core.animateIntSizeAsState
import androidx.compose.foundation.*
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import fr.upem.coursand.composexamples.ui.theme.CoursandTheme
import fr.upem.coursand.flowchrono.FlowChrono
import fr.upem.coursand.flowchrono.FlowChronoViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.io.Serializable
import kotlin.math.sqrt


class ComposePointerInput : ComponentActivity() {

    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoursandTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    PointerInputTest()
                }
            }
        }
    }
}

//tag::main[]
@Composable
fun FingerMileageDisplayer(fingerMileage: Map<Long, Float>, fingerPressed: Map<Long, Boolean>) {
    Column(Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center) {
        fingerMileage.forEach { entry ->
            Box(
                Modifier
                    .fillMaxWidth()
                    .background(if (fingerPressed[entry.key] == true) Color.Yellow else Color.LightGray)) {
                Text("${entry.value.toInt()}")
            }
        }
    }
}

@Composable
fun PointerInputTest() {
    var fingerMileage = remember { mutableStateMapOf<Long, Float>() }
    var fingerPressed = remember { mutableStateMapOf<Long, Boolean>() }
    Box(modifier = Modifier
        .fillMaxSize()
        .background(Color.Green)
        .border(1.0.dp, Color.Black)
        .pointerInput(Unit) {
            awaitPointerEventScope {
                while (true) {
                    awaitFirstDown()
                    Log.v("InputEvent", "FirstDown")
                    do {
                        val event = awaitPointerEvent()
                        event.changes.forEach { change ->
                            fingerPressed[change.id.value] = change.pressed
                            if (change.pressed) {
                                val offset = change.position - change.previousPosition
                                val distance = sqrt(offset.x * offset.x + offset.y * offset.y)
                                fingerMileage.merge(change.id.value, distance) { x, y -> x + y }
                            }
                            change.consumeAllChanges()
                        }
                        val pressed = event.changes.any { it.pressed }
                    } while (pressed)
                    Log.v("InputEvent", "End of event")
                }
            }
        }) {
        FingerMileageDisplayer(fingerMileage, fingerPressed)
    }
}
//end::main[]


@Preview(showBackground = true)
@Composable
fun PointerInputTestPreview() {
    CoursandTheme {
        PointerInputTest()
    }
}