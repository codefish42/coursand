// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.delay

//tag::main[]
class CrazyClickingViewModel: ViewModel() {
    private var _counter by mutableStateOf(1)

    val counter get() = _counter

    fun incrementCounter() {
        _counter++
    }

    val tick = liveData<Int> {
        var i = 0
        while (true) {
            emit(i++)
            delay(100L)
        }
    }

    private var backgroundColorIndex by mutableStateOf(0)

    companion object {
        val ALL_COLORS = arrayOf(Color.Blue, Color.White, Color.Red)
    }

    fun changeBackgroundColor() {
        backgroundColorIndex = (backgroundColorIndex + 1) % ALL_COLORS.size
    }

    val backgroundColor by derivedStateOf { ALL_COLORS[backgroundColorIndex] }
}

@Composable
fun CrazyClicking() {
    // dependency to use the viewModel function: implementation "androidx.lifecycle:lifecycle-viewmodel-compose:2.4.1"
    val vm: CrazyClickingViewModel = viewModel()
    val tick by vm.tick.observeAsState()
    LaunchedEffect(tick?.div(100) ?: 0) { vm.changeBackgroundColor() }
    Column(
        Modifier
            .background(color = vm.backgroundColor)
            .clickable { vm.incrementCounter() }) {
        Text("${vm.counter}")
        Text("${tick}")
    }
}
//end::main[]

@Composable
@Preview
fun CrazyClickingPreview() {
    CrazyClicking()
}