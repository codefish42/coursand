// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import fr.upem.coursand.composexamples.ui.theme.CoursandTheme
import fr.upem.coursand.flowchrono.FlowChrono
import fr.upem.coursand.flowchrono.FlowChronoViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import java.io.Serializable


class ComposeBMIActivity : ComponentActivity() {

    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoursandTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    HelloWorld("foobar")
                }
            }
        }
    }
}

@Composable
fun BMIGlobal() {
    Log.v("BMIGlobal", "Starting the composition")
    var mass by remember { mutableStateOf(70f) } // in kg
    var height by remember { mutableStateOf(1.70f) } // in meters
    val centimeterHeight by remember { derivedStateOf { height * 100 }}
    Column() {
        TextField("$mass", { t -> (t.toFloatOrNull() ?: 0f).let { mass = it } }, label = { Text("Mass in Kg") }, keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number))
        TextField("$centimeterHeight", { t -> (t.toFloatOrNull() ?: 0f).let { height = it / 100 } }, label = { Text("Height in cm")}, keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number))
        Text("You weigh $mass Kg and your height is ${height} meters")
        Text("Your BMI is ${(mass / height / height)}")
    }
}

data class BodyParams(val mass: Float, val height: Float)

val BodyParams.bodyMassIndex get() = mass / height / height

@Composable
fun BodyParamsInput(bodyParams: BodyParams, onChange: (BodyParams) -> Unit, modifier: Modifier = Modifier) {
    Column(modifier) {
        TextField("${bodyParams.mass}", { t -> onChange(bodyParams.copy(mass= (t.toFloatOrNull() ?: 0f))) }, label = { Text("Mass in Kg") }, keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number))
        TextField("${bodyParams.height * 100}", { t -> (t.toFloatOrNull() ?: 0f).let { bodyParams.copy(height = it / 100) } }, label = { Text("Height in cm")}, keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number))
    }
}

@Composable
fun BMIResult(bodyParams: BodyParams, modifier: Modifier = Modifier) {
    Column(modifier) {
        Text("You weigh ${bodyParams.mass} Kg and your height is ${bodyParams.height} meters")
        Text("Your BMI is ${bodyParams.bodyMassIndex}")
    }
}

@Composable
fun BMI() {
    var bodyParams by remember { mutableStateOf(BodyParams(70f, 1.7f))}
    Column() {
        BodyParamsInput(bodyParams, onChange = {bodyParams = it})
        BMIResult(bodyParams)
    }
}



@Preview(showBackground = true)
@Composable
fun BMIGlobalPreview() {
    CoursandTheme {
        BMIGlobal()
    }
}