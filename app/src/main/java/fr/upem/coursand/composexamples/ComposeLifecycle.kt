// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import androidx.lifecycle.*
import fr.upem.coursand.composexamples.ui.theme.CoursandTheme
import fr.upem.coursand.flowchrono.FlowChrono
import fr.upem.coursand.flowchrono.FlowChronoViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import java.io.Serializable
import java.lang.Math.abs
import kotlin.time.Duration

class ComposeLifecycleActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoursandTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    ParentComposable2()
                }
            }
        }
    }
}

//tag::main[]
@Composable
fun getLifecycleStartedState(): State<Int> {
    val lc = LocalLifecycleOwner.current.lifecycle
    val initial = if (lc.currentState.isAtLeast(Lifecycle.State.STARTED)) 1 else -1
    return produceState(initial, lc) {
        val observer = LifecycleEventObserver { _, event ->
            when(event) {
                Lifecycle.Event.ON_START -> value = kotlin.math.abs(value) + 1
                Lifecycle.Event.ON_STOP -> value = -(kotlin.math.abs(value) + 1)
                else -> {}
            }
            Log.d("getLifecycleStartedState", "New event: $event, new value=$value")
        }
        lc.addObserver(observer)
        try {
            delay(Long.MAX_VALUE) // infinite wait (or almost)
        } finally {
            Log.d("getLifecycleStartedState", "Remove observer")
            lc.removeObserver(observer)
        }
    }
}
//end::main[]

@Composable
fun ForegroundIncrementingCounter() {
    var counter by remember { mutableStateOf(0) }
    var started = getLifecycleStartedState()
    Log.d("ForegroundIncrementingCounter", "started=$started")
    Text("$counter")
    LaunchedEffect(started.value) {
        while (started.value > 0) {
            delay(500L)
            counter++
        }
    }
}

@Preview(showBackground = false)
@Composable
fun DefaultPreviewForegroundIncrementingCounter() {
    CoursandTheme {
        ForegroundIncrementingCounter()
    }
}