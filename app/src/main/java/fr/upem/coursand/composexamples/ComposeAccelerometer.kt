// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composexamples

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.compose.foundation.layout.Column
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview

//tag::main[]
@Composable
fun Accelerometer() {
    val acceleration = remember { mutableStateListOf(Float.NaN, Float.NaN, Float.NaN) }
    val context = LocalContext.current
    val sensorManager = remember { context.getSystemService(Context.SENSOR_SERVICE) as SensorManager }
    val accelerometer = remember { sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) }

    val listener = remember {
        object: SensorEventListener {
            override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}

            override fun onSensorChanged(event: SensorEvent) {
                event.values.forEachIndexed { index, value -> acceleration[index] = value }
            }
        }
    }

    DisposableEffect(true) {
        sensorManager.registerListener(listener, accelerometer, SensorManager.SENSOR_DELAY_UI)
        onDispose { sensorManager.unregisterListener(listener) }
    }

    Column() {
        Text("Accelerometer")
        acceleration.forEach {
            Text("$it")
        }
    }
}
//end::main[]

@Composable
@Preview
fun AccelerometerPreview() {
    Accelerometer()
}