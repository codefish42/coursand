// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.webrequest;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.upem.coursand.R;
import fr.upem.coursand.launcher.ActivityMetadata;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/** An activity allowing to do customized HTTP requests */
@ActivityMetadata(permissions={Manifest.permission.INTERNET})
public class WebRequestActivity extends AppCompatActivity {

    /** Value to use to specify where to put a joined file */
    public static final String FILE_PLACEHOLDER = "@file";

    private EditText urlView;
    private EditText methodView;
    private EditText headersView;
    private RadioGroup encodingView;
    private EditText bodyView;
    private TextView fileExplanationView;
    private TextView resultView;

    // If a file is joined
    private Uri fileUri;

    // current HTTP call
    private Call currentCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_request);

        // assign the fields (should be more easier using Kotlin or the ButterKnife library
        urlView = findViewById(R.id.urlView);
        methodView = findViewById(R.id.methodView);
        headersView = findViewById(R.id.headersView);
        encodingView = findViewById(R.id.encodingView);
        bodyView = findViewById(R.id.requestBodyView);
        fileExplanationView = findViewById(R.id.fileExplanationView);
        bodyView = findViewById(R.id.requestBodyView);
        resultView = findViewById(R.id.requestResultView);

        // prefill the fields using the preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        urlView.setText(prefs.getString("url", ""));
        methodView.setText(prefs.getString("method", "POST"));
        headersView.setText(prefs.getString("headers", ""));
        switch(prefs.getString("encoding", "urlEncoded")) {
            case "urlEncoded":
                encodingView.check(R.id.urlEncodedRadioButton);
                break;
            case "formdata":
                encodingView.check(R.id.formDataRadioButton);
                break;
            case "json":
                encodingView.check(R.id.jsonRadioButton);
                break;
            case "raw":
                encodingView.check(R.id.rawRadioButton);
                break;
        }
        bodyView.setText(prefs.getString("body", ""));

        // if a file is joined with ACTION_SEND
        if (getIntent().getAction() != null && getIntent().getAction().equals(Intent.ACTION_SEND)) {
            Uri uri = getIntent().getParcelableExtra(Intent.EXTRA_STREAM);
            if (uri != null) {
                fileUri = uri;
                fileExplanationView.setText("Use @file as value to include " + fileUri);
                fileExplanationView.setVisibility(View.VISIBLE);
            }
        }
    }

    /** Extract the map of parameters in a string */
    public Map<String, String> extractParams(String s) {
        Map<String, String> params = new HashMap<>();
        for (String line: s.split("\n")) {
            String[] components = line.split(":", 2);
            if (components.length == 2) {
                params.put(components[0].trim(), components[1].trim());
            }
        }
        return params;
    }

    /** Build the request using the values of fields */
    protected Request buildRequest() {
        Request.Builder rb = new Request.Builder()
                .url(urlView.getText().toString());
        RequestBody body = null; // to be built
        // add headers
        for (Map.Entry<String, String> entry: extractParams(headersView.getText().toString()).entrySet())
            rb.addHeader(entry.getKey(), entry.getValue());

        // manage encoding for the body
        String encoding = null;
        switch(encodingView.getCheckedRadioButtonId()) {
            case R.id.urlEncodedRadioButton:
                encoding = "urlEncoded";
                FormBody.Builder fbb = new FormBody.Builder();
                for (Map.Entry<String, String> entry: extractParams(bodyView.getText().toString()).entrySet())
                    fbb.add(entry.getKey(), entry.getValue());
                body = fbb.build();
                break;
            case R.id.formDataRadioButton:
                encoding = "formdata";
                MultipartBody.Builder mbb = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM);
                for (Map.Entry<String, String> entry: extractParams(bodyView.getText().toString()).entrySet())
                    if (entry.getValue().equals(FILE_PLACEHOLDER) && fileUri != null) {
                        // special treatment to join a file
                        // convert Uri to File
                        mbb.addFormDataPart(entry.getKey(), "file", new UriRequestBody(getContentResolver(), fileUri));
                    } else {
                        mbb.addFormDataPart(entry.getKey(), entry.getValue());
                    }
                body = mbb.build();
                break;
            case R.id.jsonRadioButton:
                encoding = "json";
                JSONObject json = new JSONObject(extractParams(bodyView.getText().toString()));
                body = RequestBody.create(MediaType.parse("application/json"), json.toString());
                break;
            case R.id.rawRadioButton:
                encoding = "raw";
                body = RequestBody.create(MediaType.parse("text/plain"), bodyView.getText().toString());
                break;
        }

        // save into the preferences
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putString("url", urlView.getText().toString());
        editor.putString("method", methodView.getText().toString());
        editor.putString("encoding", encoding);
        editor.putString("body", bodyView.getText().toString());
        editor.commit();

        return rb.build();
    }

    /** When one click on the send button */
    public void onSendClick(View v) {
        // cancel the possible previous call
        if (currentCall != null) currentCall.cancel();
        currentCall = null;
        // Build the request
        Request r = buildRequest();
        OkHttpClient client = new OkHttpClient();
        currentCall = client.newCall(r);
        currentCall.enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                runOnUiThread( () -> {
                    // the call failed
                    resultView.setText("HTTP call failed with exception " + e);
                    currentCall = null;
                });
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                runOnUiThread( () -> {
                    String r = "Result code: " + response.code() + "\n";
                    // add the headers
                    for (kotlin.Pair<?,?> header: response.headers())
                        r += header.getFirst().toString() + ": " + header.getSecond().toString() + "\n";
                    r += "\n";
                    // read the body
                    try {
                        r += "\n" + response.body().string(); // could be dangerous if the body is too largge for memory
                    } catch (IOException e) {
                        r += "Cannot read the body due to exception " + e;
                    }
                    resultView.setText(r);
                    currentCall = null;
                });
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        // cancel the current call if not null
        if (currentCall != null) {
            currentCall.cancel();
            currentCall = null;
        }
    }
}
