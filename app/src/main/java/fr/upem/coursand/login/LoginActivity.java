// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import fr.upem.coursand.R;

/** An activity that asks the user to supply her login information.
 * The username and password are then sent back to the calling activity.
 * It illustrates bidirectional communication between activities.
 */
public class LoginActivity extends AppCompatActivity {

    private SharedPreferences prefs;
    private String service; // the service for which we login
    private EditText usernameView;
    private EditText passwordView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameView = findViewById(R.id.usernameView);
        passwordView = findViewById(R.id.passwordView);

        // read the calling intent
        service = getIntent().getStringExtra("service");
        TextView serviceView = findViewById(R.id.serviceView);
        serviceView.setText(service); // display the service name

        // maybe we have already saved into the preferences of the activity the login info for this service
        // in this case we prefill the input fields with the stored data
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String username0 = prefs.getString(service + ".username", null);
        String password0 = prefs.getString(service + ".password", null);
        if (username0 != null) usernameView.setText(username0);
        if (password0 != null) passwordView.setText(password0);

        // set the login info
        findViewById(R.id.loginButton).setOnClickListener( v -> {
            String username = usernameView.getText().toString();
            String password = passwordView.getText().toString();
            if (username.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Missing information", Toast.LENGTH_LONG).show();
            } else {
                CheckBox rememberCheckBox = findViewById(R.id.rememberCheckBox);
                SharedPreferences.Editor editor = prefs.edit(); // open a transaction for the preferences
                if (rememberCheckBox.isChecked()) {
                    // we store the login info into the prefs
                    editor.putString(service + ".username", username);
                    editor.putString(service + ".password", password);
                    editor.putLong(service + ".timestamp", System.currentTimeMillis());
                } else {
                    // we must forget the information already (or not) stored
                    editor.remove(service + ".username");
                    editor.remove(service + ".password");
                    editor.remove(service + ".timestamp");
                }
                // finally we commit the prefs transaction
                editor.commit();

                // create a result intent and send it back to the calling activity
                Intent resultIntent = new Intent();
                resultIntent.putExtra("service", service);
                resultIntent.putExtra("username", username);
                resultIntent.putExtra("password", password);
                setResult(RESULT_OK, resultIntent);

                // the job is done, we quit the activity
                finish();
            }
        });
    }


}
