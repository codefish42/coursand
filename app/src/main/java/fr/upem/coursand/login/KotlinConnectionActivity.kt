// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License
package fr.upem.coursand.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.registerForActivityResult
import androidx.appcompat.app.AppCompatActivity
import fr.upem.coursand.R

/** A fake connection activity that uses the [KotlinLoginActivity]
 * to retrieve login information and display them.
 * This is an example using startActivityForResult.
 */
class KotlinConnectionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connection)
        val supplyLoginButton = findViewById<Button>(R.id.supplyLoginButton)
        supplyLoginButton.setOnClickListener { v: View? ->
            val i = Intent(this, LoginActivity::class.java)
            i.putExtra("service", SERVICE_NAME)
            getResult.launch(i)
        }
    }

    private val getResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        val v = findViewById<TextView>(R.id.loginInfoView)
        if (it.resultCode == RESULT_OK) {
            it.data?.let { data ->
                val text = """
                Fetched login data: 
                Username: ${data.getStringExtra("username")}
                Password: ${data.getStringExtra("password")}
                
                """.trimIndent()
                v.text = text
            }
        } else if (it.resultCode == RESULT_CANCELED) {
            v.text = "cancelled"
        }
    }


    companion object {
        const val SERVICE_NAME = "coursandService"
    }
}