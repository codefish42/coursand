// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.login;

import android.content.Intent;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import fr.upem.coursand.R;

/** A fake connection activity that uses the {@link fr.upem.coursand.login.LoginActivity}
 * to retrieve login information and display them.
 * This is an example using startActivityForResult.
 */
public class ConnectionActivity extends AppCompatActivity {

    public static final String SERVICE_NAME = "coursandService";
    public static final int REQUEST_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);
        Button supplyLoginButton = findViewById(R.id.supplyLoginButton);
        supplyLoginButton.setOnClickListener( v -> {
            Intent i = new Intent(this, LoginActivity.class);
            i.putExtra("service", SERVICE_NAME);
            startActivityForResult(i, REQUEST_ID);
        });
    }

    /** Called back when we have the result of {@link fr.upem.coursand.login.LoginActivity} */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TextView v = findViewById(R.id.loginInfoView);
        if (requestCode == REQUEST_ID && resultCode == RESULT_OK) {
            String text = "Fetched login data: \n" +
                    "Username: " + data.getStringExtra("username") + "\n" +
                    "Password: " + data.getStringExtra("password") + "\n";
            v.setText(text);
        } else if (requestCode == REQUEST_ID && resultCode == RESULT_CANCELED) {
            v.setText("cancelled");
        }
    }
}
