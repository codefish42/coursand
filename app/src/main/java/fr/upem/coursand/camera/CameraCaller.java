// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.camera;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import fr.upem.coursand.R;

/**
 * An example of the use of Intent to launch the camera application to take pictures.
 * Note that this activity does not require any special permission since it does
 * not use directly the camera API.
 */
public class CameraCaller extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera_caller);
	}
	
	private final static int IMAGE_CAPTURE_REQUEST_CODE = 1;
	private final static int VIDEO_CAPTURE_REQUEST_CODE = 2;
	
	public void startCameraActivity(boolean video)
	{
		Intent intent = new Intent((video)?MediaStore.ACTION_VIDEO_CAPTURE:MediaStore.ACTION_IMAGE_CAPTURE);
		// We can also use MediaStore.ACTION_IMAGE_CAPTURE_SECURE to capture an image in lock mode
		// To capture a video, we use MediaStore.ACTION_VIDEO_CAPTURE
		File pictDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), this.getClass().getSimpleName());
		pictDir.mkdir();
		String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + (video ? ".mp4":".jpg");
		File dest = new File(pictDir, timestamp); // Destination file
		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(dest)); // Specify the destination file
		// Now, we start the picture capture activity
		try {
			startActivityForResult(intent, (video) ? VIDEO_CAPTURE_REQUEST_CODE : IMAGE_CAPTURE_REQUEST_CODE);
		} catch (ActivityNotFoundException e)
		{
			Log.e(getClass().getName(), "Cannot start the activity due to an exception", e);
			Toast.makeText(this, "An exception encountered: " + e, Toast.LENGTH_SHORT).show();
		}
	}

	public void requestPicture(View v)
	{
		startCameraActivity(false);
	}

	public void requestVideo(View v)
	{
		startCameraActivity(true);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode)
		{
		case IMAGE_CAPTURE_REQUEST_CODE:
			switch (resultCode)
			{
			case RESULT_OK:
				Toast.makeText(this, "Image saved to " + data.getData(), Toast.LENGTH_LONG).show();
				// ((ImageView)findViewById(R.id.imageCaptureView)).setImageURI(data.getData());
				break;
			case RESULT_CANCELED: default:
				Toast.makeText(this, "Capture of image failed", Toast.LENGTH_SHORT).show();
			}
			break;
		case VIDEO_CAPTURE_REQUEST_CODE:
			switch (resultCode)
			{
			case RESULT_OK:
				Toast.makeText(this, "Video saved to " + data.getData(), Toast.LENGTH_LONG).show();
				break;
			case RESULT_CANCELED: default:
				Toast.makeText(this, "Capture of video failed", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
