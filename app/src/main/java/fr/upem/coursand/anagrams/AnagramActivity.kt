// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.anagrams

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import fr.upem.coursand.anagrams.ui.theme.CoursandTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AnagramActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoursandTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    AnagramSolver()
                }
            }
        }
    }
}



@Composable
fun AnagramsDisplayer(finder: AnagramFinder, word: String) {
    val bestAnagrams = remember(word) { derivedStateOf { finder.findBestAnagrams(word, word.length - 4).toList() } }
    LazyColumn(Modifier.fillMaxWidth()) {
        items(bestAnagrams.value) { a -> Text(a) }
    }
}

const val DICTIONARY_PATH = "dictionaries/english-scowl-60.txt"

@Composable
fun AnagramSolver() {
    var question by remember { mutableStateOf("") }
    var finder by remember { mutableStateOf<AnagramFinder?>(null) }
    val context = LocalContext.current
    LaunchedEffect(Unit) {
        val builtFinder = withContext(Dispatchers.Default) {
            val dictionary = context.loadDictionary(DICTIONARY_PATH)
            AnagramFinder(dictionary)
        }
        finder = builtFinder
    }
    Column(Modifier.fillMaxWidth()) {
        TextField(modifier=Modifier.fillMaxWidth(), value = question, onValueChange = { question = it } )
        finder?.let { AnagramsDisplayer(it, question) } ?: run { Text("Loading dictionary...") }
    }
}
