// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.currency

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.work.*
import fr.upem.coursand.R
import kotlinx.android.synthetic.main.activity_currency.*
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit

//tag::main[]
class CurrencyActivity : AppCompatActivity() {

    companion object {
        const val JOB_NAME = "fetchingCurrencyRates"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency)

        WorkManager.getInstance(this).apply {
            val data = getWorkInfosForUniqueWorkLiveData(JOB_NAME)
            // create an observer to be informed about the status of the job
            val observer = Observer { list: List<WorkInfo> ->
                currencyJobStatus.text = if (list.isNotEmpty()) list[0].state.toString() else "unknown status"
                currencyRatesTextView.text = try {
                    val f = cacheDir.resolve(CurrencyFetchingWorker.RATES_FILENAME)
                    "Modification date: ${Date(f.lastModified())}\n\n${f.readText()}"
                } catch (e: IOException) {
                    "data not available yet"
                }
            }
            data.observe(this@CurrencyActivity, observer)
        }
    }

    fun startJob(v: View) {
        val constraints = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresBatteryNotLow(true)
                .build()

        // ask to fetch the currency rates every 15 minutes if network is available and battery not low
        val request = PeriodicWorkRequestBuilder<CurrencyFetchingWorker>(15L, TimeUnit.MINUTES)
                .setConstraints(constraints)
                .setBackoffCriteria(BackoffPolicy.LINEAR, 60L, TimeUnit.SECONDS)
                .addTag(JOB_NAME)
                .build()
        WorkManager.getInstance(this).apply {
            enqueueUniquePeriodicWork(JOB_NAME, ExistingPeriodicWorkPolicy.REPLACE, request)
        }
    }

    fun stopJob(v: View) {
        WorkManager.getInstance(this).apply {
            cancelUniqueWork(JOB_NAME)
        }
    }
}
//end::main[]
