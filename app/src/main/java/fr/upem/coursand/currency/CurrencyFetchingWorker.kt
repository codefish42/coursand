// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.currency

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.concurrent.futures.CallbackToFutureAdapter
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters
import com.google.common.util.concurrent.ListenableFuture
import com.android.volley.toolbox.Volley
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest


//tag::main[]
/** Worker that retrieves currency rates */
class CurrencyFetchingWorker(context: Context, params: WorkerParameters): ListenableWorker(context, params) {
    companion object {
        const val REQUEST_URL = "https://api.ratesapi.io/api/latest"
        const val RATES_FILENAME = "currencyRates.json"
    }

    override fun startWork(): ListenableFuture<Result> {
        return CallbackToFutureAdapter.getFuture { completer ->

            val stringRequest = StringRequest(Request.Method.GET, REQUEST_URL,
                { response -> // write the result to the RATES file
                    applicationContext.cacheDir.resolve(RATES_FILENAME).writeText(response)
                },
                    object : Response.ErrorListener {
                        override fun onErrorResponse(error: VolleyError) {
                            Toast.makeText(applicationContext, "Cannot fetch rates due to " + error, Toast.LENGTH_SHORT).show()
                            Log.e(this.javaClass.name, "Error while retrieving rates", error)
                            completer.set(Result.failure())
                        }
                    }
            )

            //Instantiate the RequestQueue and add the request to the queue
            val queue = Volley.newRequestQueue(applicationContext)
            queue.add(stringRequest)

            "currencyJob"
        }
    }
}
//end::main[]