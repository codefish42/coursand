// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.navigator

import android.webkit.WebSettings
import java.lang.reflect.Method

class WebSettingInfo(val setter: Method) {
    val name = setter.name.substring("set".length)
    val getter = WebSettings::class.java.getMethod("get$name")

    fun get(settings: WebSettings) = getter.invoke(settings) as Boolean
    fun set(settings: WebSettings, value: Boolean) { setter.invoke(settings, value)}
}

object WebSettingsUtils {
    private fun obtainSettingsInfo(): List<WebSettingInfo> {
        val k = WebSettings::class.java
        return k.methods.filter {
            it.name.startsWith("set") && it.parameterTypes.getOrNull(0) == Boolean::class.java
        }.mapNotNull {
            try {
                WebSettingInfo(it)
            } catch (e: NoSuchMethodException) {
                null
            }
        }
                .sortedBy { it.name }.toList()
    }

    val settingsInfo by lazy { obtainSettingsInfo() }

    fun getSettingInfo(name: String) = settingsInfo.find { it.name == name }
}