// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.navigator;

import android.Manifest;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import fr.upem.coursand.R;
import fr.upem.coursand.launcher.ActivityMetadata;

@ActivityMetadata(permissions={Manifest.permission.INTERNET})
public class NavigatorActivity extends AppCompatActivity {

    private WebView webView;
    private EditText urlView;
    private ImageView faviconView;

    static class State {
        String currentUrl = null;
    }

    private State state = new State();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigator);
        webView = findViewById(R.id.webView);
        urlView = findViewById(R.id.urlView);
        faviconView = findViewById(R.id.faviconView);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                urlView.setText(url);
                faviconView.setImageBitmap(favicon);
            }
        });

        // listener to capture enter key to change the displayed URL on the webview
        urlView.setOnKeyListener((v, keyCode, event) -> {
            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                changeUrl(urlView.getText().toString().trim());
                return true;
            }
            return false;
        });
        // to support configuration changes restart
        if (state.currentUrl != null)
            changeUrl(state.currentUrl);
    }

    protected void changeUrl(String newUrl) {
        state.currentUrl = newUrl;
        webView.loadUrl(newUrl);
    }

    /********
     * Code that follows displays a menu with the settings of WebSettings
     * The WebSettingInfo class and WebSettingsUtils singleton are implemented in this package
     * It relies on standard Java introspection to fetch all the setters of WebSettings
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        for (WebSettingInfo wsi: WebSettingsUtils.INSTANCE.getSettingsInfo())
            menu.add(wsi.getName()).setCheckable(true);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        WebSettings settings = webView.getSettings();
        int i = 0;
        for (WebSettingInfo wsi: WebSettingsUtils.INSTANCE.getSettingsInfo())
            menu.getItem(i++).setChecked(wsi.get(settings));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        WebSettings settings = webView.getSettings();
        WebSettingInfo wsi = WebSettingsUtils.INSTANCE.getSettingInfo(item.getTitle().toString());
        if (wsi != null) {
            wsi.set(settings, !wsi.get(settings)); // toggle the boolean
            return true;
        }
        return false;
    }
}
