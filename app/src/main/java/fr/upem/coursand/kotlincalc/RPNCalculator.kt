// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.kotlincalc

import java.util.*

typealias OperandType = Int // alias pour un type
typealias Operator = (OperandType, OperandType) -> OperandType

val OPERATORS = mapOf<String, Operator>(
        "+" to { x, y -> x + y },
        "-" to { x, y -> x - y },
        "*" to { x, y -> x * y },
        "/" to { x, y -> x / y})

sealed class ExprTree {
    // Propriété abstraite
    abstract val result: OperandType
}

data class Operation(val operator: Operator, val right: ExprTree, val left: ExprTree): ExprTree() {
    override fun toString() =
        "($left) ${OPERATORS.filter { it.value == operator }.keys.first()} ($right)"

    // Définition du getter de la propriété abstraite
    override val result get() = operator(left.result, right.result)
}

data class Operand(val value: OperandType): ExprTree() {
    override fun toString() = "$value"
    override val result get() = value
}

// Méthode d'extension pour construire un arbre d'expression depuis une liste de String
fun List<String>.buildExprTree(): ExprTree {
    val stack = Stack<ExprTree>()
    this.forEach {
        val element = it.trim() // it est le paramètre de la lambda
        val symbol = OPERATORS[element]
        val number = element.toIntOrNull()
        when {
            symbol != null -> stack.push(Operation(symbol, stack.pop(), stack.pop()))
            number != null -> stack.push(Operand(number))
            else -> throw IllegalArgumentException("Invalid input: $element")
        }

    }
    return if (stack.size == 1)
        stack[0]
    else
        throw IllegalArgumentException("The stack does not contain one element")
}

fun main(args: Array<String>) {
    System.`in`.bufferedReader().forEachLine {
        print( try { // try..catch block is a string expression
            val tree = it.trim().split(" ").buildExprTree()
            "$tree = ${tree.result}"
        } catch (e: IllegalArgumentException) { "Exception encountered: $e" })
    }
}
