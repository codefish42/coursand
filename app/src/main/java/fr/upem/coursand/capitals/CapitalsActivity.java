// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.capitals;

import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import fr.upem.coursand.R;

public class CapitalsActivity extends AppCompatActivity {

    /** Create a map associating each country to its capital city
     * The data are read from the res/raw/country_capitals.csvv file
     * Each line follows the format: country,capital
     * @return a map linking countries to capitals
     */
    public Map<String, String> loadCountryCapitals() {
        Map<String, String> map = new TreeMap<>();
        try (Scanner s = new Scanner(getResources().openRawResource(R.raw.country_capitals))) {
            while (s.hasNextLine()) {
                String line = s.nextLine().trim();
                if (!line.isEmpty()) {
                    String[] elements = line.split(",");
                    try {
                        map.put(elements[0], elements[1]);
                    } catch (Exception e) {
                        Log.w(getClass().getName(), "Cannot interpret line " + line);
                    }
                }
            }
        }
        return map;
    }

    private Map<String, String> countryCapitals;

    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capitals);

        // load the data
        countryCapitals = loadCountryCapitals();

        // create the array adapter for the spinner
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                new ArrayList<>(countryCapitals.keySet())) {

            /** Method called to display the selected item */
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                // we add a yellow background for the selected item
                v.setBackgroundColor(Color.YELLOW);
                return v;
            }

            /** Method called to display items from the list (non-selected) */
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                // we alternate white and gray backgrounds for the items of the list
                v.setBackgroundColor((position % 2 == 0)?Color.WHITE:Color.LTGRAY);
                return v;
            }
        };

        // set the array adapter on the spinner
        Spinner spinner = findViewById(R.id.countrySpinner);
        spinner.setAdapter(adapter);

        // install the selected item listener on the spinner
        final TextView capitalView = findViewById(R.id.capitalView);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // display the capital in the TextView
                String capital = countryCapitals.get(adapter.getItem(position));
                capitalView.setText(capital);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                capitalView.setText("No country selected");
            }
        });
    }
}
