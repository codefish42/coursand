// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.chatclient.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import fr.upem.coursand.R;
import fr.upem.coursand.chatclient.model.ChatMessage;
import fr.upem.coursand.chatclient.viewmodel.ChatMessagesViewModel;

public class ChatClientActivity extends AppCompatActivity {

    private ChatMessagesViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_client);
        viewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(this.getApplication())
                .create(ChatMessagesViewModel.class);
        // update the status bar with the offline reason
        viewModel.getOfflineReason().observe(this, offlineReason -> {
            TextView tv = findViewById(R.id.statusTextView);
            tv.setText(offlineReason != null ? offlineReason : "online");
        });
        // update the messages list
        viewModel.getChatMessages().observe(this, messages -> {
            TextView tv = findViewById(R.id.messagesView);
            tv.setText(messages.toString());
        });
    }

    private String getServerUrl() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("params", Context.MODE_PRIVATE);
        return prefs.getString("serverUrl", "");
    }

    private void changeServerUrl(String url) {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("params", Context.MODE_PRIVATE);
        prefs.edit().putString("serverUrl", url).commit();
    }

    private void displayServerUrlDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("URL of the chat server");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
        input.setText(getServerUrl());
        builder.setView(input);
        builder.setPositiveButton("OK", (dialog, which) -> changeServerUrl(input.getText().toString()));
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_server_client_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.setServerUrl) {
            displayServerUrlDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    public void sendMessage(View v) {
        EditText et = findViewById(R.id.messageToSendEditText);
        viewModel.sendMessage(new ChatMessage(et.getText().toString()));
        et.setText("");
    }
}