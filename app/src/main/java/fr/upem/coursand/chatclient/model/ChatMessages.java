// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.chatclient.model;

import android.os.SystemClock;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

/** A set of retrieved chat messages */
public class ChatMessages implements Serializable {
    public final long updateTimestamp;
    public final SortedMap<Long, ChatMessage> messages;

    public ChatMessages(SortedMap<Long, ChatMessage> messages) {
        this.updateTimestamp = System.currentTimeMillis();
        this.messages = Collections.unmodifiableSortedMap(new TreeMap<>(messages)); // create an immutable copy of the map
    }

    @Override
    public String toString() {
        return messages.values().stream().map(ChatMessage::toString).collect(Collectors.joining("\n"));
    }

    /** Save this object to an output stream (using the standard Java serialization) */
    public boolean saveToOutputStream(OutputStream os) {
        try (ObjectOutputStream oos = new ObjectOutputStream(os)) {
            oos.writeObject(this);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /** Read this object from an input stream (using the standard Java serialization) */
    public static ChatMessages loadFromInputStream(InputStream is) {
        try (ObjectInputStream ois = new ObjectInputStream(is)) {
            return (ChatMessages)ois.readObject();
        } catch (Exception e) {
            return null;
        }
    }
}
