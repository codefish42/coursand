// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.chatclient.connector;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Consumer;

import fr.upem.coursand.chatclient.model.ChatMessage;
import fr.upem.coursand.chatclient.model.ChatMessages;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okhttp3.internal.tls.OkHostnameVerifier;
import okio.ByteString;

/** Manage a websocket connection to the chat room server to retrieve and send messages */
public class ChatRoomConnector {
    private final String url;
    private final OkHttpClient webClient;

    public ChatRoomConnector(String url) {
        this.url = url;
        this.webClient = new OkHttpClient();
    }

    private boolean initialized = false;
    private WebSocket webSocket = null;
    private String offlineReason = "not initialized";


    private WebSocketListener webSocketListener = new WebSocketListener() {
        @Override
        public void onClosed(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
            super.onClosed(webSocket, code, reason);
            ChatRoomConnector.this.webSocket = null;
            if (receivalCallback != null) {
                ChatRoomConnector.this.webSocket = null;
                offlineReason = "normally closed";
                receivalCallback.accept(null); // to signal the closure of the socket
            }
        }

        @Override
        public void onClosing(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
            super.onClosing(webSocket, code, reason);
        }

        @Override
        public void onFailure(@NotNull WebSocket webSocket, @NotNull Throwable t, @Nullable Response response) {
            super.onFailure(webSocket, t, response);
            if (receivalCallback != null) {
                ChatRoomConnector.this.webSocket = null;
                offlineReason = t.getMessage();
                receivalCallback.accept(null); // to signal the closure of the socket
            }
        }

        @Override
        public void onMessage(@NotNull WebSocket webSocket, @NotNull String text) {
            super.onMessage(webSocket, text);
            processReceivedMessage(text);
        }

        @Override
        public void onMessage(@NotNull WebSocket webSocket, @NotNull ByteString bytes) {
            super.onMessage(webSocket, bytes);
            Log.d(getClass().getName(), "Received binary message");
        }

        @Override
        public void onOpen(@NotNull WebSocket webSocket, @NotNull Response response) {
            super.onOpen(webSocket, response);
            ChatRoomConnector.this.webSocket = webSocket;
            offlineReason = null;
        }
    };

    private void initWebSocket() {
        if (! initialized) {
            Request r = new Request.Builder().url(url).build();
            webClient.newWebSocket(r, webSocketListener);
            initialized = true;
        }
    }

    /** Map storing retrieved messages */
    private SortedMap<Long, ChatMessage> retrievedMessages = new TreeMap<>();

    private void processReceivedMessage(String text) {
        Log.d(getClass().getName(), "Received message: " + text);
        try {
            JSONObject obj = new JSONObject(text);
            String kind = obj.getString("kind");
            if (kind.equals("availableMessages")) {
                long first = obj.getLong("first");
                long last = obj.getLong("last");
                if (first != -1L)
                    for (long i = first; i <= last; i++)
                        if (! retrievedMessages.containsKey(i))
                            retrieveMessage(i);
                if (receivalCallback != null)
                    receivalCallback.accept(null);
            } else if (kind.equals("message")) {
                // a retrieved message
                ChatMessage cm = ChatMessage.fromJSONObject(obj);
                if (cm != null) {
                    retrievedMessages.put(cm.id, cm);
                    if (receivalCallback != null)
                        receivalCallback.accept(new ChatMessages(retrievedMessages));
                }
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), "Error while parsing " + text, e);
        }
    }

    private void retrieveMessage(long id) {
        initWebSocket();
        try {
            JSONObject obj = new JSONObject();
            obj.put("kind", "fetchMessage");
            obj.put("id", id);
            webSocket.send(obj.toString());
        } catch (JSONException e)
        {
            Log.e(getClass().getName(), "Error while trying to ask to retrieve message " + id, e);
        }
    }

    private Consumer<ChatMessages> receivalCallback;

    /** Order to be informed about new messages */
    public void startMessageReceival(Consumer<ChatMessages> callback) {
        receivalCallback = callback;
        initWebSocket();
    }

    public boolean sendMessage(ChatMessage message) {
        initWebSocket();
        try {
            JSONObject obj = new JSONObject();
            obj.put("kind", "sendMessage");
            obj.put("content", message.content);
            return webSocket.send(obj.toString()); // thread-safe
        } catch (JSONException e) {
            Log.e(getClass().getName(), "JSON exception", e);
            return false;
        }
    }

    /** Return if the websocket is offline (null it is online) */
    public String getOfflineReason() {
        return offlineReason;
    }

    public void close() {
        if (webSocket != null)
            webSocket.close(1000, "good bye");
    }
}
