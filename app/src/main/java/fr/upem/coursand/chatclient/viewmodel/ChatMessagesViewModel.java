// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.chatclient.viewmodel;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.Queue;

import fr.upem.coursand.chatclient.connector.ChatRoomConnector;
import fr.upem.coursand.chatclient.model.ChatMessage;
import fr.upem.coursand.chatclient.model.ChatMessages;

public class ChatMessagesViewModel extends AndroidViewModel {
    public static final long RETRY_CONNECTION_DELAY = 30000L; // in millis

    private ChatRoomConnector connector = null;
    private MutableLiveData<ChatMessages> chatMessages = new MutableLiveData<>();
    private MutableLiveData<String> offlineReason = new MutableLiveData<String>();
    private Queue<ChatMessage> sendMessageQueue = new LinkedList<>();
    private Handler handler = new Handler(); // to retry connection periodically
    private SharedPreferences prefs;



    public ChatMessagesViewModel(@NonNull Application application) {
        super(application);
        prefs = getApplication().getApplicationContext().getSharedPreferences("params", Context.MODE_PRIVATE);
        // reinitialize the connector if the url is specified by the user or if it is changed in the preferences
        prefs.registerOnSharedPreferenceChangeListener((sharedPreferences, key) -> { if (key.equals("serverUrl")) initConnector(); });
        ChatMessages restoredMessages = restoreBackupedMessages();
        if (restoredMessages != null) chatMessages.setValue(restoredMessages);
        initConnector();
    }

    private Runnable retryConnectionRunnable = () -> initConnector();

    private void initConnector() {
        String url = prefs.getString("serverUrl", null);
        if (url != null) {
            Log.i(getClass().getName(), "Trying to connect to the websocket " + url);
            handler.removeCallbacks(retryConnectionRunnable);
            if (connector != null) connector.close(); // close the previous connector
            offlineReason.setValue("Connecting to " + url);
            connector = new ChatRoomConnector(url);
            // when new messages are received, post the value to the live data (that is observed by the activity)
            connector.startMessageReceival((ms) -> {
                offlineReason.postValue(connector.getOfflineReason());
                if (ms == null) {
                    // the socket has been closed
                    if (connector.getOfflineReason() != null)
                        handler.postDelayed(retryConnectionRunnable, RETRY_CONNECTION_DELAY); // retry to connect later
                } else {
                    chatMessages.postValue(ms);
                }
            });
        }
    }

    public LiveData<ChatMessages> getChatMessages() {
        return chatMessages;
    }

    public LiveData<String> getOfflineReason() {
        return offlineReason;
    }

    /** Add the message to the queue */
    public void sendMessage(ChatMessage message) {
        sendMessageQueue.add(message);
        flushMessageQueue();
    }

    /** Ask the connector to send the queued messages (if the connector is initialized) */
    private void flushMessageQueue() {
        if (connector != null && connector.getOfflineReason() == null)
            while (! sendMessageQueue.isEmpty()) {
                ChatMessage cm = sendMessageQueue.peek();
                if (connector.sendMessage(cm))
                    sendMessageQueue.poll(); // remove the message from the queue
                else break; // the websocket cannot send the messages
            }
    }

    private void backupMessages() {
        ChatMessages cms = chatMessages.getValue();
        // backup the messages
        if (cms != null) {
            Log.i(getClass().getName(), "Backup " + cms.messages.size() + " messages");
            try (OutputStream os = getApplication().getApplicationContext().openFileOutput("cachedMessages", Context.MODE_PRIVATE)) {
                cms.saveToOutputStream(os);
            } catch (IOException e) {
                Log.e(getClass().getName(), "error while backuping messages", e);
            }
        }
    }

    private ChatMessages restoreBackupedMessages() {
        try (InputStream is = getApplication().getApplicationContext().openFileInput("cachedMessages")) {
            return ChatMessages.loadFromInputStream(is);
        } catch (IOException e) {
            Log.e(getClass().getName(), "error while loading messages", e);
            return null;
        }
    }

    /** Call when the viewmodel is destroyed (because the activity using it is finished) */
    @Override
    protected void onCleared() {
        Log.i(getClass().getName(), "Cleared!");
        if (connector != null)
            connector.close(); // close the websocket
        backupMessages();
        super.onCleared();
    }
}
