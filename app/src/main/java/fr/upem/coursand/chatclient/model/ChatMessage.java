// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.chatclient.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/** A message posted to the chatroom */
public class ChatMessage implements Serializable {
    public final long id;
    public final String author;
    public final long timestamp;
    public final String content;

    public ChatMessage(long id, String author, long timestamp, String content) {
        this.id = id;
        this.author = author;
        this.timestamp = timestamp;
        this.content = content;
    }

    public ChatMessage(String content) {
        this(-1L, null, -1L, content);
    }

    /** Create a ChatMessage object from JSON data */
    public static ChatMessage fromJSONObject(JSONObject obj) {
        try {
            return new ChatMessage(obj.getLong("id"),
                    obj.getString("author"),
                    obj.getLong("timestamp"),
                    obj.getString("content"));
        } catch (JSONException e) {
            // the input in malformed, we return null
            return null;
        }
    }

    @Override
    public String toString() {
        return String.format("%d: [%s] %s", id, author, content);
    }
}
