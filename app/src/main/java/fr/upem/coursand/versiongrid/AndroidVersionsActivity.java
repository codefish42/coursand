// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.versiongrid;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.upem.coursand.R;

/**
 * An activity showing a grid of the different versions of Android known by the system.
 * A randomly chosen color is given to each version.
 *
 * @author chilowi at u-pem.fr
 */
public class AndroidVersionsActivity extends Activity
{

    public static List<Pair<Integer, String>> getAndroidVersions()
    {
        try {
            List<Pair<Integer, String>> l = new ArrayList<>();
            for (Field f : Build.VERSION_CODES.class.getFields())
                l.add(Pair.create(f.getInt(null), f.getName()));
            return l;
        } catch (Exception e)
        {
            Log.e(AndroidVersionsActivity.class.getName(), "Exception encountered", e);
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_versions);

        GridView gv = findViewById(R.id.versionGridView);
        final List<Pair<Integer, String>> versions = getAndroidVersions();
        if (versions != null)
            gv.setAdapter(new ArrayAdapter<Pair<Integer, String>>(this, android.R.layout.simple_list_item_1)
            {
                final Random r = new Random();

                @Override
                public int getCount()
                {
                    return versions.size();
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent)
                {
                    if (convertView == null)
                        convertView = getLayoutInflater().inflate(R.layout.version_cell_layout, null);
                    // set the name of the version
                    TextView nameView = convertView.findViewById(R.id.versionNameTextView);
                    nameView.setText(versions.get(position).second);
                    // set the api level of the version
                    TextView apiView = convertView.findViewById(R.id.versionApiTextView);
                    apiView.setText("" + versions.get(position).first);
                    // randomize the background color of the view (choose RVB values between 128 and 255)
                    convertView.setBackgroundColor(Color.argb(255, 128 + r.nextInt(128), 128 + r.nextInt(128), 128 + r.nextInt(128)));

                    // if this version is the current installed version, put the textviews in bold
                    if (Build.VERSION.SDK_INT == versions.get(position).first) {
                        nameView.setTypeface(Typeface.DEFAULT_BOLD);
                        apiView.setTypeface(Typeface.DEFAULT_BOLD);
                    }

                    return convertView;
                }
            });
    }
}
