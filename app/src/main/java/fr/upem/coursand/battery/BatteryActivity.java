// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.battery;

import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Locale;

import fr.upem.coursand.R;

/**
 * This class presents the use of a BroadcastReceiver to receive a sticky broadcast
 * in order to display the status of the device battery.
 */
public class BatteryActivity extends Activity 
{
	private final BroadcastReceiver batteryReceiver =
			new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			// Fetch battery state data in the intent
			updateStatus(String.format(Locale.getDefault(),
				"Charge ratio: %f\nBattery technology: %s\nBattery temperature: %s\nBattery voltage: %d\n",
				(float)intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) / intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1), // charge ratio
				intent.getStringExtra(BatteryManager.EXTRA_TECHNOLOGY),
				intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1),
				intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1)),
				intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL, -1));
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_battery);
	}
	
	private void updateStatus(String text, int logo)
	{
		TextView view = findViewById(R.id.mainBatteryTextView);
		view.setText(text);
		ActionBar ab = getActionBar();
		if (ab != null)
			ab.setLogo(logo); // Set an icon for the battery state
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		registerReceiver(batteryReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();
		unregisterReceiver(batteryReceiver);
	}
}
