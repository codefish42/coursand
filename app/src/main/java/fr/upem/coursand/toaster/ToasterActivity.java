// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.toaster;

import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import java.util.Date;

import fr.upem.coursand.R;

public class ToasterActivity extends AppCompatActivity {

    private int lastSelectedItemId = -1;
    private String message = "no message";

    protected void updateMessage(int itemId) {
        String message = null;
        switch (itemId) {
            case R.id.showHelloWorldItem: message = "Hello World!"; break;
            case R.id.showDateItem: message = new Date().toString(); break;
            case R.id.showVersionItem: message = Build.VERSION.CODENAME; break;
            default: message = "42"; break;
        }
        lastSelectedItemId = itemId;
        this.message = message;
    }

    private final ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            getMenuInflater().inflate(R.menu.toaster_menu, menu);
            return true; // something is done
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            // put a check mark for the last selected item
            for (int i = 0; i < menu.size(); i++)
                menu.getItem(i).setChecked(menu.getItem(i).getItemId() == lastSelectedItemId);
            return true; // return false since nothing is done
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
           updateMessage(item.getItemId());
           mode.finish();
           return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            // executed when we exit from the action mode
            currentActionMode = null;
        }
    };

    private ActionMode currentActionMode = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toaster);
        Button toastButton = findViewById(R.id.toastButton);

        // display the toast on click
        toastButton.setOnClickListener( v-> Toast.makeText(this, message, Toast.LENGTH_SHORT).show());

        // display the context menu allowing to select the message to display
        // when we press a long click on the button
        toastButton.setOnLongClickListener( v -> {
            if (currentActionMode == null) {
                currentActionMode = startActionMode(actionModeCallback);
                return true;
            } else
                return false;
        });
    }
}
