// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.lifecycle;

import android.os.SystemClock;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import fr.upem.coursand.R;

public class LifeCycleActivity extends AppCompatActivity {

    private TextView logView;
    private long startTimestamp;

    protected void log(String message) {
        Log.i("lifeCycleEvent", message);
        logView.append((SystemClock.elapsedRealtime() - startTimestamp) + " ms, " + message + "\n");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        startTimestamp = SystemClock.elapsedRealtime(); // in millis
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_cycle);
        logView = findViewById(R.id.logView);
        log("onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();
        log("onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        log("onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        log("onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        log("onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        log("onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        log("onDestroy"); // will not see in the TextView
    }
}
