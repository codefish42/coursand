// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import fr.upem.coursand.R
import fr.upem.coursand.composelayouts.ui.theme.CoursandTheme
import kotlinx.coroutines.launch

/** An activity to test the scaffold layout */
class ScaffoldActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoursandTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    ScaffoldExample()
                }
            }
        }
    }
}

//tag::main[]
@Composable
fun ScaffoldExample() {
    var number by remember { mutableStateOf(0) }
    val scaffoldState = rememberScaffoldState()
    val coroutineScope = rememberCoroutineScope()

    fun updateValue(n: Int) {
        number = n
        if (n == 10) {
            // display a toast when we hit 10
            coroutineScope.launch { // using the `coroutineScope` to `launch` showing the snackbar
                // taking the `snackbarHostState` from the attached `scaffoldState`
                val snackbarResult = scaffoldState.snackbarHostState.showSnackbar(
                    message = "The value reached 10",
                    actionLabel = "Decrement"
                )
                when (snackbarResult) {
                    SnackbarResult.Dismissed -> Log.d("ScaffoldExample", "Dismissed")
                    SnackbarResult.ActionPerformed -> updateValue(9)
                }
            }
        }
    }

    fun setDrawerOpenState(value: Boolean) {
        coroutineScope.launch {
            scaffoldState.drawerState.apply {
                if (value) open() else close()
            }
        }
    }

    Scaffold(
       scaffoldState = scaffoldState,
       topBar = {
           TopAppBar(
               title = { Text("A beautiful title") },
               navigationIcon = { IconButton(onClick = { setDrawerOpenState(!scaffoldState.drawerState.isOpen) }) {
                   Icon(painter = painterResource(id = R.drawable.ic_menu), contentDescription = "menu") } },
               actions = {
                   // some actions for the top bar
                   Button(onClick = { updateValue(number - 1) }) { Text("-") }
                   Button(onClick = { updateValue(number + 1)}) { Text("+") }
               })
       },
       bottomBar = {
           BottomAppBar() {
               Text("Edit:")
               TextField("$number", onValueChange = {  number = it.toIntOrNull() ?: 0 })
           }
       },
       drawerContent = {
           LazyColumn() {
               items(100) {
                   val v = it
                   Text("$v",
                       modifier=Modifier.clickable { updateValue(v); setDrawerOpenState(false)  },
                       fontWeight = if (v == number) FontWeight.Bold else FontWeight.Normal )
               }
           }
       },
       floatingActionButton = {
            ExtendedFloatingActionButton(
                text = { Text("drawer") },
                onClick = {
                    // invert the state
                    setDrawerOpenState(!scaffoldState.drawerState.isOpen)
                }
            )
        }
    ) {
        Box(modifier=Modifier.fillMaxSize()) {
            Box(modifier=Modifier.align(Alignment.Center)) { Text("$number", fontSize = 100.sp) }
        }
    }
}
//end::main[]

@Preview(showBackground = true)
@Composable
fun PreviewScaffoldExample() {
    CoursandTheme {
        ScaffoldExample()
    }
}