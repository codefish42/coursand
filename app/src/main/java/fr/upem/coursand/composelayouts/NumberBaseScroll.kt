// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

object NumberBaseScroll {
    //tag::main[]
    @Composable
    fun NumberBaseScroll(n: Int, bases: Int) {
        val hScrollState = rememberScrollState()
        Row(Modifier.horizontalScroll(hScrollState)) {
            (2..bases).forEach { base ->
                val vScrollState = rememberScrollState()
                Column(Modifier.padding(5.dp).verticalScroll(vScrollState)) {
                    Text("Base $base")
                    (0..n).forEach { i ->
                        Text(i.toString(base))
                    }
                }
            }
        }
    }
    //end::main[]
}