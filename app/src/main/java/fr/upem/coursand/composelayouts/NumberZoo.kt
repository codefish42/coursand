// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

object NumberZoo {
    //tag::main[]
    fun decompose(n: Long): List<Long> {
        val factors = mutableListOf<Long>()
        var a = n
        var divider = 2L
        while (divider * divider <= a) {
            if (a % divider == 0L) {
                factors.add(divider)
                a /= divider
            } else {
                divider = if (divider == 2L) 3L else divider + 2
            }
        }
        if (a != 1L) factors.add(a)
        return factors
    }

    @Composable
    fun Decomposition(l: List<Long>) {
        LazyRow() {
            items(l) {
                Text(modifier=Modifier.padding(end = 200.dp).border(1.dp, color= Color.Red).padding(5.dp), text="$it")
            }
        }
    }

    @Composable
    fun NumberCard(number: Long) {
        val decomposition = decompose(number)
        val isPrime = decomposition.size == 1
        Card(Modifier.padding(15.dp), elevation = 10.dp) {
            Column(Modifier.fillMaxWidth().background(color = if (isPrime) Color.Yellow else Color.White)) {
                Text(text="$number", fontSize = 30.sp)
                Spacer(modifier = Modifier.height(5.dp))
                Decomposition(decomposition)
            }
        }
    }

    @Composable
    fun NumberZoo(range: LongRange) {
        LazyColumn() {
            items(count = (range.last - range.first + 1).toInt(), key = {it - range.first}) {
                NumberCard(range.first + it)
            }
        }
    }
    //end::main[]
}