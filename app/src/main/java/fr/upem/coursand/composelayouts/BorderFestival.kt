// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

object BorderFestival {
    //tag::main[]
    @Composable
    fun SuperBorderedText(text: String, modifier: Modifier = Modifier) {
        Text(text=text, modifier=modifier
            .border(width=2.dp, color= Color.Black)
            .padding(2.dp)
            .border(width=4.dp, color=Color.Yellow)
            .padding(4.dp)
            .border(width=8.dp, color=Color.Blue)
            .padding(20.dp)
        )
    }
    //end::main[]
}