// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Slider
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.google.android.material.slider.Slider
import java.io.IOException
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid

//tag::main[]
object DessertsGrid {
    private fun getDesserts(context: Context): List<String> {
        return context.assets.list("desserts")?.map { "desserts/${it}" } ?: listOf()
    }

    private fun Context.assetsToBitmap(path: String): ImageBitmap? {
        return try {
            with(assets.open(path)){
                BitmapFactory.decodeStream(this).asImageBitmap()
            }
        } catch (e: IOException) { null }
    }

    @Composable
    fun Dessert(assetPath: String) {
        val bitmap = LocalContext.current.assetsToBitmap(assetPath)
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            if (bitmap != null)
                Image(bitmap = bitmap, contentDescription = "Image for $assetPath")
            else
                Text("Cannot load $assetPath")
            Text("${assetPath.split("/").last()}", modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center)
        }
    }

    @ExperimentalFoundationApi
    @Composable
    fun DessertsGrid() {
        val context = LocalContext.current
        var minSizeByCol by remember { mutableStateOf(50f) }
        val desserts by remember { mutableStateOf(getDesserts(context)) }
        Column() {
            Slider(value=minSizeByCol, valueRange = 5f..200f, onValueChange={ minSizeByCol = it})
            LazyVerticalGrid(columns = GridCells.Adaptive(minSize = minSizeByCol.dp), contentPadding = PaddingValues(5.dp)) {
                items(desserts) { Dessert(it) }
            }
        }
    }
}
//end::main[]