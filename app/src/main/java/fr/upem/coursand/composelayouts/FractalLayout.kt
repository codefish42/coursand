// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import android.graphics.RectF
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.unit.Constraints

object FractalLayout {
    //tag::main[]
    fun computeFractalPositions(left: Float, top: Float, right: Float, bottom: Float, number: Int, row: Boolean = false): List<RectF> {
        return when {
            number == 0 -> listOf()
            row -> {
                listOf(RectF(left, top, (left+right)/2, bottom)) +
                        computeFractalPositions((left+right)/2, top, right, bottom, number-1, !row)
            }
            else -> {
                listOf(RectF(left, top, right, (top+bottom)/2)) +
                        computeFractalPositions(left, (top+bottom)/2, right, bottom, number-1, !row)
            }
        }

    }
    /**
     * A layout that is a container splitting the space in two rows of equal height,
     * then splitting the second row to two colons of equal height,
     * then splitting the second colon of the previous row to two rows,
     * and so on to display all the the children
     */
    @Composable
    fun FractalLayout(
        modifier: Modifier = Modifier,
        content: @Composable () -> Unit
    ) {
        Layout(
            modifier = modifier,
            content = content
        ) { measurables, constraints ->
            val positions = computeFractalPositions(0f, 0f,
                constraints.maxWidth.toFloat(), constraints.maxHeight.toFloat(), measurables.size)

            val placeables = measurables.mapIndexed { index, measurable ->
                // Measure each children
                measurable.measure(Constraints.fixed(
                        positions[index].width().toInt(), positions[index].height().toInt()))
            }
            // Set the size of the layout as big as it can
            layout(constraints.maxWidth, constraints.maxHeight) {
                // Place children in the parent layout
                placeables.forEachIndexed { index, placeable ->
                    // Position item on the screen
                    placeable.placeRelative(positions[index].left.toInt(),
                        positions[index].top.toInt()
                    )
                }
            }
        }
    }
    //end::main[]

    @Composable
    fun FractalLayoutTest(colors: List<Color>) {
        FractalLayout.FractalLayout(Modifier.fillMaxSize()) {
            colors.forEach { Box(Modifier.background(color=it)) {} }
        }
    }
}