// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.GenericShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import fr.upem.coursand.composelayouts.ui.theme.CoursandTheme

object CzechFlag {
    //tag::main[]
    @Composable
    fun CzechFlag() {
        val triangleShape = GenericShape { size, layoutDirection ->
            moveTo(0f, 0f)
            lineTo(size.width, size.height / 2)
            lineTo(0f, size.height)
            lineTo(0f, 0f)
            close()
        }
        Box(modifier=Modifier.fillMaxSize().border(width=Dp.Hairline, color=Color.Black)) {
            Column(Modifier.fillMaxSize()) {
                Box(Modifier.fillMaxWidth().weight(1f/2f, fill=true).background(color=Color.White))
                Box(Modifier.fillMaxWidth().weight(1f/2f, fill=true).background(color=Color.Red))
            }
            Row(Modifier.fillMaxSize()) {
                Box(Modifier.fillMaxHeight().weight(1f/2f, fill=true).background(color=Color.Blue, shape=triangleShape))
                Box(Modifier.fillMaxHeight().weight(1f/2f, fill=true).background(color=Color.Transparent))
            }
        }
    }
    //end::main[]
}