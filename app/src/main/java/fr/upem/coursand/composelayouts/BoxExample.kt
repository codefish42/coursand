// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.material.Card
import androidx.compose.material.Slider
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex

/** An example for a card */
object BoxExample {
    //tag::main[]
    const val ALPHA_VALUE = 0.25f
    val BOX_COLORS = arrayOf(Color.Red, Color.Green, Color.Blue, Color.Yellow).map { it.copy(alpha = ALPHA_VALUE) }


    /** Display a card with an image at the bottom with layers of colors
     *  A slider can be used to change the z-index of the image to put it to the front
     */
    @Composable
    fun MultiLayeredBox(imageResource: Int) {
        var imageZIndex by remember { mutableStateOf(0f) }
        Box(modifier = Modifier.fillMaxSize()) {
            Image(modifier = Modifier.zIndex(imageZIndex).align(alignment = Alignment.Center), painter = painterResource(imageResource), contentDescription = "A beautiful image")
            BOX_COLORS.forEachIndexed { index, color ->
                Box(modifier = Modifier
                    .zIndex(index.toFloat() / BOX_COLORS.size)
                    .fillMaxSize()
                    .background(color = color)) {
                    Text(modifier = Modifier.offset(y = (index * 20).dp), text="Layer ${index}")
                }
            }
            Slider(modifier = Modifier.zIndex(2f).align(Alignment.BottomCenter), value = imageZIndex, onValueChange = { imageZIndex = it })
        }
    }
    //end::main[]
}