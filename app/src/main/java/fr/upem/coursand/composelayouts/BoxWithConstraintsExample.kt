// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Arrangement.SpaceAround
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

object BoxWithConstraintsExample {
    //tag::main[]
    @Composable
    fun NumberCard(number: Int) {
        Card(modifier=Modifier.size(100.dp)) {
            Text(modifier=Modifier.fillMaxWidth(), text="$number", textAlign = TextAlign.Center)
        }
    }

    @Composable
    fun NumberGrid(range: IntRange) {
        BoxWithConstraints() {
            val numberByRow = maxWidth.div(100.dp).toInt()
            val scrollState = rememberScrollState()
            Column(modifier = Modifier.fillMaxWidth().verticalScroll(scrollState).background(Color.Red)) {
                range.chunked(numberByRow).forEach { elements ->
                    Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = SpaceAround) {
                        elements.forEach { NumberCard(it) }
                    }
                }
            }
        }
    }
    //end::main[]
}