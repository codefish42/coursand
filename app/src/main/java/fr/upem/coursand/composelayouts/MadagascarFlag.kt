// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import fr.upem.coursand.composelayouts.ui.theme.CoursandTheme

object MadagascarFlag {
    //tag::main[]
    @Composable
    fun MadagascarFlag() {
        Row(modifier=Modifier.fillMaxSize().border(width=Dp.Hairline, color=Color.Black)) {
            Box(Modifier.fillMaxHeight().background(color=Color.White).weight(1f/3f, fill=true)) {  }
            Column(Modifier.fillMaxHeight().weight(2f/3f, fill=true)) {
                Box(Modifier.fillMaxWidth().background(color=Color.Red).weight(1f/2f, fill=true)) {}
                Box(Modifier.fillMaxWidth().background(color=Color.Green).weight(1f/2f, fill=true)) {}
            }
        }
    }
    //end::main[]
}