// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import fr.upem.coursand.desserts.Dessert
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview

//tag::main[]
@Composable
fun RenderMapWithSubcompose(map: Map<String, @Composable () -> Unit>) {
    SubcomposeLayout { constraints ->
        val keyWidths = map.keys.associateWith { key ->
            subcompose("subcompose_$key") {
                Text(key)
            }[0].measure(Constraints()).width.toDp()
        }
        val maxWidth = keyWidths.values.maxOrNull() ?: 0.dp
        val contentPlaceable = subcompose("content") {
            LazyColumn(Modifier.fillMaxWidth()) {
                items(map.entries.toList(), key = { it.key }) { entry ->
                        Row(Modifier.fillMaxWidth()) {
                            Text(modifier = Modifier.width(maxWidth), text = entry.key)
                            entry.value()
                        }
                }
            }
        }[0].measure(constraints)
        layout(contentPlaceable.width, contentPlaceable.height) {
            contentPlaceable.place(0, 0)
        }
    }
}

@Composable
fun DessertsTable() {
    val desserts = Dessert.loadAllDesserts(LocalContext.current, false)
    // must indicate the type since Kotlin does not seem to infer with the annotation
    val dessertsMap = desserts.associate<Dessert,String,@Composable () -> Unit>{
        it.name to @Composable {
            Image(it.getBitmap(LocalContext.current).asImageBitmap(), it.name)
        }
    }
    RenderMapWithSubcompose(dessertsMap)
}
//end::main[]

@Composable
@Preview
fun DessertsViewPreview() {
    DessertsTable()
}