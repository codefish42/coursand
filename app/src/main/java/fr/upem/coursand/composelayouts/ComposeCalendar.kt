// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Text
import androidx.compose.material.rememberDrawerState
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import okhttp3.internal.toImmutableList
import java.text.DateFormatSymbols
import java.time.LocalDate
import java.util.*

object ComposeCalendar {
    //tag::main[]
    @Composable
    fun Years(yearRange: IntRange, year: Int, onSelectedYear: (Int) -> Unit, modifier: Modifier = Modifier) {
        LazyColumn(modifier=modifier) {
            items(count = yearRange.last - yearRange.first) {
                val y = yearRange.first + it
                Text(modifier=Modifier.clickable {  onSelectedYear(y) }, text="$y", fontWeight = if (y == year) FontWeight.Bold else FontWeight.Normal)
            }
        }
    }

    private val MONTHS: Array<String> = DateFormatSymbols().months
    private val DAYS_OF_WEEK = DateFormatSymbols().shortWeekdays.toList().subList(1, 8).toImmutableList()

    @Composable
    fun Months(month: Int, onSelectedMonth: (Int) -> Unit, modifier: Modifier = Modifier) {
        LazyRow(modifier) {
            itemsIndexed(MONTHS) { index, item ->
                val m = index + 1
                Text(modifier= Modifier
                    .padding(5.dp)
                    .clickable { onSelectedMonth(index + 1) }, text="${MONTHS[index]}", fontWeight = if (month == m) FontWeight.Bold else FontWeight.Normal)
            }
        }
    }

    @Composable
    fun Month(date: LocalDate, modifier: Modifier = Modifier) {
        val d = date.withDayOfMonth(1)
        val weeks = IntArray(5 * 7) // a month can contain at most 5 different weeks
        val offset = d.dayOfWeek.value - 1
        Column(modifier) {
            // header with the names of the days of week
            Row(Modifier.fillMaxWidth()) {
                DAYS_OF_WEEK.forEachIndexed { index, _ -> Text(modifier=Modifier.weight(1f), text= DAYS_OF_WEEK[(index+8) % 7]) }
            }
            // days of the month
            (0 until 6).forEach { week ->
                val firstDayOfWeek = week * 7 - offset + 1
                if (firstDayOfWeek <= d.lengthOfMonth())
                    Row(Modifier.fillMaxWidth()) {
                        DAYS_OF_WEEK.forEachIndexed { dayIndex, _ ->
                            val day = firstDayOfWeek + dayIndex
                            if (day >= 1 && day <= d.lengthOfMonth())
                                Text(text="$day", modifier=Modifier.width(0.dp).weight(1f))
                            else
                                Text("", modifier=Modifier.width(0.dp).weight(1f))
                        }
                    }
            }
        }
    }


    @Composable
    fun ComposeCalendar(modifier: Modifier = Modifier) {
        val initialDate by remember { mutableStateOf(LocalDate.now()) }
        var date by remember { mutableStateOf(LocalDate.now()) }
        ConstraintLayout(modifier) {
            val (years, months, month) = createRefs()
            Years(initialDate.year-50..initialDate.year+50, date.year, { date = date.withYear(it) },
                modifier = Modifier.constrainAs(years) {
                    top.linkTo(months.top)
                    bottom.linkTo(month.bottom)
                    height = Dimension.fillToConstraints
                })
            Months(date.monthValue, { date = date.withMonth(it) }, modifier=Modifier.constrainAs(months) {
                top.linkTo(parent.top)
                start.linkTo(years.end, margin = 10.dp)
                end.linkTo(month.end)
                width = Dimension.fillToConstraints
            })
            Month(date, modifier=Modifier.constrainAs(month) {
                top.linkTo(months.bottom)
                start.linkTo(years.end, margin = 10.dp)
                end.linkTo(parent.end)
                width = Dimension.fillToConstraints
                height = Dimension.wrapContent
            })
        }
    }
    //end::main[]
}