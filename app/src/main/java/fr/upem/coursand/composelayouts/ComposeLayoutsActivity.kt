// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import fr.upem.coursand.R
import fr.upem.coursand.composelayouts.ui.theme.CoursandTheme
import kotlinx.coroutines.launch


val LAYOUT_REGISTRY: Map<String, @Composable () -> Unit> = mapOf(
    "Box" to @Composable { BoxLayout() },
    "NumberGrid" to @Composable { BoxWithConstraintsExample.NumberGrid(0..100) },
    "MadagascarFlag" to @Composable { Box(Modifier.size(300.dp, 200.dp)) { MadagascarFlag.MadagascarFlag() }},
    "CzechFlag" to @Composable { Box(Modifier.size(300.dp, 200.dp)) { CzechFlag.CzechFlag() }},
    "FractalLayout" to @Composable { FractalLayout.FractalLayoutTest(
        colors = listOf(Color.Red, Color.Yellow, Color.Blue, Color.Green, Color.Cyan, Color.Gray, Color.Black, Color.Magenta, Color.White)) },
    "MirroredText" to @Composable {
        Row() {
            DividerExample.MirroredText("Hello World everybody!", false)
            DividerExample.MirroredText("Hello World everybody!", true)
        }
    },
    "NumberZoo" to @Composable { NumberZoo.NumberZoo(range = 1L..1000L) },
    "ComposeCalendar" to @Composable { ComposeCalendar.ComposeCalendar() },
    "SuperBorderedText" to @Composable { BorderFestival.SuperBorderedText(text="Hello World!") },
    "NumberBaseScroll" to @Composable { NumberBaseScroll.NumberBaseScroll(100, 16) }
)

@Composable
fun MainLayout() {
    val scaffoldState = rememberScaffoldState()
    var selectedLayout by remember { mutableStateOf<String?>(null) }
    val scope = rememberCoroutineScope()

    Scaffold(
        scaffoldState = scaffoldState,
        drawerContent = { LayoutMenu(selectedLayout, { selectedLayout = it
            scope.launch { scaffoldState.drawerState.close() } }) },
        floatingActionButton = {
            ExtendedFloatingActionButton(
                text = { Text("drawer") },
                onClick = {
                    scope.launch {
                        scaffoldState.drawerState.apply {
                            if (isClosed) open() else close()
                        }
                    }
                }
            )
        }) {
            Box(Modifier.fillMaxSize()) {
                LAYOUT_REGISTRY[selectedLayout]?.invoke()
            }
        }
}

@Composable
fun LayoutMenu(selected: String?, onSelected: (String) -> Unit) {
    LAYOUT_REGISTRY.forEach {
        val s = it.key == selected
        Text(it.key, fontWeight = if (s) FontWeight.Bold else FontWeight.Normal, modifier=Modifier.clickable { onSelected(it.key) })
    }
}

class ComposeLayoutsActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CoursandTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    MainLayout()
                }
            }
        }
    }
}

@Composable
fun BoxLayout() {
    BoxExample.MultiLayeredBox(imageResource = R.drawable.mercator_world_map)
}

@Preview(showBackground = true)
@Composable
fun PreviewBoxLayout() {
    CoursandTheme {
        BoxLayout()
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewBoxWithConstraint() {
    CoursandTheme {
        BoxWithConstraintsExample.NumberGrid(0..100)
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewMadagascarFlag() {
    CoursandTheme {
        Box(Modifier.size(300.dp, 200.dp)) {
            MadagascarFlag.MadagascarFlag()
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewCzechFlag() {
    CoursandTheme {
        Box(Modifier.size(300.dp, 200.dp)) {
            CzechFlag.CzechFlag()
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewFractalLayoutTest() {
    CoursandTheme {
        FractalLayout.FractalLayoutTest(
            colors = listOf(Color.Red, Color.Yellow, Color.Blue, Color.Green, Color.Cyan, Color.Gray, Color.Black, Color.Magenta, Color.White)
        )
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewMirroredTextTest() {
    CoursandTheme {
        Row() {
            DividerExample.MirroredText("Hello World everybody!", false)
            DividerExample.MirroredText("Hello World everybody!", true)
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewNumberZoo() {
    CoursandTheme {
        NumberZoo.NumberZoo(range = 1L..1000L)
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewComposeCalendar() {
    CoursandTheme {
        ComposeCalendar.ComposeCalendar(Modifier.fillMaxWidth())
    }
}

@Preview(showBackground = true)
@Composable
fun PreviewSuperBorderedText() {
    CoursandTheme {
        BorderFestival.SuperBorderedText(text="Hello World!")
    }
}

@ExperimentalFoundationApi
@Preview(showBackground = true)
@Composable
fun PreviewDessertsGrid() {
    CoursandTheme {
        DessertsGrid.DessertsGrid()
    }
}