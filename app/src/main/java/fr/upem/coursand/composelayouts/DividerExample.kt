// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.composelayouts

import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

object DividerExample {
    //tag::main[]
    /** Display a text with a version rotated by 180 degrees.
     *  Inspired from the example at: https://developer.android.com/jetpack/compose/layouts/intrinsic-measurements#intrinsics-in-action
     */
    @Composable
    fun MirroredText(
        text: String,
        max: Boolean,
        modifier: Modifier = Modifier
    ) {
        Column(modifier = modifier.width(if (max) IntrinsicSize.Max else IntrinsicSize.Min)) {
            Text(modifier = Modifier.padding(4.dp), text=text)
            Divider(color = Color.Black, modifier = Modifier.fillMaxWidth().width(1.dp))
            Text(modifier = Modifier.padding(4.dp).rotate(180f), text=text)
        }
    }
    //end::main[]
}