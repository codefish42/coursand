// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.filechooser

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import fr.upem.coursand.R
import kotlinx.android.synthetic.main.activity_file_chooser.*

class FileChooserActivity : AppCompatActivity() {

    /** Return the MIME type for the selected radio button */
    private val selectedFileType: String get() {
        return when (fileTypeRadioGroup.checkedRadioButtonId) {
            R.id.imageRadioButton -> "image/*"
            R.id.videoRadioButton -> "video/*"
            R.id.AudioRadioButton -> "audio/*"
            R.id.textRadioButton -> "text/*"
            else -> "*/*"
        }
    }

    private val OPEN_REQUEST_CODE = 42

    private var selectedUri: Uri? = null
    private var activityMap: Map<String, Intent>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_file_chooser)

        // When we click on the button we open activity to choose a file matching the type selected with the radio button
        // using the Storage Access Framework
        chooserButton.setOnClickListener {
            Intent(Intent.ACTION_OPEN_DOCUMENT)
                    .addCategory(Intent.CATEGORY_OPENABLE)
                    .setType(selectedFileType)
                    .apply { startActivityForResult(this, OPEN_REQUEST_CODE) }
        }
        // we must write the onActivityResult callback
    }

    /** This methid is called back when we have the result of the file chosen with the SAF */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == OPEN_REQUEST_CODE && data != null && resultCode == Activity.RESULT_OK) {
            selectedUri = data.data
            chosenFileView.text = "${data.data}"
            // build the share Intent
            val shareIntent = Intent(Intent.ACTION_SEND)
                    .setType(contentResolver.getType(selectedUri!!))
                    .putExtra(Intent.EXTRA_STREAM, selectedUri)
            // get all the intents for all the activities that can send this file
            activityMap = shareIntent.resolveIntent(this)
            Log.i(javaClass.name, "activity map $activityMap")
            if (selectedUri != null) {
                activityRadioGroup.removeAllViews() // remove all the radio buttons
                activityMap?.map { val rb = RadioButton(this); rb.text = it.key; rb.tag = it.key; rb }
                        ?.forEachIndexed { i, b -> b.id = i; activityRadioGroup.addView(b) }

                // configure the button to start the selectedActivity
                sendToActivityButton.apply {
                    isEnabled = true
                    setOnClickListener {
                        // find the selected radio button
                        val rb = activityRadioGroup.findViewById<RadioButton>(activityRadioGroup.checkedRadioButtonId)
                        // start the activity for the selected intent
                        activityMap?.get(rb.tag?: "")?.also { startActivity(it) }
                    }
                }
            }
        }
    }
}
