// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.filechooser

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager

/**
 * This extension method for an implicit Intent find all the activities that are compatible
 * with the given Intent and create a map for all these activities with the Intent to launch them.
 *
 * Typical use to obtain all the activities for ACTION_SEND compatible with image/jpeg
 * val intentMap = Intent(android.content.Intent.ACTION_SEND).setType("image/jpeg").resolveIntent()
 */
fun Intent.resolveIntent(context: Context): Map<String, Intent> {
    // get a List<ResolveInfo>
    val resolveInfos = context.packageManager.queryIntentActivities(this, PackageManager.MATCH_ALL)
    return resolveInfos.map {
        val intent = this.clone() as Intent
        // set the class name for the Intent
        intent.setClassName(it.activityInfo.packageName, it.activityInfo.name)
        // return the label of the activity linked to the Intent to launch it
        it.activityInfo.name to intent
    }.sortedBy { it.first }.toMap()
}