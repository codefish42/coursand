// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.morse

import android.Manifest
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.upem.coursand.R
import fr.upem.coursand.launcher.ActivityMetadata
import kotlinx.android.synthetic.main.activity_morse.*

@ActivityMetadata(permissions= [Manifest.permission.CAMERA, "android.permission.FLASHLIGHT"])
class MorseActivity : AppCompatActivity() {

    val TIME_QUANTUM = 100L // in millis

    private fun createIntent(vibrator: Boolean) =
        Intent(this, if (vibrator) VibratorService::class.java else TorchService::class.java)

    // Create the Intent used to start the service
    private fun createStartIntent(): Intent? {
        val intent = when {
            vibratorView.isChecked -> createIntent(true).setAction(VibratorService.VIBRATION_ACTION)
            torchView.isChecked -> createIntent(false).setAction(TorchService.START_TORCH_ACTION)
            else -> null
        }
        val pattern = messageView.text.toString().toMorsePattern(TIME_QUANTUM)
        intent?.putExtra("pattern", pattern)
        intent?.putExtra("duration", durationView.text.toString().toLongOrNull()?.times(1000L) ?: -1L) // in millis
        return intent
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_morse)
        vibratorView.isEnabled = VibratorService.isVibratorAvailable(this)
        torchView.isEnabled = TorchService.isTorchAvailable(this)

        // start the service for vibration or torch (according to the checked radio button)
        startButton.setOnClickListener {
            val intent = createStartIntent()
            if (intent != null) startService(intent) }

        // stop all the services
        // calling stopService on a non-started service does nothing
        stopButton.setOnClickListener { listOf(false, true).forEach { stopService(createIntent(it)) } }
    }
}
