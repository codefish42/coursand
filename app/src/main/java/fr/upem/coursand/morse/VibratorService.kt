// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.morse

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Vibrator
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService

/** A service that execute vibration sequences */
class VibratorService : Service() {
    companion object {
        val VIBRATION_ACTION = javaClass.name + ".vibrate"

        /** Test if the vibrator is available on the current device */
        fun isVibratorAvailable(context: Context) =
                (context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator).let { it.hasVibrator() }
    }

    lateinit var vibrator: Vibrator
    lateinit var handler: Handler
    val vibrationEnder = Runnable { vibrator.cancel() }

    override fun onCreate() {
        super.onCreate()
        vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        handler = Handler() // create an handler to control the end of vibration
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action == VIBRATION_ACTION) {
            handler.removeCallbacks(vibrationEnder) // remove the planified end of previous vibration pttern
            vibrationEnder.run() // cancel now the possible current vibration pattern
            val pattern = intent.getLongArrayExtra("pattern")
            val duration = intent.getLongExtra("duration", -1) // duration in millis, -1 if infinite
            // execute the vibration pattern
            if (duration >= 0)
                Toast.makeText(this, "Start vibrator service for ${duration} seconds", Toast.LENGTH_SHORT).show()
            vibrator.vibrate(pattern, 0)
            if (duration != -1L) // schedule the end of vibration pattern with the handler
                handler.postDelayed(vibrationEnder, duration)
        }
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(vibrationEnder)
        vibrationEnder.run()
    }

    override fun onBind(intent: Intent): IBinder {
        throw NotImplementedError()
    }
}
