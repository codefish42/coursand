// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.morse

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.Camera
import android.hardware.camera2.CameraCharacteristics
import android.hardware.camera2.CameraManager
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import android.util.Log
import android.widget.Toast
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

class TorchService : Service() {
    companion object {
        val START_TORCH_ACTION = javaClass.name + ".startTorch"

        /** test if the torch is available on the current device and if the camera permission is granted */
        fun isTorchAvailable(context: Context) =
                context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH) &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    var camera: Camera? = null
    lateinit var executor: ExecutorService
    var currentTorchTask: Future<*>? = null

    override fun onCreate() {
        super.onCreate()
        executor = Executors.newSingleThreadExecutor() // create a thread executor to control the torch
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action == START_TORCH_ACTION) {
            val pattern = intent.getLongArrayExtra("pattern")!!
            val duration = intent.getLongExtra("duration", -1) // duration in millis, -1 if infinite
            if (duration >= 0)
                Toast.makeText(this, "Start torch service for ${duration} seconds", Toast.LENGTH_SHORT).show()
            currentTorchTask?.cancel(true) // cancel the current torch task if existing
            currentTorchTask = executor.submit {
                Log.i(javaClass.name, "Starting torch with pattern ${pattern}")
                try {
                    var index = 0
                    while (!Thread.interrupted()) {
                        when (index % 2) {
                            0 -> Thread.sleep(pattern[index])
                            1 -> activateTorch(pattern[index])
                        }
                        index = (index + 1) % pattern.size
                    }
                } finally {
                    closeTorch()
                }
            }
        }
        return START_NOT_STICKY
    }

    private fun activateTorch(duration: Long) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            activateTorch1(duration)
        else
            activateTorch2(duration)
    }

    /** Oldest way to activate torch for API < M */
    private fun activateTorch1(duration: Long) {
        if (camera == null) camera = Camera.open()
        Log.i(javaClass.name, "Camera open")
        if (duration > 0L)
            camera?.apply {
                val params = parameters
                params.flashMode = Camera.Parameters.FLASH_MODE_TORCH
                parameters = params
                startPreview()
                Log.i(javaClass.name, "Activating torch")
                try {
                    Thread.sleep(duration)
                    val params2 = parameters
                    params.flashMode = Camera.Parameters.FLASH_MODE_OFF
                    parameters = params2
                } finally {
                    stopPreview() // executed even if there is an InterruptedException
                    Log.i(javaClass.name, "Disabling torch")
                }
            }

    }


    /** New way to activate the torch for API >= M */
    @RequiresApi(Build.VERSION_CODES.M)
    private fun activateTorch2(duration: Long) {
        if (duration > 0L) {
            val cm = getSystemService(Context.CAMERA_SERVICE) as CameraManager
            if (cm != null) {
                val cameraId = cm.cameraIdList.find { cm.getCameraCharacteristics(it)[CameraCharacteristics.FLASH_INFO_AVAILABLE] == true }
                if (cameraId != null) {
                    cm.setTorchMode(cameraId, true)
                    try {
                        Thread.sleep(duration)
                    } finally {
                        cm.setTorchMode(cameraId, false)
                    }
                }
            }
        }
    }

    private fun closeTorch() {
        camera?.release()
        camera = null
    }

    override fun onDestroy() {
        super.onDestroy()
        executor.shutdownNow() // stop the running thread for the torch
    }


    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }
}
