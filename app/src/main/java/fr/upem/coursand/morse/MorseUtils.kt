// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.morse

/** This unit defines an extension method for String computing its Morse code
 *  and the timing pattern for this code
 */

/** Encode a String to Morse; note that some chars (not in [a-z0-9]) are not be supported (they are discarded) */
fun String.toMorse(): String {
    val s = this.trim().toLowerCase()
    return s.split(" ")
            .map { it.split("").map { MORSE_MAP.get(it) ?: ""}.joinToString(" ") }
            .joinToString("  ")
}

/** Translate a string to a timing pattern matching its Morse encoding */
fun String.toMorsePattern(timeQuantum: Long): LongArray {
    val morse = this.toMorse()
    return longArrayOf(0L) + morse
            .map {  when(it) {
                '.' -> longArrayOf(timeQuantum, timeQuantum)
                '-' -> longArrayOf(3 * timeQuantum, timeQuantum)
                ' ' -> longArrayOf(0, 2 * timeQuantum)
                else -> longArrayOf()
            }
            }.reduce { acc, longs -> acc + longs  }
}

val MORSE_MAP = mapOf("a" to ".-",
        "b" to "-...",
        "c" to "-.-.",
        "d" to "-..",
        "e" to ".",
        "f" to "..-.",
        "g" to "--.",
        "h" to "....",
        "i" to "..",
        "j" to ".---",
        "k" to "-.-",
        "l" to ".-..",
        "m" to "--",
        "n" to "-.",
        "o" to "---",
        "p" to ".--.",
        "q" to "--.-",
        "r" to ".-.",
        "s" to "...",
        "t" to "-",
        "u" to "..-",
        "v" to "...-",
        "w" to ".--",
        "x" to "-..-",
        "y" to "-.--",
        "z" to "--..",
        "0" to "-----",
        "1" to ".----",
        "2" to "..---",
        "3" to "...--",
        "4" to "....-",
        "5" to ".....",
        "6" to "-....",
        "7" to "--...",
        "8" to "---..",
        "9" to "----.")
