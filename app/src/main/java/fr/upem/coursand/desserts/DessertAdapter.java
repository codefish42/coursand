// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.desserts;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import fr.upem.coursand.R;

/** Adapter for RecyclerView displaying dessert pictures */
public class DessertAdapter extends RecyclerView.Adapter<DessertAdapter.ViewHolder> {

    private List<Dessert> desserts;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.dessertImageView);
            textView = itemView.findViewById(R.id.dessertLabel);
        }

        private void update(Dessert dessert) {
            imageView.setImageBitmap(dessert.getBitmap(imageView.getContext()));
            textView.setText(dessert.getName());
            // adding an action when we click on the textview (display a toast)
            textView.setOnClickListener(view -> {
                Toast t = Toast.makeText(textView.getContext(), "click on " + dessert.getName(), Toast.LENGTH_SHORT);
                t.show();
            });

        }
    }

    public DessertAdapter(List<Dessert> desserts) {
        super();
        this.desserts = desserts;
    }

    public List<Dessert> getDesserts() {
        return desserts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_dessert, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.update(desserts.get(i));
    }

    @Override
    public int getItemCount() {
        return desserts.size();
    }

    /** Exchange two items in the list */
    public void moveItem(int a, int b) {
        if (b == a);
        else {
            Dessert tmp = desserts.remove(a);
            desserts.add(b, tmp);
            notifyItemMoved(a, b);
        }
    }
}
