// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.desserts;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.annotation.NonNull;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** Android dessert picture */
public class Dessert implements Comparable<Dessert>, Serializable {

    private final String name;
    private final String assetLocation;

    /** Bitmap representation of the picture for the dessert */
    private transient Bitmap cachedBitmap;

    public Dessert(String name, String assetLocation) {
        this.name = name;
        this.assetLocation = assetLocation;
    }

    public String getName() {
        return name;
    }

    /** Get the bitmap for the picture in the assets */
    public Bitmap getBitmap(Context context) {
        if (cachedBitmap == null) {
            InputStream is = null;
            try {
                is = context.getAssets().open(assetLocation);
                cachedBitmap = BitmapFactory.decodeStream(is);
            } catch (IOException e) {
                try {
                    is.close();
                } catch (IOException ignored) { }
            }
        }
        return cachedBitmap;
    }

    @Override
    public int compareTo(@NonNull Dessert dessert) {
        return name.compareTo(dessert.name);
    }

    public static final String DESSERTS_ASSETS_LOCATION = "desserts";

    /** Load all the desserts from the assets */
    public static List<Dessert> loadAllDesserts(Context context, boolean shuffled) {
        List<Dessert> l = new ArrayList<>();
        try {
            for (String filename : context.getAssets().list(DESSERTS_ASSETS_LOCATION)) {
                String name = filename.substring(0, filename.indexOf("."));
                l.add(new Dessert(name, DESSERTS_ASSETS_LOCATION + "/" + filename));
            }
        } catch (IOException e)
        {
            Log.e(Dessert.class.getName(), e.getMessage(), e);
        }
        if (shuffled)
            Collections.shuffle(l);
        return l;
    }
}
