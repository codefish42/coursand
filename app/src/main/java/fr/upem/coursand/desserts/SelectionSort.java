// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.desserts;

import android.os.Handler;

import java.util.List;

/**
 * Simulate a selection sort using the adapter.
 */
public class SelectionSort {
    private DessertAdapter adapter;
    private Handler handler;
    private int period;
    private int step = 0;

    public SelectionSort(Handler handler, DessertAdapter adapter, int period) {
        this.adapter = adapter;
        this.handler = handler;
        this.period = period;
    }

    private Runnable getStepRunnable() {
        return stepRunnable;
    }

    private final Runnable stepRunnable = () -> {
        List<Dessert> dessertList = adapter.getDesserts();
        if (step >= dessertList.size()-1) return; // the end
        // find min
        int min = step;
        for (int i = step+1; i < adapter.getDesserts().size(); i++)
            if (dessertList.get(i).compareTo(dessertList.get(min)) < 0)
                min = i;
        if (min != step) {
            adapter.moveItem(min, step);
            step++;
            handler.postDelayed(getStepRunnable(), period);
        } else {
            step++;
            handler.post(getStepRunnable());
        }
    };

    public void start() {
        handler.post(getStepRunnable());
    }

    public void stop() {
        handler.removeCallbacks(getStepRunnable());
    }
}
