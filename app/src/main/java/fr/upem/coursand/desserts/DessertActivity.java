// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.desserts;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import fr.upem.coursand.R;

/** An activity displaying Android dessert demonstrating the use of {@link RecyclerView} */
public class DessertActivity extends Activity {

    private RecyclerView recyclerView;
    private DessertAdapter dessertAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dessert);
        recyclerView = findViewById(R.id.recyclerView);
        dessertAdapter = new DessertAdapter(Dessert.loadAllDesserts(this, true));
        installListeners();
        recyclerView.setAdapter(dessertAdapter);
        updateLayoutManager(null);
    }

    /** Install all the listeners to refresh when needed the LayoutManager */
    private void installListeners() {
        // for spanEditText
        EditText spanEditText = findViewById(R.id.spanEditText);
        spanEditText.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override public void afterTextChanged(Editable editable) { }
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { updateLayoutManager(spanEditText); }
        });
        // for staggeredCheckBox
        CheckBox staggeredCheckBox = findViewById(R.id.staggeredCheckBox);
        staggeredCheckBox.setOnCheckedChangeListener((compoundButton, b) -> updateLayoutManager(staggeredCheckBox));
        // for radio group
        RadioGroup orientationRadioGroup = findViewById(R.id.orientationRadioGroup);
        orientationRadioGroup.setOnCheckedChangeListener((radioGroup, i) -> updateLayoutManager(orientationRadioGroup));
    }

    /** Create the layout manager used by the recyclerView according to the user options */
    private RecyclerView.LayoutManager createLayoutManager() {
        RecyclerView.LayoutManager lm = null;
        int span = 1; // default valus is 1
        try {
            span = Integer.parseInt(((EditText)findViewById(R.id.spanEditText)).getText().toString());
            if (span <= 0) throw new RuntimeException("Invalid number: " + span);
        } catch (Exception e) {
            Log.e(getClass().getName(), "Text in the spanEditText is not a valid integer");
        }
        boolean staggered = ((CheckBox)findViewById(R.id.staggeredCheckBox)).isChecked();
        int orientation = 0;
        if (((RadioButton)findViewById(R.id.horizontalRadioButton)).isChecked())
            orientation = LinearLayoutManager.HORIZONTAL;
        else if (((RadioButton)findViewById(R.id.verticalRadioButton)).isChecked())
            orientation = LinearLayoutManager.VERTICAL;

        // Create the layout manager
        if (span == 1) // a classical linear layout is enough
            return new LinearLayoutManager(this, orientation, false);
        else if (! staggered)
            return new GridLayoutManager(this, span, orientation, false);
        else
            return new StaggeredGridLayoutManager(span, orientation);
    }

    private void updateLayoutManager(View v) {
        recyclerView.setLayoutManager(createLayoutManager());
    }

    public void sort(View v) {
        new SelectionSort(new Handler(), dessertAdapter, 2000).start();
        findViewById(R.id.sortButton).setEnabled(false);
    }
}
