// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.elapsedtimewidget

import android.app.LauncherActivity
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import android.widget.RemoteViews
import fr.upem.coursand.R

/** Implementation of an appwidget displaying the elapsed time since boot */
class ElapsedTimeAppWidgetProvider: AppWidgetProvider() {

    override fun onUpdate(
            context: Context,
            appWidgetManager: AppWidgetManager,
            appWidgetIds: IntArray
    ) {
        // Perform this loop procedure for each App Widget that belongs to this provider
        appWidgetIds.forEach { appWidgetId ->
            // Create an Intent to launch the main activity of Coursand
            val pendingIntent: PendingIntent = Intent(context, LauncherActivity::class.java)
                    .let { PendingIntent.getActivity(context, 0, it, 0) }

            // Get the layout for the App Widget
            // Update the elapsed time of the TextView and a click listener on it launching
            // the main activity of coursand
            val views: RemoteViews = RemoteViews(context.packageName, R.layout.widget_elapsedtime).apply {
                val elapsedMinutes = SystemClock.elapsedRealtime() / 1000 / 60 // in minutes
                this.setTextViewText(R.id.elapsedTimeView, "$elapsedMinutes minutes since boot")
                setOnClickPendingIntent(R.id.elapsedTimeView, pendingIntent)
            }

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}