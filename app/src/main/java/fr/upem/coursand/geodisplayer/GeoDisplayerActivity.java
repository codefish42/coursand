// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.geodisplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import fr.upem.coursand.R;
import fr.upem.coursand.launcher.ActivityMetadata;

@ActivityMetadata(permissions = {Manifest.permission.ACCESS_FINE_LOCATION})
public class GeoDisplayerActivity extends AppCompatActivity {

    // an arbitrary constant
    public static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_displayer);
    }

    /** When we click on the geolocate button */
    public void onGeolocateClick(View v) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // ask the permission to the user, this is an asynchronous call
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
        } else
            doGeolocation();
    }

    /** This method is called when the user has answered to the permission request */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            doGeolocation();
        } else
            Toast.makeText(this, "Permission is not granted for geolocation", Toast.LENGTH_SHORT).show();
    }

    protected void updateLocation(Location location) {
        TextView latitude = findViewById(R.id.latitudeTextView);
        TextView longitude = findViewById(R.id.longitudeTextView);
        TextView altitude = findViewById(R.id.altitudeTextView);
        TextView date = findViewById(R.id.dateTextView);
        latitude.setText("" + location.getLatitude());
        longitude.setText("" + location.getLongitude());
        altitude.setText(location.getAltitude() + " m");
        date.setText(new Date(location.getTime()).toString());
    }

    @SuppressLint("MissingPermission") // already checked by caller method
    protected void doGeolocation() {
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        // first fetch the last known location
        Location l = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (l != null) updateLocation(l);

        // ... and order a fresh location
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                updateLocation(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        lm.requestSingleUpdate(LocationManager.GPS_PROVIDER, locationListener, Looper.myLooper());
    }
}
