// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.digester

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import fr.upem.coursand.R
import kotlinx.android.synthetic.main.activity_digester.*

fun ByteArray.toHexString() = joinToString("") { "%02x".format(it) }

class DigesterActivity : AppCompatActivity() {

    var uri: Uri? = null
    private var digester: Digester? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_digester)
        uri = intent.data
        // usually the Uri is in the extra variable EXTRA_STREAM
        if (uri == null) uri = intent.getParcelableExtra(Intent.EXTRA_STREAM) as? Uri
    }

    override fun onStart() {
        super.onStart()
        val uri = this.uri
        if (uri != null) {
            // create an async task for digest computation
            digester = object : Digester() {

                override fun onPreExecute() {
                    uriView.text = uri.toString()
                    // display the metadata about the file
                    resultView.text = uri.getMetadata(this@DigesterActivity)?.map { "${it.key}=${it.value}" }?.joinToString("\n")
                }

                override fun onProgressUpdate(vararg values: Long?) {
                    // update the byte counter
                    statusView.text = "${values.last()} bytes processed"
                }

                override fun onPostExecute(result: Map<String, ByteArray>?) {
                    // display the result
                    if (result != null) {
                        val resultText = result.map { "${it.key}=${it.value.toHexString()}" }.joinToString("\n")
                        resultView.append(resultText)
                    } else
                        resultView.append("computation aborted")
                }
            }
            // start asynchronously the computation with the digester
            digester?.execute(contentResolver.openInputStream(uri))
        } else
            uriView.text = "This activity must be started with an Intent including a data URI"
    }

    // destroy the digest when the activity is destroyed
    // (we continue the computation if the activity is alive in the background)
    override fun onDestroy() {
        super.onDestroy()
        digester?.cancel(true)
    }
}
