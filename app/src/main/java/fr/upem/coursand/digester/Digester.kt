// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.digester

import android.os.AsyncTask
import java.io.InputStream
import java.security.MessageDigest
import java.security.Security

/** Compute digests of a stream of bytes */
open class Digester: AsyncTask<InputStream, Long, Map<String, ByteArray>>() {

    override fun doInBackground(vararg params: InputStream): Map<String, ByteArray>? {
        // the result is a map linking the name of digest algorithms to the digest results
        val result = mutableMapOf<String, String>()
        val algos = Security.getAlgorithms("MessageDigest")
        val hashers = algos.associateWith { MessageDigest.getInstance(it) }
        val buffer = ByteArray(2048)
        var bytes = 0L
        val inputStream = params[0] // stream to read
        inputStream.use {
            var r = inputStream.read(buffer)
            while (r != -1 && !isCancelled) {
                hashers.values.forEach { it.update(buffer, 0, r) }
                r = inputStream.read(buffer)
                if (r > 0) bytes += r // increment the counter of bytes
                publishProgress(bytes) // publish the counter of bytes for progress information
            }
            // finalize the digest
            return if (isCancelled) null else hashers.mapValues { it.value.digest() }
        }
    }
}