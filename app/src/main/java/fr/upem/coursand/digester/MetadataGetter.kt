// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.digester

import android.content.Context
import android.database.Cursor
import android.net.Uri
import java.io.Serializable

/** Get metadata of a file */
fun Uri.getMetadata(context: Context): Map<String, Serializable?>? {
    val cursor = context.contentResolver.query(this, null, null, null, null)
    // only one row should be returned
    if (cursor == null || !cursor.moveToFirst()) return null // no row available
    // there is now at least one row
    // read all the columns
    return cursor.use { cursor -> (0 until cursor.columnCount).map {
        cursor.getColumnName(it) to when (cursor.getType(it)) {
            Cursor.FIELD_TYPE_BLOB -> cursor.getBlob(it)
            Cursor.FIELD_TYPE_FLOAT -> cursor.getFloat(it)
            Cursor.FIELD_TYPE_INTEGER -> cursor.getInt(it)
            Cursor.FIELD_TYPE_STRING -> cursor.getString(it)
            else -> null
        }
    }.toMap() }
}