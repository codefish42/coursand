// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.flowchrono

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map

class FlowChronoViewModel: ViewModel() {
    private val flowChrono = FlowChrono()
    val chronoData: LiveData<Long> = liveData {
        // convert the elements of the flow to seconds (and removes the duplicates)
        flowChrono.getTimeFlow().map { it / 1_000_000_000L }.distinctUntilChanged().collect {
            emit(it)
        }
    }
}