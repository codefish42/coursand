// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.flowchrono

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import fr.upem.coursand.R
import fr.upem.coursand.paristraffic.TrafficViewModel

class FlowChronoActivity : AppCompatActivity() {
    private val viewModel: FlowChronoViewModel by viewModels()
    private var observer: Observer<Long>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flow_chrono)
    }

    // called when the activity is visible in foreground
    override fun onStart() {
        super.onStart()
        val et = findViewById<TextView>(R.id.chronoTextView)
        observer = viewModel.chronoData.observe(this) { et.text = "$it seconds" }
    }

    override fun onStop() {
        super.onStop()
        observer?.also { viewModel.chronoData.removeObserver(it) }
    }
}