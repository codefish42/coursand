// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.flowchrono

import android.util.Log
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FlowChrono {
    private val EMIT_DELAY = 100L // in ms

    private val startTime = System.nanoTime()

    fun getTimeFlow(): Flow<Long> = flow {
        try {
            while (true) {
                emit(System.nanoTime() - startTime) // in nanoseconds
                delay(EMIT_DELAY)
            }
        } finally {
            Log.d(javaClass.name, "End of the flow")
        }
    }
}