// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.paristraffic

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import kotlinx.coroutines.delay

class TrafficViewModel: ViewModel() {
    val trafficInfo: LiveData<String?> = liveData {
        delay(10000) // artificial delay (for testing and providing the ability to destroy the activity before fetching data)
        val text = getParisTrafficInfo()?.entries?.joinToString("\n") { "${it.key}: ${it.value}" }
        emit(text)
    }
}