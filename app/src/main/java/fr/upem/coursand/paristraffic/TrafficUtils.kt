// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.paristraffic

import android.util.Log
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.isActive
import kotlinx.coroutines.withContext
import java.io.IOException
import java.net.URL

const val TRAFFIC_INFO_URL = "https://api-ratp.pierre-grimaud.fr/v4/traffic"

fun parseJsonTrafficInfo(content: String): Map<String,String> {
    val rawMap = Gson().fromJson(content, Map::class.java)["result"] as? Map<*, *>
    val result = mutableMapOf<String, String>()
    rawMap?.entries?.forEach { entry ->
        val transportKind = entry.key.toString()
        val lines = entry.value as? List<*>
        lines?.mapNotNull { it as? Map<*, *> }?.forEach { line ->
            val lineName = line["line"].toString()
            val message = line["message"].toString()
            result["$transportKind $lineName"] = message
        }
        Unit
    }
    return result
}

suspend fun getParisTrafficInfo(): Map<String, String>? {
    Log.d("getParisTrafficInfo", "Starting info traffic retrieval")
    return try {
        val content = withContext(Dispatchers.IO) { // to execute the blocking IO code on the IO thread
            val connection = URL(TRAFFIC_INFO_URL)
            connection.readText()
        }
        Log.d("getParisTrafficInfo", "End of traffic info retrieval")
        parseJsonTrafficInfo(content)
    } catch (e: IOException) {
        Log.e("getParisTrafficInfo", "Error while retrieving traffic info", e)
        null
    }
}