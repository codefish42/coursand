// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.paristraffic

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import fr.upem.coursand.R

class ParisTrafficActivityWithViewModel : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paris_traffic)
        // the by viewModels() delegate requires the activity-ktx dependency
        val viewModel: TrafficViewModel by viewModels()
        viewModel.trafficInfo.observe(this, {
            Log.d(javaClass.name, "Receiving data in the observer of the LiveData: ${it?.length} chars")
            findViewById<TextView>(R.id.resultTextView).text = it ?: "no parsable data"
        })
    }
}