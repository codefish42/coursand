// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.paristraffic

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import fr.upem.coursand.R
import kotlinx.coroutines.*

class ParisTrafficActivity : AppCompatActivity() {
    private val cScope by lazy { CoroutineScope(Dispatchers.Main) } // scope on the main thread

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paris_traffic)
        cScope.launch {
            delay(10000) // artificial delay (for testing and providing the ability to destroy the activity before fetching data)
            val et = findViewById<TextView>(R.id.resultTextView)
            val text = getParisTrafficInfo()?.entries?.joinToString("\n") { "${it.key}: ${it.value}" }
            et.text = text
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // we cancel the scope (and the possible coroutine to retrieve traffic information)
        Log.i(javaClass.name, "Cancelling the scope")
        cScope.cancel()
    }
}