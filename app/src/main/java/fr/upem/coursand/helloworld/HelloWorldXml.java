// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.helloworld;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Date;

import fr.upem.coursand.R;

/**
 * A simple Hello World activity that uses a XML layout
 */
public class HelloWorldXml extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello_world_xml);
        ((TextView)findViewById(R.id.textView)).setText("Hello World at " + new Date());
    }

    /** should be linked with the onclick property of the button of the XML layout */
    public void quitActivity(View v) {
        finish();
    }

    public void startNewActivityInstance(View v) {
        startActivity(new Intent(this, HelloWorldXml.class));
    }
}
