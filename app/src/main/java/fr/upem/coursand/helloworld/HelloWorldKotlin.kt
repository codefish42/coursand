// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.helloworld

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup.LayoutParams
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import java.util.*


typealias Dictionary = TreeSet<String>
/**
 * An Hello World example programmed using Kotlin.
 * The layout is installed programmatically.
 */
class HelloWorldKotlin : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        // We must never forget to call the super method in every onX() method overridden in the activity
        super.onCreate(savedInstanceState)
        // We create ourselves the layout rather than loading it from a XML description
        val layout = LinearLayout(this) // New layout: container for the graphical elements
        layout.orientation = LinearLayout.VERTICAL
        val tv = TextView(this)
        val date = Date()
        tv.text = "Hello World at $date" // The string should be externalized as a resource
        tv.gravity = Gravity.CENTER
        val tvParams = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT /*width*/, LayoutParams.WRAP_CONTENT /*height*/, 1f/*weight*/)
        layout.addView(tv, tvParams)
        val b = Button(this)
        b.text = "Quit the activity"
        // We choose a weight of 1 for the TextView and 0 for the button (only the TextView will be resized)
        val buttonParams = LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 0f)
        layout.addView(b, buttonParams)
        val b2 = Button(this)
        b2.text = "Restart activity"
        layout.addView(b2, buttonParams)
        // We add a listener for the click event of the button to finish the activity
        b.setOnClickListener { this.finish() }
        // We add a listener on a button for the activity to start a new instance of itself
        b2.setOnClickListener { this.startActivity(Intent(this, HelloWorldKotlin::class.java)) }
        setContentView(layout)
    }
}