// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.screenrecorder

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.display.DisplayManager
import android.media.MediaRecorder
import android.media.projection.MediaProjection
import android.media.projection.MediaProjectionManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.RequiresApi
import android.util.DisplayMetrics
import android.hardware.display.VirtualDisplay
import android.os.Environment
import androidx.core.content.ContextCompat
import android.widget.Toast
import fr.upem.coursand.R
import fr.upem.coursand.launcher.ActivityMetadata
import kotlinx.android.synthetic.main.activity_screen_recorder.*
import java.io.File


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
@ActivityMetadata(permissions= [Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO], minApi=Build.VERSION_CODES.LOLLIPOP)
class ScreenRecorderActivity : AppCompatActivity() {
    val CAPTURE_REQUEST_ID = 1

    private val mediaProjectionManager by lazy { getSystemService(Context.MEDIA_PROJECTION_SERVICE) as MediaProjectionManager }

    private var mediaProjection: MediaProjection? = null

    private val displayMetrics by lazy {
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)
        metrics
    }

    private val mediaRecorder by lazy { MediaRecorder() }

    private var virtualDisplay: VirtualDisplay? = null

    private var mediaProjectionCallback = object: MediaProjection.Callback() {
        override fun onStop() {
            super.onStop()
            stopMediaRecorder()
        }
    }

    var name: String? = null
    private var temporaryFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_screen_recorder)
        startButton.setOnClickListener { createMediaProjector() }
        stopButton.setOnClickListener { stopMediaRecorder() }
        recordAudioView.isEnabled = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
        val recordable = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        startButton.isEnabled = recordable
        if (! recordable)
            Toast.makeText(this, "The permission WRITE_EXTERNAL_STORAGE is not granted", Toast.LENGTH_SHORT).show()
    }

    private fun createMediaProjector() {
        if (mediaProjection != null) return // nothing to do it is already created
        startActivityForResult(mediaProjectionManager.createScreenCaptureIntent(), CAPTURE_REQUEST_ID)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CAPTURE_REQUEST_ID && resultCode == Activity.RESULT_OK && data != null) {
            val mp = mediaProjectionManager.getMediaProjection(resultCode, data)
            mediaProjection = mp
            mp.registerCallback(mediaProjectionCallback, null)
            startMediaRecorder(nameView.text.toString())
        }
    }

    private fun startMediaRecorder(givenName: String) {
        startButton.isEnabled = false
        stopButton.isEnabled = true
        mediaRecorder.apply {
            val audio = recordAudioView.isChecked
            if (audio) setAudioSource(MediaRecorder.AudioSource.MIC)
            setVideoSource(MediaRecorder.VideoSource.SURFACE)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            val tmpFile = File.createTempFile("screenRecord", ".mp4", Environment.getExternalStorageDirectory())
            temporaryFile = tmpFile
            name = givenName
            setOutputFile(tmpFile.absolutePath)
            setVideoSize(displayMetrics.widthPixels, displayMetrics.heightPixels)
            setVideoEncoder(MediaRecorder.VideoEncoder.H264)
            if (audio) setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            setVideoEncodingBitRate(512 * 1000)
            setVideoFrameRate(30)
            prepare()
            virtualDisplay = mediaProjection?.createVirtualDisplay("screenRecorder",
                    displayMetrics.widthPixels, displayMetrics.heightPixels, displayMetrics.densityDpi,
                    DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR, mediaRecorder.surface, null, null)
            start()
        }
    }

    private fun stopMediaRecorder() {
        mediaRecorder.stop()
        mediaRecorder.reset()
        mediaProjection?.apply {
            unregisterCallback(mediaProjectionCallback)
            stop()
        }
        virtualDisplay?.release()
        virtualDisplay = null
        mediaProjection = null
        val uri = saveInMediaStore(MediaType.VIDEO, "video/mp4", name!!, temporaryFile!!)
        Toast.makeText(this, "Saved to $uri", Toast.LENGTH_SHORT).show()
        startButton.isEnabled = true
        stopButton.isEnabled = false
    }


}
