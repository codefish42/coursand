// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.screenrecorder

import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import java.io.File

enum class MediaType { IMAGE, AUDIO, VIDEO }

fun Context.saveInMediaStore(type: MediaType, mimeType: String, name: String, file: File, additionalMetadata: Map<String, String> = emptyMap()): Uri? {
    val contentValues = ContentValues()
    contentValues.put(MediaStore.MediaColumns.TITLE, name)
    contentValues.put(MediaStore.MediaColumns.DATE_ADDED, (System.currentTimeMillis() / 1000).toInt())
    contentValues.put(MediaStore.MediaColumns.MIME_TYPE, mimeType)
    contentValues.put(MediaStore.MediaColumns.DATA, file.absolutePath)
    additionalMetadata.forEach { contentValues.put(it.key, it.value) }

    val resolver = contentResolver

    val contentUri = when (type) {
        MediaType.IMAGE -> MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        MediaType.AUDIO -> MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        MediaType.VIDEO -> MediaStore.Video.Media.EXTERNAL_CONTENT_URI
    }
    return resolver.insert(contentUri, contentValues)
}

fun Context.openUri(uri: Uri) = contentResolver.openOutputStream(uri)