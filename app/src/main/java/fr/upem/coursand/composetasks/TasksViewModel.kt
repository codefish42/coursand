package fr.upem.coursand.composetasks

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel

class TasksViewModel: ViewModel() {
    var tasks = mutableStateListOf<Task>()
    private set

    fun createTask(description: String): Task {
        val t = Task.create(description)
        tasks.add(t)
        return t
    }

    fun removeTask(task: Task): Boolean {
        return tasks.remove(task)
    }

    fun completeTask(task: Task): Boolean {
        val pos = tasks.indexOf(task)
        return if (pos != -1) {
            tasks[pos] = task.complete
            true
        } else
            false
    }
}