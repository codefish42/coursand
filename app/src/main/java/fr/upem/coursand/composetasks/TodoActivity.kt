package fr.upem.coursand.composetasks

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import fr.upem.coursand.composetasks.ui.theme.TestAppTheme
import java.util.*

class TodoActivity : ComponentActivity() {
    private val tasksViewModel by viewModels<TasksViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TestAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    TasksScreen(tasksViewModel)
                }
            }
        }
    }
}

@Composable
fun TasksScreen(viewModel: TasksViewModel) {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
    var editing by remember { mutableStateOf(false) } // is a task being created (and edited)
    Scaffold(scaffoldState = scaffoldState,
        topBar = { TopAppBar(title = {Text("Tasks with compose")})  },
        floatingActionButtonPosition = FabPosition.End,
        floatingActionButton = {
            if (! editing) {
                FloatingActionButton(onClick = { editing = true }) { Text("+") }
            }
        },
        drawerContent = { Text("Nothing to say!") },
        content = { TasksListWithCreationBar(viewModel, editing, onEditingEnd = { editing = false }) }
    )
}

@Composable
fun TasksListWithCreationBar(viewModel: TasksViewModel, editing: Boolean, onEditingEnd: () -> Unit) {
    Column() {
        Card(modifier = Modifier.weight(1.0f)) { TasksList(viewModel) }
        if (editing) Card() { TaskCreationBar() { viewModel.createTask(it); onEditingEnd() } }
    }
}

@Composable
fun TaskCreationBar(onCreatedTask: (String) -> Unit) {
    var description by remember { mutableStateOf("") }
    Row(modifier = Modifier.border(width=1.dp, color= Color.Red)) {
        OutlinedTextField(
            modifier = Modifier.weight(1.0f),
            value = description,
            label = { Text("New task") },
            onValueChange = { description = it })
        Button(modifier = Modifier.align(CenterVertically), onClick = { onCreatedTask(description); description = "" }) {
            Text("Add")
        }
    }
}

@Composable
fun TasksList(viewModel: TasksViewModel) {
    LazyColumn() {
        items(viewModel.tasks, { it.id }) { task -> TaskCard(task,
            onCompletedTask = { viewModel.completeTask(task) },
            onRemovedTask = { viewModel.removeTask(task)}) }
    }
}

@Composable
fun ValidationDialog(title: String, text: String, onCancellation: () -> Unit, onValidation: () -> Unit) {
    AlertDialog(
        onDismissRequest = { onCancellation() },
        title = { Text(text = title) },
        text = { Text(text = text) },
        buttons = {
            Row(
                modifier = Modifier.padding(all = 8.dp),
                horizontalArrangement = Arrangement.Center
            ) {
                Button(onClick = onCancellation) { Text("No") }
                Button(onClick = onValidation) { Text("Yes") }
            }
        }
    )
}

@Composable
fun TaskCard(task: Task, onCompletedTask: () -> Unit, onRemovedTask: () -> Unit) {
    var deletionDialog by remember { mutableStateOf(false) }
    if (deletionDialog)
        ValidationDialog("Delete ?", "Do you to delete task $task ?",
            { deletionDialog = false },
            { deletionDialog = false; onRemovedTask() })
    Card(modifier = Modifier
        .padding(10.dp)
        .fillMaxWidth()) {
        val modifier = Modifier.pointerInput(Unit) {
            detectTapGestures (
                onLongPress = { deletionDialog = true }
            )
        }
        Row(modifier = modifier) {
            Checkbox(checked = task.isCompleted , onCheckedChange = {if (it) onCompletedTask()} )
            Spacer(modifier = Modifier.size(5.dp))
            TaskDetails(task)
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun TaskDetails(task: Task) {
    var expanded by rememberSaveable { mutableStateOf(false) }
    Column() {
        ClickableText(AnnotatedString(task.description),
            onClick = { expanded = !expanded })
        AnimatedVisibility(visible = expanded) {
            Column() {
                Text("Created on ${Date(task.creationDate)}")
                if (task.isCompleted)
                    Text("Completed on ${Date(task.completionDate)}")
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    val viewModel = TasksViewModel()
    val t1 = viewModel.createTask("First task")
    val t2 = viewModel.createTask("Second task")
    val t3 = viewModel.createTask("Third task")
    viewModel.completeTask(t2)
    TestAppTheme {
        TasksScreen(viewModel)
    }
}