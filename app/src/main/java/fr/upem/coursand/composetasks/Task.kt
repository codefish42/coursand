package fr.upem.coursand.composetasks

data class Task(val id: Int, val description: String, val creationDate: Long, val completionDate: Long = -1) {
    val isCompleted get() = completionDate > 0

    /** Create a copy of the task with the completionDate set to now */
    val complete get() = this.copy(completionDate = System.currentTimeMillis())

    companion object {
        private var COUNTER = 0

        fun create(description: String): Task {
            return Task(COUNTER++, description, System.currentTimeMillis(), -1)
        }
    }
}