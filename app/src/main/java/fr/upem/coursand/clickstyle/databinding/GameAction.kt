// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.clickstyle.databinding

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.BindingAdapter

object GameAction {

    /** Finish the current activity and start a new one with a new goal */
    private fun restartActivity(v: View, newGoal: Int) {
        val activity = v.context as Activity
        activity.finish()
        activity.startActivity(Intent(activity, DataBindingClickGame::class.java).putExtra("clickGoal", newGoal))
    }

    fun addClick(v: View, data: GameData) {
        data.incrementClick()
        Log.i(javaClass.name, "Clicks made: ${data.clickNumber}/${data.clickGoal}")
        if (data.isFinished) {
            Toast.makeText(v.context, "Congratulations, ${data.clickNumber} clicks made!", Toast.LENGTH_LONG).show()
            restartActivity(v, data.clickGoal + GameData.DEFAULT_CLICK_GOAL_INCREMENT)
        }
    }

    @BindingAdapter("greeness")
    @JvmStatic fun setGreeness(view: View, greeness: Float) {
        view.setBackgroundColor(Color.rgb(0, (greeness * 255).toInt(), 0))
    }
}