// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.clickstyle.databinding

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import fr.upem.coursand.BR

class GameData(_clickGoal: Int = DEFAULT_CLICK_GOAL): BaseObservable() {
    companion object {
        const val DEFAULT_CLICK_GOAL = 10
        const val DEFAULT_CLICK_GOAL_INCREMENT = 10
    }

    @Bindable
    var clickGoal: Int = _clickGoal


    @Bindable
    var clickNumber: Int = 0
        set(value) {
            field = value
            notifyPropertyChanged(BR.clickNumber)
        }

    @get:Bindable
    val isFinished: Boolean get() = clickNumber >= clickGoal


    fun incrementClick() {
        clickNumber += 1
    }
}