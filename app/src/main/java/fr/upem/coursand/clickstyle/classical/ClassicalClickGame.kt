// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.clickstyle.classical

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import fr.upem.coursand.R
import kotlinx.android.synthetic.main.activity_classical_click_game.*

class ClassicalClickGame : AppCompatActivity() {
    companion object {
        const val DEFAULT_CLICK_GOAL = 10
        const val DEFAULT_CLICK_GOAL_INCREMENT = 10
    }

    private var clickGoal: Int = DEFAULT_CLICK_GOAL
    private var clickNumber: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        clickGoal = intent.getIntExtra("clickGoal", DEFAULT_CLICK_GOAL)
        setContentView(R.layout.activity_classical_click_game)
        clickButton.setOnClickListener {
            clickNumber++
            clickTextView.text = "$clickNumber clicks"
            clickTextView.setBackgroundColor(Color.rgb(0, ((clickNumber.toFloat() / clickGoal) * 255).toInt(), 0))
            if (clickNumber == clickGoal) {
                Toast.makeText(this, "Congratulations, $clickNumber clicks made!", Toast.LENGTH_LONG).show()
                finish()
                Intent(this, ClassicalClickGame::class.java).apply {
                    putExtra("clickGoal", clickGoal + DEFAULT_CLICK_GOAL_INCREMENT)
                    startActivity(this)
                }
            }
        }
    }
}
