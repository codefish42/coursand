// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.clickstyle.databinding


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import fr.upem.coursand.R
import fr.upem.coursand.databinding.ActivityDataBindingClickGameBinding

class DataBindingClickGame : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityDataBindingClickGameBinding = DataBindingUtil.setContentView(this, R.layout.activity_data_binding_click_game)

        // create the gameData object containing the data displayed by the activity
        binding.gameData = GameData(intent.getIntExtra("clickGoal", GameData.DEFAULT_CLICK_GOAL))
        binding.gameAction = GameAction
    }

}
