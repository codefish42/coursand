// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.menu;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.upem.coursand.R;

/**
 * An activity displaying pictures from the assets directory.
 */
public class DiaporamaActivity extends Activity {

    /** Path of the pictures into the assets directory */
    public static final String PICTURES_PATH = "desserts";

    public static List<String> getPicturePaths(Context context) {
        List<String> result = new ArrayList<>();
        try {
            for (String filename : context.getResources().getAssets().list(PICTURES_PATH))
                result.add(PICTURES_PATH + "/" + filename);
        } catch (IOException e) {
            Log.e(DiaporamaActivity.class.getName(), "IOException while fetching picture paths", e);
        }
        return result;
    }

    /** Random number generator */
    private static final Random rng = new Random();

    private List<String> picturesPaths;

    /** The index of the currently displayed picture */
    private int currentPictureIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diaporama);
        picturesPaths = getPicturePaths(this);
        displayPicture(0);
    }

    private final int firstMenuItemID = Menu.FIRST;

    private Bitmap loadAssetBitmap(String path) {
        try {
            return BitmapFactory.decodeStream(getAssets().open(path));
        } catch (IOException e) {
            Log.e(this.getClass().getName(), "Cannot load bitmap for path " + path);
            return null;
        }
    }

    /** Display a picture on the ImageView */
    protected void displayPicture(int index) {
        if (index != currentPictureIndex) {
            ImageView iv = findViewById(R.id.pictureView);
            iv.setImageBitmap(loadAssetBitmap(picturesPaths.get(index)));
            currentPictureIndex = index;
        }
    }

    /** Called once in the life of the activity when the menu is created */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // load the menu from the XML file
        getMenuInflater().inflate(R.menu.diaporama_menu, menu);
        int i = 0;
        for (String path: picturesPaths) {
            int start = path.indexOf("/") + 1;
            int stop = path.lastIndexOf(".");
            String name = path.substring(start, stop);
            MenuItem mi = menu.add(R.id.directPictureAccess /* group id */, firstMenuItemID + i  /* item id */, 0 /* the order */, name /* title */);
            mi.setCheckable(true);
            mi.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER); // never put the individual picture items in the action bar
            i++;
        }
        return true; // do not forget to return true to display the menu
    }

    /** Called before each time the menu is displayed */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Check the correct item
        for (int i = firstMenuItemID; i < firstMenuItemID + picturesPaths.size(); i++)
            menu.findItem(i).setChecked(currentPictureIndex == i - firstMenuItemID);
        return true; // in order to display the menu
    }

    /** Called when an element of the menu is clicked */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.randomPictureAccess)
            displayPicture(rng.nextInt(picturesPaths.size()));
        else if (item.getItemId() == R.id.previousPictureAccess)
            displayPicture((currentPictureIndex - 1) % picturesPaths.size());
        else if (item.getItemId() == R.id.nextPictureAccess)
            displayPicture((currentPictureIndex + 1) % picturesPaths.size());
        else {
            // direct access to a picture
            int index = item.getItemId() - firstMenuItemID;
            if (index >= 0 && index < picturesPaths.size())
                displayPicture(index);
            else
                return false; // the menu item is not managed by our method (if we have forgotten a case)
        }
        return true;
    }
}
