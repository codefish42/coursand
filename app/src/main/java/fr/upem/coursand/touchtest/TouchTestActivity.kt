package fr.upem.coursand.touchtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import fr.upem.coursand.R
import kotlinx.android.synthetic.main.activity_touch_test.*

class TouchTestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_touch_test)
        touchTextView.setOnTouchListener { _, event ->
            when (event.actionMasked) {
                MotionEvent.ACTION_DOWN -> touchTextView.append("action_down: ${event.x}, ${event.y}\n")
                MotionEvent.ACTION_UP -> touchTextView.append("action_up: ${event.x}, ${event.y}\n")
                MotionEvent.ACTION_MOVE -> {
                    val s = (0 until event.pointerCount).map { event.getX(it) }.joinToString(";")
                    touchTextView.append("$s\n")
                }
            }
            return@setOnTouchListener true
        }
    }
}