// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.spiral;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

/**
 * Spiral animation activity implemented using a Handler.
 */

public class HandlerSpiralActivity extends ViewSpiralActivity
{
    private Handler handler = null;
    private final Runnable updater = this::periodicalUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
    }

    private void periodicalUpdate() {
        updateProgress();
        handler.postDelayed(updater, REFRESH_PERIOD);
    }

    @Override
    protected void startSpiralAnimation() {
        Log.i(getClass().getName(), "start animation");
        handler.post(updater);
    }

    @Override
    protected void stopSpiralAnimation() {
        Log.i(getClass().getName(), "stop animation");
        handler.removeCallbacks(updater);
    }
}
