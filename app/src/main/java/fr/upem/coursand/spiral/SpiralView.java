// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.spiral;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

/**
 * A view filling its space with a spiral.
 */
public class SpiralView extends View {

    /** Most of the work will be delegated to this drawable */
    private SpiralDrawable drawable = new SpiralDrawable();

    public SpiralView(Context context) {
        super(context);
    }

    public SpiralView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpiralView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    public void setLevelNumber(int levelNumber)
    {
        drawable.setLevelNumber(levelNumber);
        invalidate();
    }

    /** Set the number of filled levels (that could be a non integer number)
     * For a non-integer specified level, the last level is partially colored following a spiral path
     */
    public void setFilledLevels(float filledLevels)
    {
        drawable.setFilledLevels(filledLevels);
        invalidate();
    }

    public void setFillColor(int color)
    {
        drawable.setFillColor(color);
        invalidate();
    }



    /**
     * Paint the spiral on the component
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawable.draw(canvas);
    }


}
