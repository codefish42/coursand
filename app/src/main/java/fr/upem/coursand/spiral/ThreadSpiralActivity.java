// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.spiral;

/**
 * Spiral animation activity using a thread.
 */

public class ThreadSpiralActivity extends ViewSpiralActivity
{
    Thread animationThread = null;

    @Override
    protected void startSpiralAnimation()
    {
        animationThread = new Thread(() -> {
            while (! Thread.interrupted())
            {
                runOnUiThread(this::updateProgress);
                try {
                    Thread.sleep(REFRESH_PERIOD);
                } catch (InterruptedException e) {
                    return;
                }
            }
        });
        animationThread.start();
    }

    @Override
    protected void stopSpiralAnimation()
    {
        animationThread.interrupt();
    }
}
