// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.spiral;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import java.io.Serializable;

import fr.upem.coursand.R;


public abstract class AbstractSpiralActivity extends Activity
{
    public static final int LEVEL_NUMBER = 10;
    public static final float MAX_SPEED = 0.2f; // complete progress is done in 5 seconds
    public static final int REFRESH_PERIOD = 40; // in milliseconds

    protected static class State implements Serializable
    {
        float progressBySecond = 0.0f; // speed of the progress evolution
        float currentProgress = 0.0f; // current value of the progress (between 0.0 and 1.0)
        boolean decreasingPhase = false; // is the progress increasing or decreasing?
        long lastRefreshTimestamp = -1;
    }

    protected State state = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abstract_spiral);
        final SeekBar speedSeekBar = findViewById(R.id.speedSeekBar);
        // install listener
        speedSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                setAnimationSpeed((float)i / speedSeekBar.getMax() * MAX_SPEED);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        if (savedInstanceState != null)
            state = (State)savedInstanceState.getSerializable("state");
        if (state == null)
            state = new State(); // initial state value if no previous state has been retrieved
        ((ViewGroup)findViewById(R.id.spiralViewContainer)).addView(initView());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("state", state);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if (state.progressBySecond != 0.0f)
            stopSpiralAnimation();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (state.progressBySecond != 0.0f)
            startSpiralAnimation();
    }

    protected void setAnimationSpeed(float progressBySecond)
    {
        if (progressBySecond != state.progressBySecond)
        {
            Log.i(getClass().getName(), "setting animation speed to " + progressBySecond + "(previous speed: " + state.progressBySecond + ")");
            if (progressBySecond == 0.0f)
                stopSpiralAnimation();
            else if (progressBySecond != 0.0f && state.progressBySecond == 0.0f) {
                setProgress(state.currentProgress);
                startSpiralAnimation();
            }
            state.progressBySecond = progressBySecond;
        }
    }

    /**
     * Set the displayed progress.
     * @param progress progress of the animation
     */
    protected void setProgress(float progress)
    {
        state.currentProgress = progress;
        state.lastRefreshTimestamp = System.nanoTime();
        setProgressOnView(progress);
    }

    /** Increment or decrement progress */
    protected void updateProgress()
    {
        float deltaProgress = state.progressBySecond * (System.nanoTime() - state.lastRefreshTimestamp) / 1e9f;
        if (state.decreasingPhase) deltaProgress *= -1; // decrease progress in decreasing phase
        float newProgress = state.currentProgress + deltaProgress;
        if (newProgress > 1.0f)
        {
            newProgress = 1.0f - (newProgress - 1.0f);
            state.decreasingPhase = true;
        } else if (newProgress < 0.0f)
        {
            newProgress = -newProgress;
            state.decreasingPhase = false;
        }
        setProgress(newProgress);
    }

    protected abstract View initView();
    protected abstract void startSpiralAnimation();
    protected abstract void stopSpiralAnimation();
    protected abstract void setProgressOnView(float progress);
}
