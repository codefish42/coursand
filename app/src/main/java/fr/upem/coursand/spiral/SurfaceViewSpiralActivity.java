// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.spiral;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.util.concurrent.CountDownLatch;

/**
 * A spiral activity using a rendering thread with a SurfaceView
 */
public class SurfaceViewSpiralActivity extends AbstractSpiralActivity
{
    private SpiralDrawable drawable = new SpiralDrawable();
    private SurfaceHolder surfaceHolder = null;
    private CountDownLatch startLatch = new CountDownLatch(1);
    private Thread renderingThread = null;

    protected void renderer()
    {
        // we wait for the creation of the surface holder to start the rendering loop
        try {
            startLatch.await();
        } catch (InterruptedException e)
        {
            return; // interrupted before starting
        }
        while (! Thread.interrupted())
        {
            if (surfaceHolder.getSurface().isValid())
            {
                Canvas c = surfaceHolder.lockCanvas();
                try {
                    updateProgress();
                    drawable.draw(c);
                } finally {
                    surfaceHolder.unlockCanvasAndPost(c);
                }
            }
        }
    }

    /** Create the surface view */
    @Override
    protected View initView() {
        drawable.setFillColor(Color.RED);
        drawable.setLevelNumber(LEVEL_NUMBER);
        SurfaceView sv = new SurfaceView(this);
        sv.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                startLatch.countDown(); // we can order the rendering thread to do its work!
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

            }
        });
        surfaceHolder = sv.getHolder();
        return sv;
    }

    @Override
    protected void startSpiralAnimation() {
        renderingThread = new Thread(this::renderer);
        renderingThread.start();
    }

    @Override
    protected void stopSpiralAnimation() {
        renderingThread.interrupt();
    }

    @Override
    protected void setProgressOnView(float progress) {
        // it would be useful to synchronize on drawable
        // but we use a volatile field to save the progress on drawable
        // and we don't modify other properties in a multithread context
        // so the volatile modifier seems sufficient
        drawable.setFilledLevels(progress * LEVEL_NUMBER);
    }
}
