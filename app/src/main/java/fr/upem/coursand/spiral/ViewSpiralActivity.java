// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.spiral;

import android.graphics.Color;
import android.view.View;

/**
 * A spiral activity based on a normal view (SpiralView).
 * Note that this class is abstract since the methods to start and stop the animation are not implemented.
 *
 * @author chilowi at u-pem.fr
 */

public abstract class ViewSpiralActivity extends AbstractSpiralActivity
{
    protected SpiralView spiralView = null;

    protected View initView()
    {
        spiralView = new SpiralView(this);
        spiralView.setLevelNumber(LEVEL_NUMBER);
        spiralView.setFillColor(Color.RED);
        return spiralView;
    }

    protected void setProgressOnView(float progress)
    {
        spiralView.setFilledLevels(LEVEL_NUMBER * progress);
    }
}
