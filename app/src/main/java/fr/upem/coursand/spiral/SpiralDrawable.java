// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.spiral;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.Drawable;
import android.util.Log;

import androidx.annotation.Nullable;

/**
 * A drawable implementing a draw method painting a spiral on a canvas.
 *
 * @author chilowi at u-pem.fr
 */

public class SpiralDrawable extends Drawable
{
    private int levelNumber = 1; // number of levels in the spiral
    // volatile is a wise precaution in the case of a multithread use with a SurfaceView
    private volatile float filledLevels = 0.0f; // number of filled levels
    private int fillColor = Color.RED; // color used for filling

    public void setLevelNumber(int levelNumber)
    {
        this.levelNumber = levelNumber;
        setFilledLevels(0.0f);
    }

    /** Set the number of filled levels (that could be a non integer number)
     * For a non-integer specified level, the last level is partially colored following a spiral path
     */
    public void setFilledLevels(float filledLevels)
    {
        this.filledLevels = filledLevels;
    }

    public void setFillColor(int color)
    {
        this.plainPaint.setColor(color);
        this.plainPaint.setStyle(Paint.Style.FILL);
    }


    private Paint plainPaint = new Paint();
    private Paint transparentPaint = null;

    /**
     * Paint the spiral on the component
     */
    @Override
    public void draw(Canvas canvas) {
        float filledLevels = this.filledLevels;
        if (transparentPaint == null) {
            // initialize the first time
            transparentPaint = new Paint();
            transparentPaint.setColor(Color.TRANSPARENT);
            transparentPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        }
        float width = canvas.getWidth();
        float height = canvas.getHeight();
        float levelWidth = width / levelNumber / 2;
        float levelHeight = height / levelNumber / 2;

        // draw the complete levels
        // first paint all the component...
        canvas.drawRect(0, 0, width, height, plainPaint);
        // ... and render transparent the center of the component that must not be paint
        int completeLevels = (int)filledLevels;
        canvas.drawRect(completeLevels*levelWidth, completeLevels*levelHeight, width-completeLevels*levelWidth, height-completeLevels*levelHeight, transparentPaint);
        float partialLevel = filledLevels - completeLevels;

        // coordinates of the partial last level
        float x1 = levelWidth*completeLevels;
        float x2 = width - levelWidth*completeLevels;
        float y3 = levelHeight*(completeLevels+1);
        float y4 = height-levelHeight*completeLevels;
        float x5 = x1;
        float x6 = width - levelWidth*(completeLevels+1);
        float y7 = levelHeight*(completeLevels+1);
        float y8 = height - levelHeight*(completeLevels+1);

        float p1 = x2 - x1;
        float p2 = y4 - y3;
        float p3 = x6 - x5;
        float p4 = y8 - y7;
        float tot = p1 + p2 + p3 + p4;
        float a1 = Math.min(partialLevel, p1 / tot) / (p1 / tot);
        float a2  = Math.min(partialLevel - p1 / tot, p2 / tot) / (p2 / tot);
        float a3 = Math.min(partialLevel - p1 / tot - p2 / tot, p3 / tot) / (p3 / tot);
        float a4 = (partialLevel - p1 / tot - p2 / tot - p3 / tot) / (p4 / tot);
        Log.i(getClass().getName(), filledLevels + "," + partialLevel + "," + a1 + "," + a2 + "," + a3 + "," + a4);
        if (a1 > 0)
            canvas.drawRect(x1, levelHeight * completeLevels, x1 + p1 * a1, levelHeight * (completeLevels+1), plainPaint);
        if (a2 > 0)
            canvas.drawRect(width - levelWidth * (completeLevels+1), y3, width - levelWidth * completeLevels, y3 + p2 * a2, plainPaint);
        if (a3 > 0)
            canvas.drawRect(x5 + p3 * (1-a3), height - levelHeight * (completeLevels+1), x6, height - levelHeight * completeLevels, plainPaint);
        if (a4 > 0)
            canvas.drawRect(levelWidth * completeLevels, y7 + p4 * (1-a4), levelWidth * (completeLevels+1), y8, plainPaint);

    }

    @Override
    public void setAlpha(int i) {

    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }
}
