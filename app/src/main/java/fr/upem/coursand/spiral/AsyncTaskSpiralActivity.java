// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.spiral;

import android.os.AsyncTask;

public class AsyncTaskSpiralActivity extends ViewSpiralActivity
{
    private AsyncTask<Integer,?,?> task = null;

    @Override
    protected void startSpiralAnimation() {
        task = new AsyncTask<Integer, Object, Object>()
        {
            @Override
            protected Object doInBackground(Integer... params) {
                while (! isCancelled())
                {
                    publishProgress(null);
                    try {
                        Thread.sleep(params[0]);
                    } catch (InterruptedException e)
                    {
                        return null; // the task was interrupted
                    }
                }
                return null; // return value is not used
            }

            @Override
            protected void onProgressUpdate(Object... values) {
                updateProgress(); // update the displayed progress
            }
        };
        task.execute(REFRESH_PERIOD);
    }

    @Override
    protected void stopSpiralAnimation() {
        task.cancel(true);
    }
}
