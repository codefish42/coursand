// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.launcher;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


/** Annotation to tag activities with their required permissions */
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivityMetadata {
    String[] permissions();
    int minApi() default -1;
}
