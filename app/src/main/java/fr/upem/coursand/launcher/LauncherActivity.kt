// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.coursand.launcher

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import android.view.View
import android.view.ViewGroup
import android.widget.*
import fr.upem.coursand.R
import kotlinx.android.synthetic.main.activity_launcher.*

/**
 * An activity that is used to start other activities from the application package.
 * It is used as the main activity of the application.
 *
 * @author chilowi at u-pem.fr
 */
class LauncherActivity : Activity() {

    private val activities = mutableListOf<ActivityInfo>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
        val packageName = applicationContext.packageName
        activities += packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES).activities
        activities.sortBy { it.name.split('.').last() }
        activityListView.adapter = object: ArrayAdapter<ActivityInfo>(this, android.R.layout.simple_list_item_1, activities) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val v = (convertView ?: layoutInflater.inflate(R.layout.item_launcher, parent, false)) as LinearLayout
                val activityInfo = activities[position]
                val clazz = Class.forName(activityInfo.name)
                val titleView = v.findViewById<TextView>(R.id.activityNameView)
                var title = activityInfo.name.split(".").last()
                val minApi = clazz?.getAnnotation(RequiresApi::class.java)?.value
                val apiGranted = minApi == null || Build.VERSION.SDK_INT >= minApi
                if (minApi != null) title += " (API >= $minApi)"
                titleView.text = title
                titleView.isEnabled = apiGranted
                titleView.setOnClickListener {
                    startActivity(Intent().apply {
                        setClassName(activityInfo.packageName, activityInfo.name)
                    })
                }

                // display permissions
                val permissions = clazz?.getAnnotation(ActivityMetadata::class.java)?.permissions ?: emptyArray()
                v.findViewById<View>(R.id.permissionsOuterLayout).visibility = if (permissions.isNotEmpty()) View.VISIBLE else View.GONE
                val permissionLayout = v.findViewById<LinearLayout>(R.id.permissionsLayout)
                permissionLayout.removeAllViews()
                permissions.forEach {
                    val perm = it
                    val cb = CheckBox(this@LauncherActivity)
                    cb.text = it
                    val granted = ActivityCompat.checkSelfPermission(this@LauncherActivity, it) == PackageManager.PERMISSION_GRANTED
                    cb.isChecked = granted
                    cb.isClickable = !granted // do not allow unchecking for a granted permission
                    if (!granted)
                        cb.setOnClickListener {
                            // async call to request permissions
                            ActivityCompat.requestPermissions(this@LauncherActivity, arrayOf(perm), 1)
                            cb.isChecked = false // veto the check until the permission is confirmed
                        }
                    permissionLayout.addView(cb)
                }
                return v
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        (activityListView.adapter as ArrayAdapter<*>).notifyDataSetChanged()
    }
}
