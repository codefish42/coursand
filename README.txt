Coursand
Samples of code used in the Android course
http://igm.univ-mlv.fr/~chilowi/lectures/android/
by Michel Chilowicz <chilowi@u-pem.fr>

All the code is free software released under the terms of the Apache License v2.0
<http://www.apache.org/licenses/LICENSE-2.0>
Text of the license is available in the `app/src/main/assets/licenses/permissive/apache-2.txt` file