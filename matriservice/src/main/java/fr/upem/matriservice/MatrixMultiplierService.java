// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.matriservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;

import fr.upem.matrixlibrary.IMatrixMultiplier;
import fr.upem.matrixlibrary.Matrix;

/** A service providing multiplications on matrices */
public class MatrixMultiplierService extends Service 
{
	private final IBinder multiplier = new IMatrixMultiplier.Stub()
	{
		@Override
		public Matrix pow(Matrix m, int power) throws RemoteException
		{
			if (power == 0) return Matrix.createIdentity(m.getRows());
			if (power % 2 == 0)
			{
				Matrix m2 = pow(m, power / 2);
				return multiply(m2, m2);
			} else
			{
				// power % 2 == 1
				return multiply(m, pow(m, power-1));
			}
		}
		
		@Override
		public Matrix multiply(Matrix m1, Matrix m2) throws RemoteException 
		{
			if (m1.getRows() != m2.getCols())
				throw new IndexOutOfBoundsException("Dimension of matrices not compatible");
			Matrix r = new Matrix(m1.getRows(), m2.getCols());
			for (int i = 0; i < r.getRows(); i++)
				for (int j = 0; j < r.getCols(); j++)
				{
					int v = 0;
					for (int k = 0; k < m1.getCols(); k++)
						v += m1.get(i, k) * m2.get(k, j);
					r.set(i, j, v);
				}
			return r;
		}
	};
		
	@Override
	public IBinder onBind(Intent arg0) { return multiplier; }
}
