#! /usr/bin/env python3
"""
A simple websocket server that echoes all the text messages that are sent to it
(transforming them in uppercase)
"""

import asyncio
import websockets

async def echo(websocket, path):
    async for message in websocket:
        message2 = message.upper()
        await websocket.send(message)

def start(interface, port):
    asyncio.get_event_loop().run_until_complete(
    websockets.serve(echo, interface, port))
    asyncio.get_event_loop().run_forever()

def main():
    try:
        interface, port = (sys.argv[1], int(sys.argv[2]))
    except:
        print("Usage: %s interface port" % sys.argv[0])
        sys.exit(1)
    start(interface, port)

if __name__ == "__main__":
    main()
