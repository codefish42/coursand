#! /usr/bin/env python3
"""
A simple server managing a chatroom
"""

import asyncio, time, sys, json
import websockets

class Post(object):
	def __init__(self, author, content):
		self.author = author
		self.content = content
		self.timestamp = int(time.time()) # unix timestamp
		
class Posts(object):
	def __init__(self):
		self._elements = {}
		self._listeners = set() # to be called when there is a new post
		self.first_id = -1
		self.last_id = -1
	
	def get(self, id):
		return self._elements.get(id)
		
	def add(self, post: Post):
		if self.first_id == -1:
			self.first_id = 0
		self.last_id += 1
		self._elements[self.last_id] = post
		for listener in self._listeners:
			listener()
		
	def add_listener(self, listener):
		self._listeners.add(listener)
		
	def remove_listener(self, listener):
		self._listeners.remove(listener)


async def manage_websocket(posts, websocket, path):
	# the username is based on the IP address and port of the client
	username = "{}/{}".format(websocket.remote_address[0], websocket.remote_address[1])
	
	def on_new_post():
		# called to notify about a new post
		print("Sending available messages: {} {}".format(posts.first_id, posts.last_id), file=sys.stderr)
		asyncio.create_task(websocket.send(json.dumps({"kind": "availableMessages", "first": posts.first_id, "last": posts.last_id})))
	posts.add_listener(on_new_post)
	on_new_post() # call the listener for the first time
	
	async for message in websocket:
		# interpret the message as JSON
		try:
			message_dict = json.loads(message)
		except:
			message_dict = None
		kind = message_dict.get("kind", None) if message_dict else None
		if kind == "fetchMessage":
			id = message_dict.get("id", None)
			post = posts.get(id)
			if post:
				asyncio.create_task(websocket.send(json.dumps(
					{"kind": "message", "id": id, "author": post.author, "timestamp": post.timestamp, "content": post.content})))
		elif kind == "sendMessage":
			content = message_dict.get("content")
			if content:
				posts.add(Post(username, content))

def start(interface, port):
	posts = Posts()
	asyncio.get_event_loop().run_until_complete(
		websockets.serve(lambda websocket, path: manage_websocket(posts, websocket, path), interface, port))
	asyncio.get_event_loop().run_forever()
    
def main():
	try:
		interface, port = (sys.argv[1], int(sys.argv[2]))
	except:
		print("Usage: {} interface port".format(sys.argv[0]))
		sys.exit(1)
	start(interface, port)

if __name__ == "__main__":
	main()
