#! /usr/bin/env python3
"""
This simple Python script implements a very basic HTTP server
that receives files that are sent to it
and saves them in the current working directory (under a subdirectory with the date of the request).

Must only be used as a test server.

@author: chilowi at u-pem.fr
"""

import os, sys, re
from http.server import HTTPServer, BaseHTTPRequestHandler
from cgi import FieldStorage
from datetime import datetime
from socketserver import ThreadingMixIn

class Handler(BaseHTTPRequestHandler):
	def do_POST(self):
	    subdirectory = datetime.now().isoformat()
	    os.mkdir(subdirectory)
		fs = FieldStorage(self.rfile, self.headers, environ={'REQUEST_METHOD': 'POST'})
		if fs.list:
			for k in fs.keys():
				field = fs[k]
				filename = re.replace("[^A-Za-z0-9_]", "", field.filename) # remove unsafe characters
				if not os.path.exists(filename):
					with open(filename, "wb") as f:
						data = field.file.read()
						f.write(data)
		self.send_response(200)

def start(interface, port):
	class ThreadedHTTPServer(ThreadingMixIn, HTTPServer): pass
	httpd = ThreadedHTTPServer((interface, port), Handler)
	httpd.serve_forever()

if __name__ == "__main__":
	try:
		interface, port = (sys.argv[1], int(sys.argv[2]))
	except:
		print("Usage: %s interface port" % sys.argv[0])
		sys.exit(1)
	start(interface, port)

