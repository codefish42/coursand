#! /usr/bin/env python3
"""
A very simple HTTP server that prints the received requests
"""

import os, sys, re
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn

class RequestHandler(BaseHTTPRequestHandler):
    def treat_request(self):
        print(self.path)
        print(self.headers)
        print("\n")
        try:
            content_length = int(request_headers.getheaders('content-length')[0])
        except:
            content_length = 0
        print(self.rfile.read(length))
        self.send_response(200, "Test server")
    do_GET = treat_request
    do_POST = treat_request
    do_PUT = treat_request
    do_DELETE = treat_request

def start(interface, port):
    class ThreadedHTTPServer(ThreadingMixIn, HTTPServer): pass
    httpd = ThreadedHTTPServer((interface, port), RequestHandler)
    httpd.serve_forever()

def main():
    try:
        interface, port = (sys.argv[1], int(sys.argv[2]))
    except:
        print("Usage: %s interface port" % sys.argv[0])
        sys.exit(1)
    start(interface, port)


if __name__ == "__main__":
    main()