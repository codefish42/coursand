// Code sample from Coursand [http://igm.univ-mlv.fr/~chilowi/], under the Apache 2.0 License

package fr.upem.matrixlibrary;

import java.util.Arrays;

import android.os.Parcel;
import android.os.Parcelable;

/* A matrix of integers */
public class Matrix implements Parcelable
{
	private final int rows, cols;
	private final int[][] content;
	
	public Matrix(int rows, int cols)
	{
		this.rows = rows; this.cols = cols;
		this.content = new int[rows][];
		// Initialize the matrix
		for (int i = 0; i < rows; i++) this.content[i] = new int[cols];
	}
	
	public static Matrix createIdentity(int n)
	{
		Matrix m = new Matrix(n, n);
		for (int i = 0; i < n; i++) m.set(i, i, 1);
		return m;
	}
	
	public int getRows() { return rows; }
	public int getCols() { return cols; }
	public int get(int i, int j) { return content[i][j]; }
	public void set(int i, int j, int value) { content[i][j] = value; }
	
	@Override public int describeContents()  { return 0; }

	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
		dest.writeInt(rows);
		dest.writeInt(cols);
		// Write the matrix row by row
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				dest.writeInt(content[i][j]);
	}
	
	public static final Creator<Matrix> CREATOR =
		new Creator<Matrix>()
		{
			@Override
			public Matrix createFromParcel(Parcel source) 
			{
				int rows = source.readInt(), cols = source.readInt();
				Matrix m = new Matrix(rows, cols);
				for (int i = 0; i < rows; i++)
					for (int j = 0; j < cols; j++)
						m.set(i, j, source.readInt());
				return m;
			}

			@Override
			public Matrix[] newArray(int size) 
			{
				return new Matrix[size];
			}
		};

	@Override
	public String toString()
	{
		return Arrays.deepToString(content);
	}
}
