package fr.upem.matrixlibrary;

import fr.upem.matrixlibrary.Matrix;

interface IMatrixMultiplier
{
	Matrix multiply(in Matrix m1, in Matrix m2);
	Matrix pow(in Matrix m, int power);
}